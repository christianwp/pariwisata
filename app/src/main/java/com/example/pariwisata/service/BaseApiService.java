package com.example.pariwisata.service;

import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.utilities.JSONResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

import static com.example.pariwisata.utilities.Link.KERANJANG;
import static com.example.pariwisata.utilities.Link.KOMENTAR;
import static com.example.pariwisata.utilities.Link.MS_BANK;
import static com.example.pariwisata.utilities.Link.MS_EVENT;
import static com.example.pariwisata.utilities.Link.MS_FASILITAS;
import static com.example.pariwisata.utilities.Link.MS_JNSMENU;
import static com.example.pariwisata.utilities.Link.MS_KAB_KOTA;
import static com.example.pariwisata.utilities.Link.MS_KATEGORI_WISATA;
import static com.example.pariwisata.utilities.Link.MS_MENU;
import static com.example.pariwisata.utilities.Link.MS_PROV;
import static com.example.pariwisata.utilities.Link.MS_REKENING;
import static com.example.pariwisata.utilities.Link.MS_TENANT;

public interface BaseApiService {

    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseBody> loginRequest(@Field("userid") String id,
                                    @Field("password") String pasw,
                                    @Field("view") String view);

    @FormUrlEncoded
    @POST("update_profile.php")
    Call<ResponseBody> updateProfile(@Field("userid") String id,
                                     @Field("password") String pasw,
                                     @Field("username") String nama,
                                     @Field("telp") String telp,
                                     @Field("email") String email);

    @FormUrlEncoded
    @POST("register.php")
    Call<ResponseBody> registerRequest(@Field("userid") String id,
                                       @Field("password") String pasw,
                                       @Field("username") String nama,
                                       @Field("tglNow") String tanggalNow,
                                       @Field("telp") String telp,
                                       @Field("email") String email,
                                       @Field("status") String status,
                                       @Field("path") String path,
                                       @Field("prov") String prov,
                                       @Field("kab") String kab,
                                       @Field("wisata") String wisata);

    @FormUrlEncoded
    @POST("MsUser.php")
    Call<ResponseBody> updateUser(@Field("kodeUser") String kode,
                                       @Field("pasw") String pasw,
                                       @Field("namaUser") String nama,
                                       @Field("telp") String telp,
                                       @Field("email") String email,
                                       @Field("view") String view,
                                       @Field("kab") String kab,
                                  @Field("tglNow") String tanggalNow,
                                  @Field("id") String id);

    @FormUrlEncoded
    @POST("MsUser.php")
    Call<ResponseBody> viewUser(@Field("kode") String kode,
                                  @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_KAB_KOTA)
    Call<ResponseBody> kabKota(@Field("kodeProv") String kodeProv,
                               @Field("kodeKab") String kodeKab,
                               @Field("namaKab") String namaKab,
                               @Field("view") String view);

    @FormUrlEncoded
    @POST(KOMENTAR)
    Call<ResponseBody> komentar(@Field("kodeWisata") String kodeProv,
                               @Field("bintang") Integer bintang,
                               @Field("komentar") String komentar,
                               @Field("tanggal") String tglNow,
                               @Field("userid") String userid,
                               @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_PROV)
    Call<ResponseBody> provinsi(@Field("kodeProv") String kodeProv,
                               @Field("namaProv") String namaProv,
                               @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_BANK)
    Call<ResponseBody> bank(@Field("kodeBank") String kode,
                            @Field("namaBank") String nama,
                            @Field("kodeUser") String kodeUser,
                            @Field("tglNow") String tglNow,
                            @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_JNSMENU)
    Call<ResponseBody> jenisMenu(@Field("kode") Integer kode,
                                @Field("nama") String nama,
                                 @Field("tglNow") String tglNow,
                                 @Field("kodeUser") String kodeUser,
                                @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_FASILITAS)
    Call<ResponseBody> fasilitas(@Field("kode") Integer kode,
                                @Field("nama") String nama,
                                 @Field("kodeUser") String userId,
                                 @Field("tglNow") String tglNow,
                                @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_KATEGORI_WISATA)
    Call<ResponseBody> kategoriWisata(@Field("kodeKategori") String kode,
                                      @Field("namaKategori") String nama,
                                      @Field("userId") String userId,
                                      @Field("tglNow") String tglNow,
                                      @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_TENANT)
    Call<ResponseBody> tenant(@Field("kodeTenant") String kode,
                              @Field("kodeWisata") String kodeWisata,
                              @Field("namaTenant") String nama,
                              @Field("deskripsi") String deskripsi,
                              @Field("kodeUser") String userId,
                              @Field("tglNow") String tglNow,
                              @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_EVENT)
    Call<ResponseBody> event(@Field("kodeEvent") String kode,
                             @Field("kodeWisata") String kodeWisata,
                             @Field("namaEvent") String nama,
                             @Field("deskripsi") String deskripsi,
                             @Field("tglAwal") String tglAwal,
                             @Field("tglAkhir") String tglAkhir,
                             @Field("kodeUser") String userId,
                             @Field("tglNow") String tglNow,
                             @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_MENU)
    Call<ResponseBody> menu(@Field("kodeMenu") String kode,
                            @Field("kodeTenant") String kodeTenant,
                            @Field("namaMenu") String nama,
                            @Field("jenis") Integer jenis,
                            @Field("harga") double harga,
                            @Field("kodeUser") String userId,
                            @Field("tglNow") String tglNow,
                            @Field("view") String view);

    @FormUrlEncoded
    @POST(MS_REKENING)
    Call<ResponseBody> rekening(@Field("kodeTenant") String kodeTenant,
                            @Field("kodeBank") String kodeBank,
                            @Field("noRek") String noRek,
                            @Field("namaRek") String namaRek,
                            @Field("kodeUser") String userId,
                            @Field("tglNow") String tglNow,
                            @Field("view") String view);

    @FormUrlEncoded
    @POST("wisata_insert.php")
    Call<ResponseBody> insertWisata(@Field("nama") String namaevent,
                                   @Field("deskripsi") String deskripsi,
                                   @Field("alamat") String alamat,
                                   @Field("latt") double latt,
                                   @Field("longt") double longt,
                                   @Field("telp") String telp,
                                   @Field("kategori") String kategori,
                                   @Field("tglNow") String tanggalNow,
                                   @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("wisata_update.php")
    Call<ResponseBody> updateWisata(@Field("kode") String kode,
                                   @Field("nama") String nama,
                                   @Field("deskripsi") String deskripsi,
                                   @Field("alamat") String alamat,
                                   @Field("latt") double latt,
                                   @Field("longt") double longt,
                                   @Field("telp") String telp,
                                   @Field("kategori") String kategori,
                                   @Field("tglNow") String tanggalNow,
                                   @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("wisata.php")
    Call<ResponseBody> requestForUpdateWisata(@Field("kode") String kode,
                                              @Field("user") String id,
                                              @Field("status") String status);

    @FormUrlEncoded
    @POST(KERANJANG)
    Call<ResponseBody> uploadBuktiOrder(@Field("noTransaksi") String noTransaksi,
                                        @Field("kodeBank") String kodeBank,
                                        @Field("noRek") String noRek,
                                        @Field("kodeUser") String kodeUser,
                                        @Field("tglNow") String tglNow,
                                              @Field("view") String status);

    @GET("listProvinsi.php")
    Call<JSONResponse> getProvinsi();

    @FormUrlEncoded
    @POST("listWisata.php")
    Call<JSONResponse> getWisata(@Field("kodeKab") String kode);

    @FormUrlEncoded
    @POST("listRekBank.php")
    Call<JSONResponse> getRekBank(@Field("kodeTenant") String kode);

    @FormUrlEncoded
    @POST("listTenant.php")
    Call<JSONResponse> getTenant(@Field("kode") String kode,
                                 @Field("view") String view);

    @FormUrlEncoded
    @POST("listMenu.php")
    Call<JSONResponse> getMenu(@Field("kodeTenant") String kode);

    @FormUrlEncoded
    @POST("listKab.php")
    Call<JSONResponse> getKab(@Field("kodeProv") String kode);

    @GET("listFasilitas.php")
    Call<JSONResponse> getFasilitas();

    @GET("listBank.php")
    Call<JSONResponse> getBank();

    @GET("listKategori.php")
    Call<JSONResponse> getKategori();

    @GET("listJenis.php")
    Call<JSONResponse> getJenis();
}
