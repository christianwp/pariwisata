package com.example.pariwisata.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.example.pariwisata.R;
import com.example.pariwisata.transaksi.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMessagingService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject json = new JSONObject();
        try {
            json = new JSONObject(remoteMessage.getData().toString());
        }catch (Exception e) {
            Log.d("Msg", "Exception: " + e.getMessage());
        }
        sendNotification(json);
    }

    /**
     * this method is only generating push notification
     */
    private void sendNotification(JSONObject json){
        Context c = getApplicationContext();
        String title="", message="";
        try {
            JSONObject data = json.getJSONObject("data");
            title = data.getString("title");
            message = data.getString("message");
        }catch (JSONException e) {
            Log.e("Msg", "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e("Msg", "Exception: " + e.getMessage());
        }
        Intent intent;
        intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("status", "NOTIF");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String NOTIFICATION_CHANNEL_ID = "101";

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel =
                    new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.inflicted),
                    new AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build());
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        inboxStyle.addLine(message);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setVibrate(new long[]{0, 100})
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setWhen(System.currentTimeMillis())
                .setSound(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.inflicted))
                .setAutoCancel(true)
                .setStyle(inboxStyle)
                .setLights(ContextCompat.getColor(c, R.color.colorPrimary), 3000, 3000)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);
        notificationManager.notify(1, notificationBuilder.build());
    }
}
