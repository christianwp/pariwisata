package com.example.pariwisata.model.list;

public class ListTenant {

    private String kodeTenant;
    private String namaTenant;
    private String pemilikTenant;
    private String kodeWisata;
    private String namaWisata;

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public String getKodeWisata() {
        return kodeWisata;
    }

    public void setKodeWisata(String kodeWisata) {
        this.kodeWisata = kodeWisata;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getPemilikTenant() {
        return pemilikTenant;
    }

    public void setPemilikTenant(String pemilikTenant) {
        this.pemilikTenant = pemilikTenant;
    }
}
