package com.example.pariwisata.model.master;

import java.io.Serializable;

public class MsWisataJadwalModel implements Serializable {

    private String kodeWisata;
    private String hari;
    private String buka;
    private String tutup;
    private String status;

    public String getKodeWisata() {
        return kodeWisata;
    }

    public void setKodeWisata(String kodeWisata) {
        this.kodeWisata = kodeWisata;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getBuka() {
        return buka;
    }

    public void setBuka(String buka) {
        this.buka = buka;
    }

    public String getTutup() {
        return tutup;
    }

    public void setTutup(String tutup) {
        this.tutup = tutup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
