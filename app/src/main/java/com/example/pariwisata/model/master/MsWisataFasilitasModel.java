package com.example.pariwisata.model.master;

import java.io.Serializable;

public class MsWisataFasilitasModel implements Serializable {

    private String kodeWisata;
    private String kodeFasilitas;
    private String namaFasilitas;

    public String getKodeWisata() {
        return kodeWisata;
    }

    public void setKodeWisata(String kodeWisata) {
        this.kodeWisata = kodeWisata;
    }

    public String getKodeFasilitas() {
        return kodeFasilitas;
    }

    public void setKodeFasilitas(String kodeFasilitas) {
        this.kodeFasilitas = kodeFasilitas;
    }

    public String getNamaFasilitas() {
        return namaFasilitas;
    }

    public void setNamaFasilitas(String namaFasilitas) {
        this.namaFasilitas = namaFasilitas;
    }
}
