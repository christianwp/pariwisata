package com.example.pariwisata.model.master;

import java.io.Serializable;

public class MsTenantModel implements Serializable {

    private String kodeTenant;
    private String namaTenant;
    private String kodeWisata;
    private String namaWisata;
    private String kodePemilik;
    private String namaPemilik;
    private String deskripsi;

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public String getKodeWisata() {
        return kodeWisata;
    }

    public void setKodeWisata(String kodeWisata) {
        this.kodeWisata = kodeWisata;
    }

    public String getNamaWisata() {
        return namaWisata;
    }

    public void setNamaWisata(String namaWisata) {
        this.namaWisata = namaWisata;
    }

    public String getKodePemilik() {
        return kodePemilik;
    }

    public void setKodePemilik(String kodePemilik) {
        this.kodePemilik = kodePemilik;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
