package com.example.pariwisata.model.transaksi;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.math.BigDecimal;

public class KeranjangHeaderModel implements Serializable {

    private String noTransaksi;
    private String kodeTenant;
    private String namaTenant;
    private String kodeUser;
    private String namaUser;
    private String tanggal;
    private String lokasi;
    private String cdstatus;
    private BigDecimal nilai;

    public String getNoTransaksi() {
        return noTransaksi;
    }

    public void setNoTransaksi(String noTransaksi) {
        this.noTransaksi = noTransaksi;
    }

    public String getKodeUser() {
        return kodeUser;
    }

    public void setKodeUser(String kodeUser) {
        this.kodeUser = kodeUser;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getCdstatus() {
        return cdstatus;
    }

    public void setCdstatus(String cdstatus) {
        this.cdstatus = cdstatus;
    }

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public BigDecimal getNilai() {
        return nilai;
    }

    public void setNilai(BigDecimal nilai) {
        this.nilai = nilai;
    }

    public String computeHash(){
        StringBuilder sb = new StringBuilder();
        sb.append(getNoTransaksi()).append(getTanggal()).append(getKodeTenant());
        sb.append(getNamaTenant()).append(getKodeUser());
        sb.append(getNamaUser()).append(getLokasi());
        sb.append(getCdstatus()).append(getNilai());
        return DigestUtils.md5Hex(sb.toString());
    }
}
