package com.example.pariwisata.model.master;

public class MsRekeningModel {

    private String kodeTenant;
    private String namaTenant;
    private String kodeBank;
    private String namaBank;
    private String noRek;
    private String namaRek;

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public String getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(String kodeBank) {
        this.kodeBank = kodeBank;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public void setNamaBank(String namaBank) {
        this.namaBank = namaBank;
    }

    public String getNoRek() {
        return noRek;
    }

    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }

    public String getNamaRek() {
        return namaRek;
    }

    public void setNamaRek(String namaRek) {
        this.namaRek = namaRek;
    }
}
