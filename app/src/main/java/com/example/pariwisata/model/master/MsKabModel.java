package com.example.pariwisata.model.master;

public class MsKabModel {

    private String kodeProv;
    private String namaProv;
    private String kodeKabKota;
    private String namaKabKota;
    private String cstat;

    public String getKodeProv() {
        return kodeProv;
    }

    public void setKodeProv(String kodeProv) {
        this.kodeProv = kodeProv;
    }

    public String getKodeKabKota() {
        return kodeKabKota;
    }

    public void setKodeKabKota(String kodeKabKota) {
        this.kodeKabKota = kodeKabKota;
    }

    public String getNamaKabKota() {
        return namaKabKota;
    }

    public void setNamaKabKota(String namaKabKota) {
        this.namaKabKota = namaKabKota;
    }

    public String getCstat() {
        return cstat;
    }

    public void setCstat(String cstat) {
        this.cstat = cstat;
    }

    public String getNamaProv() {
        return namaProv;
    }

    public void setNamaProv(String namaProv) {
        this.namaProv = namaProv;
    }
}
