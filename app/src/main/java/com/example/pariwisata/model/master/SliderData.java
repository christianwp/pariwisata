package com.example.pariwisata.model.master;

import com.google.firebase.storage.StorageReference;

public class SliderData {

    private StorageReference imgUrl;

    // Constructor method.
    public SliderData() {

    }

    // Getter method
    public StorageReference getImgUrl() {
        return imgUrl;
    }

    // Setter method
    public void setImgUrl(StorageReference imgUrl) {
        this.imgUrl = imgUrl;
    }
}
