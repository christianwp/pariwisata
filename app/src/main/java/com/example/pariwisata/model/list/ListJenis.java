package com.example.pariwisata.model.list;

public class ListJenis {

    private Integer kodeJenis;
    private String namaJenis;

    public Integer getKodeJenis() {
        return kodeJenis;
    }

    public void setKodeJenis(Integer kodeJenis) {
        this.kodeJenis = kodeJenis;
    }

    public String getNamaJenis() {
        return namaJenis;
    }

    public void setNamaJenis(String namaJenis) {
        this.namaJenis = namaJenis;
    }
}
