package com.example.pariwisata.model.master;

import java.io.Serializable;

public class MsViewRatingModel implements Serializable {

    private String kodeWisata;
    private Integer nilai;
    private String komentar;
    private String tanggal;
    private String kodeUser;
    private String namaUser;

    public String getKodeWisata() {
        return kodeWisata;
    }

    public void setKodeWisata(String kodeWisata) {
        this.kodeWisata = kodeWisata;
    }

    public Integer getNilai() {
        return nilai;
    }

    public void setNilai(Integer nilai) {
        this.nilai = nilai;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getKodeUser() {
        return kodeUser;
    }

    public void setKodeUser(String kodeUser) {
        this.kodeUser = kodeUser;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }
}
