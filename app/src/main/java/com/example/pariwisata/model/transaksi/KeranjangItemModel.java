package com.example.pariwisata.model.transaksi;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.math.BigDecimal;

public class KeranjangItemModel implements Serializable {

    public enum Status {
        NEW, NEW_MODIFIED, OLD, OLD_MODIFIED;
    }
    private String noTransaksi;
    private String kodeMenu;
    private String namaMenu;
    private BigDecimal harga;
    private Integer qty;
    private BigDecimal total;
    private String keterangan;
    private Status status;

    public String getNoTransaksi() {
        return noTransaksi;
    }

    public void setNoTransaksi(String noTransaksi) {
        this.noTransaksi = noTransaksi;
    }

    public String getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(String kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    public String computeHash(){
        StringBuilder sb = new StringBuilder();
        sb.append(getNoTransaksi()).append(getKodeMenu()).append(getNamaMenu());
        sb.append(getHarga()).append(getQty());
        sb.append(getTotal()).append(getKeterangan());
        return DigestUtils.md5Hex(sb.toString());
    }
}
