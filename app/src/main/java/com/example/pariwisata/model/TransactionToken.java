package com.example.pariwisata.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class TransactionToken implements Serializable {

    private Map<String,Object> originProperties;
    private Map<String,String> originStringProperties;
    private Map<String,BigDecimal> originDecimalProperties;
    private Map<String,Integer> originIntegerProperties;
    private Map<String,Long> originLongProperties;
    private Map<String,Date> originDateProperties;
    private String originHash;

    public TransactionToken(){}

    public TransactionToken(Map<String,Object> originProperties, String originHash){
        this.originProperties = originProperties;
        this.originHash = originHash;
    }

    public Map<String,Object> getOriginProperties() {return originProperties;}
    public Map<String,String> getOriginStringProperties() {return originStringProperties;}
    public Map<String,BigDecimal> getOriginDecimalProperties() {return originDecimalProperties;}
    public Map<String,Integer> getOriginIntegerProperties() {return originIntegerProperties;}
    public Map<String,Long> getOriginLongProperties() {return originLongProperties;}
    public Map<String,Date> getOriginDateProperties() {return originDateProperties;}
    public String getOriginHash(){return this.originHash;}

    public Object getOriginProperty(String propertyName){return this.getOriginProperties().get(propertyName);}
    public String getOriginStringProperty(String propertyName){return this.getOriginStringProperties().get(propertyName);}
    public BigDecimal getOriginDecimalProperty(String propertyName){return this.getOriginDecimalProperties().get(propertyName);}
    public Integer getOriginIntegerProperty(String propertyName){return this.getOriginIntegerProperties().get(propertyName);}
    public Long getOriginLongProperty(String propertyName){return this.getOriginLongProperties().get(propertyName);}
    public Date getOriginDateProperty(String propertyName){return this.getOriginDateProperties().get(propertyName);}

    public void setOriginProperties(Map<String,Object> originProperties) {
        this.originProperties = originProperties;
    }

    public void setOriginStringProperties(Map<String,String> originStringProperties) {
        this.originStringProperties = originStringProperties;
    }

    public void setOriginDecimalProperties(Map<String, BigDecimal> originDecimalProperties) {
        this.originDecimalProperties = originDecimalProperties;
    }

    public void setOriginIntegerProperties(Map<String,Integer> originIntegerProperties) {
        this.originIntegerProperties = originIntegerProperties;
    }

    public void setOriginLongProperties(Map<String,Long> originLongProperties) {
        this.originLongProperties = originLongProperties;
    }

    public void setOriginDateProperties(Map<String, Date> originDateProperties) {
        this.originDateProperties = originDateProperties;
    }

    public void setOriginHash(String originHash) {
        this.originHash = originHash;
    }
}
