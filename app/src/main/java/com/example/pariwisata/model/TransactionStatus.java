package com.example.pariwisata.model;

import java.io.Serializable;

public class TransactionStatus implements Serializable {

    public static final String PROP_STATUS_STRING = "statusString";
    public static final String PROP_TRANS_MODIFY_ALLOWED = "transModifyAllowed";
    public static final String PROP_TRANS_DELETE_ALLOWED = "transDeleteAllowed";
    public static final TransactionStatus NONE = new TransactionStatus("",true,true);
    private String statusString;
    private boolean transModifyAllowed, transDeleteAllowed;

    public TransactionStatus(){}

    public TransactionStatus(String statusString) {
        this(statusString,false,false);
    }

    public TransactionStatus(String statusString, boolean transModifyAllowed, boolean transDeleteAllowed){
        this.statusString = statusString;
        this.transModifyAllowed = transModifyAllowed;
        this.transDeleteAllowed = transDeleteAllowed;
    }

    public String getStatusString() {return statusString;}
    public boolean isTransModifyAllowed() {return transModifyAllowed;}
    public boolean isTransDeleteAllowed() {return transDeleteAllowed;}

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    public void setTransModifyAllowed(boolean transModifyAllowed) {
        this.transModifyAllowed = transModifyAllowed;
    }

    public void setTransDeleteAllowed(boolean transDeleteAllowed) {
        this.transDeleteAllowed = transDeleteAllowed;
    }
}