package com.example.pariwisata.model.master;

public class MsFasilitasModel {

    private Integer kode;
    private String nama;

    public Integer getKode() {
        return kode;
    }

    public void setKode(Integer kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
