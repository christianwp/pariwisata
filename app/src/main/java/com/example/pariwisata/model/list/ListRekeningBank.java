package com.example.pariwisata.model.list;

public class ListRekeningBank {

    private String kodeTenant;
    private String namaTenant;
    private String noRek;
    private String namaRek;
    private String kodeBank;
    private String namaBank;

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public String getNoRek() {
        return noRek;
    }

    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }

    public String getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(String kodeBank) {
        this.kodeBank = kodeBank;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public void setNamaBank(String namaBank) {
        this.namaBank = namaBank;
    }

    public String getNamaRek() {
        return namaRek;
    }

    public void setNamaRek(String namaRek) {
        this.namaRek = namaRek;
    }
}
