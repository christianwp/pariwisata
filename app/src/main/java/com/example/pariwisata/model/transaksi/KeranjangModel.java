package com.example.pariwisata.model.transaksi;

import com.example.pariwisata.model.TransactionStatus;
import com.example.pariwisata.model.TransactionToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class KeranjangModel implements Serializable {
    private KeranjangHeaderModel headerModel;
    private List<KeranjangItemModel> itemList;
    private List<KeranjangItemModel> deleteList;
    private TransactionStatus transactionStatus;
    private TransactionToken transactionToken;

    public KeranjangModel(){
        this.itemList = new ArrayList<KeranjangItemModel>();
        this.setDeleteList(new ArrayList<KeranjangItemModel>());
    }

    public KeranjangHeaderModel getHeaderModel() {return headerModel;}
    public List<KeranjangItemModel> getItemList() {return itemList;}
    public List<KeranjangItemModel> getDeleteList() {return deleteList;}
    public TransactionStatus getTransactionStatus() {return transactionStatus;}
    public TransactionToken getTransactionToken() {return transactionToken;}

    public void setHeaderModel(KeranjangHeaderModel headerModel) {
        this.headerModel = headerModel;
    }
    public void setItemList(List<KeranjangItemModel> itemList) {
        this.itemList = itemList;
    }
    public void setDeleteList(List<KeranjangItemModel> deleteList) {
        this.deleteList = deleteList;
    }
    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
    public void setTransactionToken(TransactionToken transactionToken) {
        this.transactionToken = transactionToken;
    }
    public void addItem(KeranjangItemModel itemModel){
        itemList.add(itemModel);
    }
    public void removeItem(KeranjangItemModel itemModel){
        Iterator<KeranjangItemModel> itemIterator = itemList.iterator();
        while(itemIterator.hasNext()){
            KeranjangItemModel item = itemIterator.next();
            switch(item.getStatus()){
                case NEW: case NEW_MODIFIED :
                    if(item.getKodeMenu().equals(itemModel.getKodeMenu())){
                        itemIterator.remove();
                    }
                    break;
                case OLD : case OLD_MODIFIED:
                    if(item.getNoTransaksi().equals(itemModel.getNoTransaksi()) &&
                            item.getKodeMenu().equals(itemModel.getKodeMenu())){
                        deleteList.add(item);
                        itemIterator.remove();
                    }
                    break;
            }
        }
    }
}