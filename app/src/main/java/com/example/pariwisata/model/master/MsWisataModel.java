package com.example.pariwisata.model.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MsWisataModel implements Serializable {

    private MsWisataHeaderModel header;
    private List<MsWisataFasilitasModel> item;
    private List<MsWisataJadwalModel> itemjadwal;

    public MsWisataModel(){
        this.item = new ArrayList<MsWisataFasilitasModel>();
        this.itemjadwal = new ArrayList<MsWisataJadwalModel>();
    }

    public List<MsWisataFasilitasModel> getItem() {
        return item;
    }

    public void setItem(List<MsWisataFasilitasModel> item) {
        this.item = item;
    }

    public void addItem(MsWisataFasilitasModel itemModel){
        item.add(itemModel);
    }

    public MsWisataHeaderModel getHeader() {
        return header;
    }

    public void setHeader(MsWisataHeaderModel header) {
        this.header = header;
    }

    public List<MsWisataJadwalModel> getItemjadwal() {
        return itemjadwal;
    }

    public void setItemjadwal(List<MsWisataJadwalModel> itemjadwal) {
        this.itemjadwal = itemjadwal;
    }

    public void addItemJadwal(MsWisataJadwalModel itemModel){
        itemjadwal.add(itemModel);
    }
}