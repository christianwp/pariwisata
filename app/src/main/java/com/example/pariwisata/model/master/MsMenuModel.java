package com.example.pariwisata.model.master;

import java.io.Serializable;
import java.math.BigDecimal;

public class MsMenuModel implements Serializable {

    private String kodeMenu;
    private String namaMenu;
    private Integer kodeJenis;
    private String namaJenis;
    private BigDecimal harga;
    private String kodeTenant;
    private String namaTenant;

    public String getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(String kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public Integer getKodeJenis() {
        return kodeJenis;
    }

    public void setKodeJenis(Integer kodeJenis) {
        this.kodeJenis = kodeJenis;
    }

    public String getNamaJenis() {
        return namaJenis;
    }

    public void setNamaJenis(String namaJenis) {
        this.namaJenis = namaJenis;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public String getKodeTenant() {
        return kodeTenant;
    }

    public void setKodeTenant(String kodeTenant) {
        this.kodeTenant = kodeTenant;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }
}
