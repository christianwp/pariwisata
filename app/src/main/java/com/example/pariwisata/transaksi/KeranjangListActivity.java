package com.example.pariwisata.transaksi;

import static com.example.pariwisata.utilities.Link.KERANJANG;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMasterWisata;
import com.example.pariwisata.adapter.transaksi.AdpKeranjang;
import com.example.pariwisata.list.ListKabView;
import com.example.pariwisata.list.ListTenantView;
import com.example.pariwisata.list.ListWisataView;
import com.example.pariwisata.master.MsTempatWisataActivity;
import com.example.pariwisata.master.MsTempatWisataItemActivity;
import com.example.pariwisata.model.TransactionStatus;
import com.example.pariwisata.model.transaksi.KeranjangHeaderModel;
import com.example.pariwisata.model.transaksi.KeranjangListKriteriaModel;
import com.example.pariwisata.model.transaksi.KeranjangModel;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class KeranjangListActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private AdpKeranjang adapter;
    private BaseApiService mApiService;
    private PrefUtil prefUtil;
    private ImageView imgBack, imgAdd;
    private Button btnProses;
    private TextInputEditText edKab, edWisata, edTenant;
    private ListView lsvData;
    private SharedPreferences shared;
    private String level, userId, namaUser, kodeKab, kodeWisata, kodeTenant, pemilikTenant;
    private TextView txtStatus;
    private ArrayList<KeranjangListKriteriaModel> columnlist= new ArrayList<KeranjangListKriteriaModel>();
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tenant_cust_order);
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        prefUtil = new PrefUtil(this);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            namaUser = shared.getString(PrefUtil.NAME, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.imgTransPesanBack);
        edKab = (TextInputEditText)findViewById(R.id.eTransPesanNamaKabKota);
        edWisata = (TextInputEditText)findViewById(R.id.eTransPesanNamaWisata);
        edTenant = (TextInputEditText) findViewById(R.id.eTransPesanNamaTenant);
        btnProses = (Button) findViewById(R.id.btnTransPesanRetrieve);
        lsvData = (ListView)findViewById(R.id.listTransPesan);
        txtStatus = (TextView) findViewById(R.id.txtListTransPesanStatus);
        imgAdd = (ImageView) findViewById(R.id.imgListTransPesanAdd);
        txtStatus.setVisibility(View.GONE);

        adapter		= new AdpKeranjang(KeranjangListActivity.this, R.layout.adp_keranjang, columnlist, level, userId);
        lsvData.setAdapter(adapter);

        if(level.equals("C"))
            imgAdd.setVisibility(View.VISIBLE);
        else
            imgAdd.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        edKab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edKab.setEnabled(false);
                hideKeyboard(v);
                pilihKab();
                edKab.setEnabled(true);
            }
        });

        edKab.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edKab.setEnabled(false);
                    hideKeyboard(v);
                    pilihKab();
                    edKab.setEnabled(true);
                }
            }
        });

        edWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kodeKab!=null && !kodeKab.equals("")){
                    edWisata.setEnabled(false);
                    hideKeyboard(v);
                    pilihWisata();
                    edWisata.setEnabled(true);
                }else{
                    Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }
            }
        });

        edWisata.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(kodeKab!=null && !kodeKab.equals("")){
                        edWisata.setEnabled(false);
                        hideKeyboard(v);
                        pilihWisata();
                        edWisata.setEnabled(true);
                    }else{
                        Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        edTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kodeKab==null && kodeKab.equals("")){
                    Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }else if(kodeWisata==null && kodeWisata.equals("")){
                    Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }else{
                    edTenant.setEnabled(false);
                    hideKeyboard(v);
                    pilihTenant();
                    edTenant.setEnabled(true);
                }
            }
        });

        edTenant.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(kodeKab==null && kodeKab.equals("")){
                        Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }else if(kodeWisata==null && kodeWisata.equals("")){
                        Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }else{
                        edTenant.setEnabled(false);
                        hideKeyboard(v);
                        pilihTenant();
                        edTenant.setEnabled(true);
                    }
                }
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(kodeKab==null){
                    Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }else if(kodeWisata==null){
                    Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }else if(kodeTenant==null){
                    Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }else{
                    getData(Link.BASE_URL_API+KERANJANG, kodeTenant);
                }
            }
        });

        lsvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(KeranjangListActivity.this, KeranjangItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("noTransaksi", columnlist.get(position).getNoTrans());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(level.equals("C")){
                    if(kodeKab==null && kodeKab.equals("")){
                        Toast.makeText(KeranjangListActivity.this, "Kab/Kota harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }else if(kodeWisata==null && kodeWisata.equals("")){
                        Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }else if(kodeTenant==null && kodeTenant.equals("")){
                        Toast.makeText(KeranjangListActivity.this, "Wisata harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }else{
                        KeranjangModel model = new KeranjangModel();
                        KeranjangHeaderModel headerModel = new KeranjangHeaderModel();
                        headerModel.setCdstatus("**********");
                        headerModel.setKodeUser(userId);
                        headerModel.setNamaUser(namaUser);
                        headerModel.setKodeTenant(kodeTenant);
                        headerModel.setNamaTenant(edTenant.getText().toString());
                        Date today = Calendar.getInstance().getTime();
                        String tanggalNow = df.format(Utils.getFirstTimeOfDay(today));
                        headerModel.setTanggal(tanggalNow);
                        headerModel.setNilai(BigDecimal.ZERO);
                        model.setHeaderModel(headerModel);
                        model.setTransactionStatus(TransactionStatus.NONE);
                        Intent i = new Intent(KeranjangListActivity.this, KeranjangItemActivity.class);
                        i.putExtra("Status", "ADD");
                        i.putExtra("model", model);
                        i.putExtra("pemilikTenant", pemilikTenant);
                        startActivityForResult(i, 9);
                    }
                }else{
                    finish();
                }
            }
        });
    }

    private void pilihKab(){
        Intent i = new Intent(KeranjangListActivity.this, ListKabView.class);
        i.putExtra("kode", "72");
        startActivityForResult(i, 4);
    }

    private void pilihWisata(){
        Intent i = new Intent(KeranjangListActivity.this, ListWisataView.class);
        i.putExtra("kode", kodeKab);
        startActivityForResult(i, 1);
    }

    private void pilihTenant(){
        Intent i = new Intent(KeranjangListActivity.this, ListTenantView.class);
        i.putExtra("kode", kodeWisata);
        i.putExtra("view", "WISATA");
        startActivityForResult(i, 2);
    }

    private void getData(String Url, final String kode){
        pDialog.setMessage("Loading");
        showDialog();
        txtStatus.setVisibility(View.GONE);
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    if (sucses==1){
                        txtStatus.setVisibility(View.GONE);
                        hideDialog();
                        adapter.clear();
                        JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                        for (int i = 0; i < JsonArray.length(); i++) {
                            JSONObject object = JsonArray.getJSONObject(i);
                            KeranjangListKriteriaModel colums 	= new KeranjangListKriteriaModel();
                            colums.setNoTrans(object.getString("c_kodetransaksi"));
                            colums.setTanggal(object.getString("dt_tanggal"));
                            colums.setKodeTenant(object.getString("c_kodetenant"));
                            colums.setNamaTenant(object.getString("vc_namatenant"));
                            colums.setNilai(new BigDecimal(object.getDouble("n_nilai")));
                            colums.setKodeUser(object.getString("c_userid"));
                            colums.setNamaUser(object.getString("vc_username"));
                            colums.setStatus(object.getString("vc_status"));
                            colums.setCdstatus(object.getString("c_dstatus"));
                            colums.setPemilikTenant(object.getString("c_pemilik"));
                            columnlist.add(colums);
                        }
                    }else{
                        txtStatus.setVisibility(View.VISIBLE);
                        txtStatus.setText("Tidak Ada Data");
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
                //lsvupload.invalidate();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check Koneksi Internet Anda");
                } else if (error instanceof AuthFailureError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("AuthFailureError");
                } else if (error instanceof ServerError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check ServerError");
                } else if (error instanceof NetworkError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check NetworkError");
                } else if (error instanceof ParseError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check ParseError");
                }
                hideDialog();
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeTenant", kode);
                params.put("kodeUser", userId);
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist= new ArrayList<KeranjangListKriteriaModel>();
        adapter		= new AdpKeranjang(KeranjangListActivity.this, R.layout.adp_keranjang, columnlist, level, userId);
        lsvData.setAdapter(adapter);
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4) {
            if(resultCode == RESULT_OK) {
                edKab.setText(data.getStringExtra("namaKab"));
                kodeKab = data.getStringExtra("kodeKab");
            }
        }
        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                kodeWisata = data.getStringExtra("kodeWisata");
                edWisata.setText(data.getStringExtra("namaWisata"));
            }
        }
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                kodeTenant = data.getStringExtra("kodeTenant");
                pemilikTenant = data.getStringExtra("pemilikTenant");
                edTenant.setText(data.getStringExtra("namaTenant"));
            }
        }
        if(requestCode == 9) {
            if(resultCode == RESULT_OK) {
                columnlist= new ArrayList<KeranjangListKriteriaModel>();
                adapter		= new AdpKeranjang(KeranjangListActivity.this, R.layout.adp_keranjang, columnlist, level, userId);
                lsvData.setAdapter(adapter);
                getData(Link.BASE_URL_API+KERANJANG, kodeTenant);
            }
        }
    }
}
