package com.example.pariwisata.transaksi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.pariwisata.R;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private ImageView imgBack, imgProfil;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, nama, email, telp, profile;
    private ProgressDialog pDialog;
    private TextView txtId, txtEmail, txtTelp, txtNama;
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private ProgressBar prbProfil;
    private Button btnUbah;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private BaseApiService mApiService;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_user);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        mApiService         = Link.getAPIService();
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
            nama = shared.getString(PrefUtil.NAME, null);
            email = shared.getString(PrefUtil.EMAIL, null);
            telp = shared.getString(PrefUtil.TELP, null);
            profile = shared.getString(PrefUtil.PATH, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.img_profile_back);
//        btnGalery = (Button)findViewById(R.id.btn_galery);
        txtId = (TextView)findViewById(R.id.txt_profile_iduser);
        txtNama = (TextView)findViewById(R.id.txt_profile_namauser);
        txtEmail = (TextView)findViewById(R.id.txt_profile_emailuser);
        txtTelp = (TextView)findViewById(R.id.txt_profile_telpuser);
        imgProfil = (ImageView)findViewById(R.id.img_profile_foto);
        prbProfil = (ProgressBar)findViewById(R.id.prbProfilUserImg);
        btnUbah = (Button)findViewById(R.id.btn_ubah_profile);
//        btnWisata = (Button)findViewById(R.id.btn_buat_wisata);
//        btnUmkm = (Button)findViewById(R.id.btn_umkm);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        txtId.setText(userId);
        requestLogin(userId);
//        if(level.equals("A")){
//            btnUbah.setVisibility(View.VISIBLE);
//        }else if(level.equals("A")){
//
//        }
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProfileActivity.this, UbahProfileActivity.class), 2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                requestLogin(userId);
            }
        }
    }

    private void requestLogin(String id){
        pDialog.setMessage("Ambil Data User ...\nHarap Tunggu");
        showDialog();
        mApiService.loginRequest(id, "", "PROFILE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    String nama = jsonRESULTS.getJSONObject("user").getString("vc_username")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_username");
                                    String uId = jsonRESULTS.getJSONObject("user").getString("c_userid")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_userid");
                                    String telp = jsonRESULTS.getJSONObject("user").getString("c_telp")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_telp");
                                    String status = jsonRESULTS.getJSONObject("user").getString("c_status")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_status");
                                    String email = jsonRESULTS.getJSONObject("user").getString("vc_email")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_email");
                                    String path = jsonRESULTS.getJSONObject("user").getString("vc_pathfoto")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_pathfoto");
                                    String pasw = jsonRESULTS.getJSONObject("user").getString("vc_password")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_password");
                                    Integer countWisata = jsonRESULTS.getJSONObject("user").getInt("count_wisata");
                                    String kdKab = "", nmKab = "", kdPrv="", nmPrv="", kdWisata="", nmWisata = "";
                                    if (status.equals("X")){
                                        kdKab = jsonRESULTS.getJSONObject("user").getString("c_idkabkota")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_idkabkota");
                                        nmKab = jsonRESULTS.getJSONObject("user").getString("vc_namekabkota")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_namekabkota");
                                        kdPrv = jsonRESULTS.getJSONObject("user").getString("c_idprovinsi")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_idprovinsi");
                                        nmPrv = jsonRESULTS.getJSONObject("user").getString("vc_nameprovinsi")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_nameprovinsi");
                                    }else if(status.equals("U")){
                                        kdWisata = jsonRESULTS.getJSONObject("user").getString("c_kodewisata")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_kodewisata");
                                        nmWisata = jsonRESULTS.getJSONObject("user").getString("vc_namawisata")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_namawisata");
                                    }
                                    hideDialog();
                                    pref.clear();
                                    pref.saveUserInfo(uId, nama, status, path, pasw, telp, email, countWisata,
                                            kdKab, nmKab, kdPrv, nmPrv, kdWisata, nmWisata);
                                    loadImage();
                                    txtId.setText(userId);
                                    txtNama.setText(nama);
                                    txtEmail.setText(email);
                                    txtTelp.setText(telp);
                                } else {
                                    hideDialog();
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(ProfileActivity.this, error_message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(ProfileActivity.this, "GAGAL MENGAMBIL DATA", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(ProfileActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void loadImage(){
        pDialog.setMessage("Loading Image....");
        showDialog();
        imgProfil.setBackgroundResource(0);
        StorageReference ref = storageReference.child("profile/"+(userId)+".jpg");
        Glide.with(this)
                .load(ref)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }
                })
                .into(imgProfil);
    }
}
