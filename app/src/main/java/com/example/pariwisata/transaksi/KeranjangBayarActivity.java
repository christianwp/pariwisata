package com.example.pariwisata.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.pariwisata.R;
import com.example.pariwisata.list.ListRekBankView;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.GlideApp;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KeranjangBayarActivity extends AppCompatActivity {

    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private ProgressDialog pDialog;
    private BaseApiService mApiService;
    private PrefUtil prefUtil;
    private ImageView imgBack, imgUpload;
    private Button btnSave;
    private SharedPreferences shared;
    private String level, userId, noTransaksi, kodeBank, kodeTenant, pemilikTenant;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private TextInputEditText edNo, edNominal, edNoRek, edNamaRek, edBank;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private static final int REQUEST_CODE_GALLERY = 0013;
    private String hasilFoto = "N", status;
    private Uri selectedImage;
    private BigDecimal nilai;
    private Double doublee;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private LinearLayout lin;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_keranjang_bayar);
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                doublee = i.getDouble("nilai",0);
                kodeTenant = i.getString("kodeTenant");
                pemilikTenant = i.getString("pemilikTenant");
                noTransaksi = i.getString("noTransaksi");
                status = i.getString("status");
            } catch (Exception e) {}
        }
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        imgBack = (ImageView) findViewById(R.id.imgViewOrderBayarBack);
        edNo = (TextInputEditText) findViewById(R.id.eAddViewOrderBayarNoTrans);
        edNominal = (TextInputEditText) findViewById(R.id.eAddViewOrderBayarNominal);
        edNoRek = (TextInputEditText) findViewById(R.id.eAddViewOrderBayarNoRek);
        edBank = (TextInputEditText) findViewById(R.id.eAddViewOrderBayarBank);
        edNamaRek = (TextInputEditText) findViewById(R.id.eAddViewOrderBayarNamaRek);
        imgUpload = (ImageView)findViewById(R.id.img_upload_bukti);
        btnSave = (Button) findViewById(R.id.btnViewOrderBayarSave);
        lin = (LinearLayout)findViewById(R.id.linKeranjangbayarRek);

        edNo.setText(noTransaksi);
        edNominal.setText(String.valueOf(formatter.format(new BigDecimal(doublee))));

        if(status.equals("UPLOAD")){
            lin.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.VISIBLE);
        }else{
            lin.setVisibility(View.GONE);
            btnSave.setVisibility(View.GONE);
            loadImage(imgUpload, noTransaksi);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.equals("UPLOAD")) {
                    openImage();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.equals("UPLOAD")) {
                    if (validateRek() && validateFoto()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangBayarActivity.this);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Proses upload pembayaran?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        uploadImage();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            }
        });

        edNoRek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edNoRek.setEnabled(false);
                hideKeyboard(v);
                pilihRekBank();
                edNoRek.setEnabled(true);
            }
        });

        edNoRek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edNoRek.setEnabled(false);
                    hideKeyboard(v);
                    pilihRekBank();
                    edNoRek.setEnabled(true);
                }
            }
        });
    }

    private void loadImage(ImageView img, String noTransaksi){
        pDialog.setMessage("Loading Image....");
        showDialog();
        img.setBackgroundResource(0);
        StorageReference ref = storageReference.child("order/"+(noTransaksi+".jpg"));
        Glide.with(this /* context */)
                .load(ref)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }
                })
                .into(img);
    }

    private boolean validateRek() {
        boolean value;
        if (edNoRek.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(KeranjangBayarActivity.this, "Nomor rekening belum dipilih", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateFoto() {
        boolean value;
        if (!hasilFoto.equals("Y")){
            value=false;
            Toast.makeText(KeranjangBayarActivity.this, "Bukti pembayaran belum dipilih", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private void uploadImage() {
        if (selectedImage != null) {
            pDialog.setMessage("Uploading Image...");
            showDialog();
            StorageReference ref = storageReference.child("order/"+ (noTransaksi.trim()+".jpg"));
            ref.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(
                                UploadTask.TaskSnapshot taskSnapshot){
                            updateData(noTransaksi.trim(), kodeBank, edNoRek.getText().toString(), userId);
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(KeranjangBayarActivity.this,"Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    private void updateData(String noTransaksi, String kodeBank, String noRek, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        mApiService.uploadBuktiOrder(noTransaksi, kodeBank, noRek, userId, tanggalNow, "UPLOAD")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    sendNotification(pemilikTenant, ("Transaksi sudah dibayarkan dengan nomor "+noTransaksi));
                                    hideDialog();
                                    Toast.makeText(KeranjangBayarActivity.this, "DATA BERHASIL DIUPLOAD", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(KeranjangBayarActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(KeranjangBayarActivity.this, "PROSES UPLOAD GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(KeranjangBayarActivity.this, "PROSES UPLOAD GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(KeranjangBayarActivity.this, "PROSES UPLOAD GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(KeranjangBayarActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void openImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY);
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void sendNotification(final String pemilik, final String message){
        StringRequest register = new StringRequest(Request.Method.POST, Link.BASE_URL_NOTIF, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonrespon = new JSONObject(response);
                    JSONArray JsonArray = jsonrespon.getJSONArray("data");
                    if (JsonArray!=null){

                    }else{
                        //tvstatus.setText("Tidak Ada Data");
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //tvstatus.setText("Check Koneksi Internet Anda");
                } else if (error instanceof AuthFailureError) {
                    //tvstatus.setText("AuthFailureError");
                } else if (error instanceof ServerError) {
                    //tvstatus.setText("Check ServerError");
                } else if (error instanceof NetworkError) {
                    //tvstatus.setText("Check NetworkError");
                } else if (error instanceof ParseError) {
                    //tvstatus.setText("Check ParseError");
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("topics", pemilik);
                params.put("message", message);
                params.put("judul", "Notifikasi Pembayaran");
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void pilihRekBank(){
        Intent i = new Intent(KeranjangBayarActivity.this, ListRekBankView.class);
        i.putExtra("kode", kodeTenant);
        startActivityForResult(i, 4);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 4) {
            if(resultCode == RESULT_OK) {
                kodeBank = data.getStringExtra("kodeBank");
                edBank.setText(data.getStringExtra("namaBank"));
                edNoRek.setText(data.getStringExtra("noRek"));
                edNamaRek.setText(data.getStringExtra("namaRek"));
            }
        }
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto = "Y";
                selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                imgUpload.setBackgroundResource(0);
                GlideApp.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .override(Target.SIZE_ORIGINAL)
                        .into(imgUpload);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}