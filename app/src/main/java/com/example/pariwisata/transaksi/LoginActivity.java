package com.example.pariwisata.transaksi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pariwisata.R;
import com.example.pariwisata.master.MsKabItemView;
import com.example.pariwisata.master.MsKabView;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.RequestPermissionHandler;
import com.example.pariwisata.utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private BaseApiService mApiService;
    private PrefUtil prefUtil;
    private RequestPermissionHandler mRequestPermissionHandler;
    private List<String> listPermissionsNeeded;
    private Button btnLogin, btnClearUser;
    private TextInputEditText eUserID, ePassword;
    private TextInputLayout inputLayoutUser, inputLayoutPasw;
    private TextView txtSignUp;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mRequestPermissionHandler = new RequestPermissionHandler();
            checkAndRequestPermissions();
            openPermission();
        }
        setContentView(R.layout.login);
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnLogin = (Button) findViewById(R.id.bLoginLogin);
        btnClearUser = (Button) findViewById(R.id.btnLoginClearUser);
        inputLayoutUser = (TextInputLayout)findViewById(R.id.input_layout_login_userid);
        inputLayoutPasw = (TextInputLayout)findViewById(R.id.input_layout_login_sandi);
        eUserID = (TextInputEditText)findViewById(R.id.eLoginUserID);
        ePassword = (TextInputEditText)findViewById(R.id.eLoginSandi);
        txtSignUp = (TextView) findViewById(R.id.txtLoginSignUp);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUser() && validatePasw(ePassword.length())){
                    requestLogin(eUserID.getText().toString(), ePassword.getText().toString());
                }
            }
        });

        btnClearUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eUserID.setText("");
                btnClearUser.setVisibility(View.INVISIBLE);
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(LoginActivity.this, RegisterActivity.class);
                i.putExtra("view", "LOGIN");
                i.putExtra("status", "ADD");
                startActivityForResult(i,3);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private void checkAndRequestPermissions() {
        int writeExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int lokasi = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int lokasi2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        listPermissionsNeeded = new ArrayList<>();
        if (writeExtStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readExtStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (lokasi != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (lokasi2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private void openPermission(){
        if (!listPermissionsNeeded.isEmpty()) {
            mRequestPermissionHandler.requestPermission(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    123, new RequestPermissionHandler.RequestPermissionListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(LoginActivity.this, "Request permission success", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailed() {
                            Toast.makeText(LoginActivity.this, "Request permission failed", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void requestLogin(String id, final String pasw){
        pDialog.setMessage("Login ...\nHarap Tunggu");
        showDialog();
        mApiService.loginRequest(id, pasw, "LOGIN")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    String nama = jsonRESULTS.getJSONObject("user").getString("vc_username")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_username");
                                    String uId = jsonRESULTS.getJSONObject("user").getString("c_userid")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_userid");
                                    String telp = jsonRESULTS.getJSONObject("user").getString("c_telp")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_telp");
                                    String status = jsonRESULTS.getJSONObject("user").getString("c_status")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("c_status");
                                    String email = jsonRESULTS.getJSONObject("user").getString("vc_email")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_email");
                                    String path = jsonRESULTS.getJSONObject("user").getString("vc_pathfoto")==null?"":
                                            jsonRESULTS.getJSONObject("user").getString("vc_pathfoto");
                                    String kdKab = "", nmKab = "", kdPrv="", nmPrv="", kdWisata="", nmWisata = "";
                                    /*if (status.equals("")){
                                        kdKab = jsonRESULTS.getJSONObject("user").getString("c_idkabkota")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_idkabkota");
                                        nmKab = jsonRESULTS.getJSONObject("user").getString("vc_namekabkota")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_namekabkota");
                                        kdPrv = jsonRESULTS.getJSONObject("user").getString("c_idprovinsi")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_idprovinsi");
                                        nmPrv = jsonRESULTS.getJSONObject("user").getString("vc_nameprovinsi")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_nameprovinsi");
                                    }else */if(status.equals("U") || status.equals("E")){
                                        kdWisata = jsonRESULTS.getJSONObject("user").getString("c_kodewisata")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("c_kodewisata");
                                        nmWisata = jsonRESULTS.getJSONObject("user").getString("vc_namawisata")==null?"":
                                                jsonRESULTS.getJSONObject("user").getString("vc_namawisata");
                                    }
                                    Integer countWisata = jsonRESULTS.getJSONObject("user").getInt("count_wisata");
                                    hideDialog();
                                    prefUtil.saveUserInfo(uId, nama, status, path, pasw, telp, email, countWisata,
                                            kdKab, nmKab, kdPrv, nmPrv, kdWisata, nmWisata);
                                    Toast.makeText(LoginActivity.this, jsonRESULTS.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    i.putExtra("status", "LOGIN");
                                    startActivity(i);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                } else {
                                    hideDialog();
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(LoginActivity.this, error_message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(LoginActivity.this, "GAGAL LOGIN", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(LoginActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateUser() {
        boolean value;
        if (eUserID.getText().toString().isEmpty()){
            value=false;
            requestFocus(eUserID);
            inputLayoutUser.setError(getString(R.string.err_msg_user));
        } else{
            value=true;
            inputLayoutUser.setError(null);
        }
        return value;
    }

    private boolean validatePasw(int length) {
        boolean value = true;
        int minValue = 6;
        if (ePassword.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword);
            inputLayoutPasw.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw.setError("Max character password length is " + inputLayoutPasw.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw.setError("Min character password length is 6" );
        } else{
            value=true;
            inputLayoutPasw.setError(null);}
        return value;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        Utils.freeMemory();
        super.onDestroy();
        Utils.trimCache(this);
    }
}
