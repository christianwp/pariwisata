package com.example.pariwisata.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.pariwisata.R;
import com.example.pariwisata.list.ListKabView;
import com.example.pariwisata.list.ListWisataView;
import com.example.pariwisata.master.MsTempatWisataItemActivity;
import com.example.pariwisata.master.MsTenantActivity;
import com.example.pariwisata.master.MsUserActivity;
import com.example.pariwisata.model.master.MsUserModel;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private String kodeKab;
    private BaseApiService mApiService, mUploadService;
    private ProgressDialog pDialog;
    private PrefUtil prefUtil;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private static final int REQUEST_CODE_GALLERY = 0013;
    private String hasilFoto = "N", encodedString;
    private Uri selectedImage;
    private TextInputEditText eUserId, eNama, ePassword, ePassword2, eTelp, eEmail, eKab, eWisata;
    private TextInputLayout inputLayoutUserId, inputLayoutNama, inputLayoutPasw, inputLayoutPasw2, inputLayoutTelp,
            inputLayoutEmail, ilKab, ilWisata;
    private ImageView imgFoto;
    private Button btnRegister, btnClearUserId;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Spinner spTipe;
    private LinearLayout linearTipe, linKab, linWisata;
    private String view, status, kodeWisata;
    private int RESULT_WISATA = 29;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                view = i.getString("view");
                status = i.getString("status");
            } catch (Exception e) {}
        }
        kodeKab = null;
        mApiService         = Link.getAPIService();
        mUploadService      = Link.getImgProfileService();
        prefUtil            = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnRegister = (Button) findViewById(R.id.bRegisterDaftar);
        btnClearUserId = (Button) findViewById(R.id.btnRegisterClearUserID);
        imgFoto = (ImageView) findViewById(R.id.imgFotoProfile);
        inputLayoutUserId = (TextInputLayout)findViewById(R.id.input_layout_register_userid);
        inputLayoutNama = (TextInputLayout)findViewById(R.id.input_layout_register_nama);
        inputLayoutPasw = (TextInputLayout)findViewById(R.id.input_layout_register_sandi);
        inputLayoutPasw2 = (TextInputLayout)findViewById(R.id.input_layout_register_sandi2);
        inputLayoutTelp = (TextInputLayout)findViewById(R.id.input_layout_register_telp);
        inputLayoutEmail = (TextInputLayout)findViewById(R.id.input_layout_register_email);
        ilKab = (TextInputLayout)findViewById(R.id.input_layout_register_kab);
        ilWisata = (TextInputLayout)findViewById(R.id.input_layout_register_wisata);

        eUserId = (TextInputEditText)findViewById(R.id.eRegisterUserId);
        eNama = (TextInputEditText)findViewById(R.id.eRegisterNama);
        ePassword = (TextInputEditText)findViewById(R.id.eRegisterSandi);
        ePassword2 = (TextInputEditText)findViewById(R.id.eRegisterSandi2);
        eTelp = (TextInputEditText)findViewById(R.id.eRegisterTelp);
        eEmail = (TextInputEditText)findViewById(R.id.eRegisterEmail);
        eKab = (TextInputEditText)findViewById(R.id.eAddViewRegisterKab);
        eWisata = (TextInputEditText)findViewById(R.id.eAddViewRegisterWisata);

        spTipe = (Spinner)findViewById(R.id.spRegisterTipe);
        linearTipe = (LinearLayout)findViewById(R.id.linRegisterTipeUser);
        linKab = (LinearLayout)findViewById(R.id.linRegisterKab);
        linWisata = (LinearLayout)findViewById(R.id.linRegisterWisata);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if(!view.equals("LOGIN")){
            linearTipe.setVisibility(View.VISIBLE);
            linKab.setVisibility(View.VISIBLE);
            linWisata.setVisibility(View.VISIBLE);
            imgFoto.setVisibility(View.GONE);
            if(view.equals("EDIT")){
                String kodeUser = i.getString("kodeUser");
                linearTipe.setVisibility(View.GONE);
                requestLogin(kodeUser);
            }
        }else{
            linearTipe.setVisibility(View.GONE);
            linKab.setVisibility(View.GONE);
            linWisata.setVisibility(View.GONE);
            imgFoto.setVisibility(View.VISIBLE);
        }

        btnClearUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eUserId.setText("");
                btnClearUserId.setVisibility(View.INVISIBLE);
            }
        });

        eKab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eKab.setEnabled(false);
                hideKeyboard(v);
                pilihKab();
                eKab.setEnabled(true);
            }
        });

        eKab.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    eKab.setEnabled(false);
                    hideKeyboard(v);
                    pilihKab();
                    eKab.setEnabled(true);
                }
            }
        });

        eWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eWisata.setEnabled(false);
                hideKeyboard(v);
                pilihWisata();
                eWisata.setEnabled(true);
            }
        });

        eWisata.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    eWisata.setEnabled(false);
                    hideKeyboard(v);
                    pilihWisata();
                    eWisata.setEnabled(true);
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUserId(eUserId.length()) && validateNama(eNama.length()) && validateWisata() &&
                        validatePasw(ePassword.length()) && validatePasw2(ePassword2.length()) &&
                        validateTelp(eTelp.length()) && validateEmail(eEmail.length())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(hasilFoto.equals("N")){
                                        String tipe = "";
                                        if(String.valueOf(spTipe.getSelectedItem()).equals("UMKM")){
                                            tipe = "U";
                                        }else if(String.valueOf(spTipe.getSelectedItem()).equals("Admin Wisata")){
                                            tipe = "E";
                                        }
                                        if(status.equals("ADD")){
                                            requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                                    eTelp.getText().toString(), eEmail.getText().toString(),
                                                    view.equals("LOGIN")?"C":tipe,
                                                    tipe.equals("X")?"72":null, tipe.equals("X")?kodeKab:null, kodeWisata);
                                        }else{
                                            updateUser(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                                    eTelp.getText().toString(), eEmail.getText().toString(), tipe.equals("X")?kodeKab:null);
                                        }
                                    }else{
                                        uploadImage();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });

        spTipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                Object item = parent.getItemAtPosition(pos);
                if(String.valueOf(item.toString()).equals("Konsumen")){
                    linKab.setVisibility(View.GONE);
                }else if(String.valueOf(item.toString()).equals("UMKM")){
                    linKab.setVisibility(View.GONE);
                }else if(String.valueOf(item.toString()).equals("Admin Wisata")){
                    linKab.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void pilihWisata(){
        Intent i = new Intent(RegisterActivity.this, ListWisataView.class);
        i.putExtra("kode", "%");
        startActivityForResult(i, RESULT_WISATA);
    }
    private void pilihKab(){
        Intent i = new Intent(RegisterActivity.this, ListKabView.class);
        i.putExtra("kode", "72");
        startActivityForResult(i, 4);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void openImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY);
    }

    private void requestRegister(String nama, String pasw, String id, String telp, String email, String status,
                                 String prov, String kab, String kodewst){
        pDialog.setMessage("Registering ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.registerRequest(id, pasw, nama, tanggalNow, telp, email, status,
                        hasilFoto.equals("N")?null:eUserId.getText().toString()+".jpg", prov, kab, kodewst)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(RegisterActivity.this, "BERHASIL REGISTRASI", Toast.LENGTH_LONG).show();
                                    if(!view.equals("LOGIN")){
                                        Intent returnIntent = new Intent();
                                        setResult(Activity.RESULT_OK,returnIntent);
                                    }
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(RegisterActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(RegisterActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void requestLogin(String id){
        pDialog.setMessage("Ambil Data User ...\nHarap Tunggu");
        showDialog();
        mApiService.viewUser(id, "VIEW")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                int sucses= jsonRESULTS.getInt("success");
                                JSONArray JsonArray = jsonRESULTS.getJSONArray("uploade");
                                if(sucses==0){
                                    hideDialog();
                                    Toast.makeText(RegisterActivity.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }else if(sucses<0){
                                    String msg= jsonRESULTS.getString("message");
                                    hideDialog();
                                    Toast.makeText(RegisterActivity.this,msg, Toast.LENGTH_SHORT).show();
                                }else{
                                    MsUserModel colums 	= new MsUserModel();
                                    for (int i = 0; i <JsonArray.length(); i++) {
                                        Object object = JsonArray.get(i);
                                        colums.setKodeUser((String)((JSONObject) object).get("c_userid"));
                                        colums.setNamaUser((String)((JSONObject) object).get("vc_username"));
                                        colums.setEmail((String)((JSONObject) object).get("vc_email"));
                                        colums.setTelp((String)((JSONObject) object).get("c_telp"));
                                        colums.setNamaKab(((JSONObject) object).get("vc_namekabkota").toString().equals("null")?null:
                                                (String)((JSONObject) object).get("vc_namekabkota"));
                                        colums.setKodeKab(((JSONObject) object).get("c_idkabkota").toString().equals("null")?null:
                                                (String)((JSONObject) object).get("c_idkabkota"));
                                        colums.setNamaWisata(((JSONObject) object).get("vc_namawisata").toString().equals("null")?null:
                                                (String)((JSONObject) object).get("vc_namawisata"));
                                        colums.setKodeWisata(((JSONObject) object).get("c_kodewisata").toString().equals("null")?null:
                                                (String)((JSONObject) object).get("c_kodewisata"));
                                        colums.setPasw((String)((JSONObject) object).get("vc_password"));
                                        colums.setTipe((String)((JSONObject) object).get("c_status"));
                                    }
                                    eUserId.setText(colums.getKodeUser());
                                    eNama.setText(colums.getNamaUser());
                                    eTelp.setText(colums.getTelp());
                                    eKab.setText(colums.getNamaKab());
                                    kodeKab = colums.getKodeKab();
                                    eEmail.setText(colums.getEmail());
                                    ePassword.setText(colums.getPasw());
                                    ePassword2.setText(colums.getPasw());
                                    kodeWisata = colums.getKodeWisata();
                                    eWisata.setText(colums.getNamaWisata());
                                    if(colums.getTipe().equals("U")){//UMKM
                                        spTipe.setSelection(2);
                                        linKab.setVisibility(View.GONE);
                                        linWisata.setVisibility(View.GONE);
                                    }else if(colums.getTipe().equals("X")){
                                        spTipe.setSelection(1);
                                        linKab.setVisibility(View.VISIBLE);
                                        linWisata.setVisibility(View.GONE);
                                    }else if(colums.getTipe().equals("E")){
                                        spTipe.setSelection(1);
                                        linKab.setVisibility(View.VISIBLE);
                                        linWisata.setVisibility(View.VISIBLE);
                                    }
                                    eUserId.setEnabled(false);
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(RegisterActivity.this, "GAGAL MENGAMBIL DATA", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(RegisterActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateUser(String nama, String pasw, String kodeuser, String telp, String email, String kab){
        pDialog.setMessage("Registering ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        PrefUtil pref = new PrefUtil(this);
        String userId = "";
        try{
            SharedPreferences shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
        }catch (Exception e){e.getMessage();}
        mApiService.updateUser(kodeuser, pasw, nama, telp, email, "UPDATE", kab,
                        tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(RegisterActivity.this, "Data berhasil diupdate", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(RegisterActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "UPDATE DATA GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "UPDATE DATA GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(RegisterActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    /*private boolean validateKab() {
        boolean value;
        if(!view.equals("LOGIN")) {
            if (String.valueOf(spTipe.getSelectedItem()).equals("Admin Wisata")) {
                if (eKab.getText().toString().isEmpty()) {
                    value = false;
                    ilKab.setError(getString(R.string.err_msg_namakab));
                } else {
                    value = true;
                    ilKab.setError(null);
                }
            } else {
                value = true;
                ilKab.setError(null);
            }
        }else{
            value = true;
            ilKab.setError(null);
        }
        return value;
    }*/

    private boolean validateWisata() {
        boolean value;
        if(!view.equals("LOGIN")) {
            if (String.valueOf(spTipe.getSelectedItem()).equals("UMKM")) {
                if (eWisata.getText().toString().isEmpty()) {
                    value = false;
                    ilWisata.setError(getString(R.string.err_msg_wisata));
                } else {
                    value = true;
                    ilWisata.setError(null);
                }
            } else {
                value = true;
                ilWisata.setError(null);
            }
        }else{
            value = true;
            ilWisata.setError(null);
        }
        return value;
    }

    private boolean validateUserId(int length) {
        boolean value;
        if (eUserId.getText().toString().isEmpty()){
            value=false;
            requestFocus(eUserId);
            inputLayoutUserId.setError(getString(R.string.err_msg_user));
        } else if (length > inputLayoutUserId.getCounterMaxLength()) {
            value=false;
            inputLayoutUserId.setError("Max character User ID length is " + inputLayoutUserId.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutUserId.setError(null);
        }
        return value;
    }

    private boolean validatePasw(int length) {
        boolean value=true;
        int minValue = 6;
        if (ePassword.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword);
            inputLayoutPasw.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw.setError("Max character password length is " + inputLayoutPasw.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw.setError("Min character password length is 6" );
        } else{
            value=true;
            inputLayoutPasw.setError(null);}
        return value;
    }

    private boolean validatePasw2(int length) {
        boolean value=true;
        int minValue = 6;
        if (ePassword2.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword2);
            inputLayoutPasw2.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw2.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw2.setError("Max character password length is " + inputLayoutPasw2.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw2.setError("Min character password length is 6" );
        } else if(!ePassword2.getText().toString().equals(ePassword.getText().toString())){
            value=false;
            inputLayoutPasw2.setError("Confirm Password is wrong");
        }else{
            value=true;
            inputLayoutPasw2.setError(null);
        }
        return value;
    }

    private boolean validateTelp(int length) {
        boolean value=true;
        if (eTelp.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eTelp);
            inputLayoutTelp.setError(getString(R.string.err_msg_telp));
        } else if (length > inputLayoutTelp.getCounterMaxLength()) {
            value=false;
            inputLayoutTelp.setError("Max character name length is " + inputLayoutTelp.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutTelp.setError(null);
        }
        return value;
    }

    private boolean validateEmail(int length) {
        boolean value=true;
        if (eEmail.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eEmail);
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
        } else if (length > inputLayoutEmail.getCounterMaxLength()) {
            value=false;
            inputLayoutEmail.setError("Max character name length is " + inputLayoutEmail.getCounterMaxLength());
        }else {
            value= Utils.isEmailValid(eEmail.getText().toString());
            if(value){
                inputLayoutEmail.setError(null);
            }else{
                inputLayoutEmail.setError("Email Tidak Valid");
            }
        }
        return value;
    }

    private boolean validateNama(int length) {
        boolean value=true;
        if (eNama.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eNama);
            inputLayoutNama.setError(getString(R.string.err_msg_nama));
        } else if (length > inputLayoutNama.getCounterMaxLength()) {
            value=false;
            inputLayoutNama.setError("Max character name length is " + inputLayoutNama.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutNama.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        Utils.freeMemory();
        super.onDestroy();
        Utils.trimCache(this);
    }

    private void uploadImage() {
        if (selectedImage != null) {
            pDialog.setMessage("Uploading Image...");
            showDialog();
            StorageReference ref = storageReference.child("profile/"+ (eUserId.getText().toString()+".jpg"));

            ref.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(
                                UploadTask.TaskSnapshot taskSnapshot){
                            String tipe = "";
                            if(String.valueOf(spTipe.getSelectedItem()).equals("UMKM")){
                                tipe = "U";
                            }else if(String.valueOf(spTipe.getSelectedItem()).equals("Admin Wisata")){
                                tipe = "E";
                            }
                            if(status.equals("ADD")){
                                requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                        eTelp.getText().toString(), eEmail.getText().toString(),
                                        view.equals("LOGIN")?"C":tipe,
                                        tipe.equals("X")?"72":null, tipe.equals("X")?kodeKab:null, kodeWisata);
                            }else{
                                updateUser(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                        eTelp.getText().toString(), eEmail.getText().toString(), tipe.equals("X")?kodeKab:null);
                            }
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(RegisterActivity.this,"Failed " + e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4) {
            if(resultCode == RESULT_OK) {
                eKab.setText(data.getStringExtra("namaKab"));
                kodeKab = data.getStringExtra("kodeKab");
            }
        }
        if(requestCode == RESULT_WISATA) {
            if(resultCode == RESULT_OK) {
                kodeWisata = data.getStringExtra("kodeWisata");
                eWisata.setText(data.getStringExtra("namaWisata"));
            }
        }
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto = "Y";
                selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                /*Utils.getCycleImage("file:///"+picturePath, imgFoto, this);
                String fileNameSegments[] = picturePath.split("/");
                Bitmap myImg = BitmapFactory.decodeFile(picturePath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                myImg.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, Base64.DEFAULT);*/
                imgFoto.setBackgroundResource(0);
                Glide.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .into(imgFoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
