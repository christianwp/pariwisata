package com.example.pariwisata.transaksi;

import static com.example.pariwisata.utilities.Utils.getDecimalFormattedString;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.list.ListMenuView;
import com.example.pariwisata.model.TransactionStatus;
import com.example.pariwisata.model.transaksi.KeranjangItemDetailModel;
import com.example.pariwisata.model.transaksi.KeranjangItemModel;
import com.example.pariwisata.model.transaksi.KeranjangModel;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class KeranjangItemDetailActivity extends AppCompatActivity {

    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private ImageView imgBack, imgSave;
    private TextInputEditText edMenu, edharga, edQty, edTotal, edKet;
    private KeranjangModel model;
    private KeranjangItemDetailModel detailmodel;
    private KeranjangItemModel itemmodel;
    private String status;
    private SharedPreferences shared;
    private String level, userId;
    private PrefUtil prefUtil;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_keranjang_item);
        prefUtil = new PrefUtil(this);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("statusItem");
                model = (KeranjangModel)i.getSerializable("model");
                itemmodel = (KeranjangItemModel)i.getSerializable("itemModel");
            } catch (Exception e) {}
        }

        imgBack = (ImageView) findViewById(R.id.imgViewTransItemDetailBack);
        imgSave = (ImageView) findViewById(R.id.imgViewTransItemDetailSave);
        edMenu = (TextInputEditText)findViewById(R.id.eAddViewTransItemDetailNamaMenu);
        edharga = (TextInputEditText) findViewById(R.id.eAddViewTransItemDetailHarga);
        edQty = (TextInputEditText) findViewById(R.id.eAddViewTransItemDetailQty);
        edTotal = (TextInputEditText) findViewById(R.id.eAddViewTransItemDetailTotal);
        edKet = (TextInputEditText) findViewById(R.id.eAddViewTransItemDetailKet);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        edMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edMenu.setEnabled(false);
                hideKeyboard(v);
                pilihMenu();
                edMenu.setEnabled(true);
            }
        });

        edMenu.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edMenu.setEnabled(false);
                    hideKeyboard(v);
                    pilihMenu();
                    edMenu.setEnabled(true);
                }
            }
        });

        edQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                try{
                    edQty.removeTextChangedListener(this);
                    String value = edQty.getText().toString();
                    if (value != null && !value.equals("")){
                        if(value.startsWith(".")){
                            edQty.setText("0.");
                        }
                        /*if(value.startsWith("0") && !value.startsWith("0.")){
                            edNominal.setText("");
                        }*/
                        String str = edQty.getText().toString().replaceAll(",", "");
                        Integer val = Integer.parseInt(str);
                        if (!value.equals(""))
                            edQty.setText(getDecimalFormattedString(val.toString()));
                        edQty.setSelection(edQty.getText().toString().length());
                        BigDecimal total = detailmodel.getHarga().multiply(new BigDecimal(edQty.getText().toString()));
                        edTotal.setText(String.valueOf(formatter.format(total)));
                    }
                    edQty.addTextChangedListener(this);
                    return;
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    edQty.addTextChangedListener(this);
                }
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(status.equals("ADD")){
                        initModel();
                        if(validateForInsert()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemDetailActivity.this);
                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Simpan data ini?")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            addItem(detailmodel);

                                            Intent returnIntent = new Intent();
                                            returnIntent.putExtra("model", model);
                                            setResult(Activity.RESULT_OK,returnIntent);
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }else if(status.equals("EDIT")){
                        initModel();
                        if(validateForUpdate()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemDetailActivity.this);
                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Update data ini?")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            modifyItem(detailmodel);

                                            Intent returnIntent = new Intent();
                                            returnIntent.putExtra("model", model);
                                            setResult(Activity.RESULT_OK,returnIntent);
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }else if(status.equals("DELETE")){
                        if(validateForUpdate()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemDetailActivity.this);
                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Hapus data ini?")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            removeItem(itemmodel);

                                            Intent returnIntent = new Intent();
                                            returnIntent.putExtra("model", model);
                                            setResult(Activity.RESULT_OK,returnIntent);
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }else{
                        Toast.makeText(KeranjangItemDetailActivity.this, "INVALID STATUS", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception ex){
                    Toast.makeText(KeranjangItemDetailActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        detailmodel = new KeranjangItemDetailModel();
        if(status.equals("ADD")){
            imgSave.setImageResource(R.mipmap.ic_check_circle_white_36dp);
            if(model.getTransactionStatus().isTransModifyAllowed()) {
                edMenu.setEnabled(true);
                edQty.setEnabled(true);
                edKet.setEnabled(true);
                imgSave.setVisibility(View.VISIBLE);
            }else{
                edMenu.setEnabled(false);
                edQty.setEnabled(false);
                edKet.setEnabled(false);
                imgSave.setVisibility(View.GONE);
            }
        }else if(status.equals("EDIT")){
            setValues();
            setModelToView();
            imgSave.setImageResource(R.mipmap.ic_check_circle_white_36dp);
            if(model.getTransactionStatus().isTransModifyAllowed()){
                edMenu.setEnabled(false);
                edQty.setEnabled(true);
                edKet.setEnabled(true);
                imgSave.setVisibility(View.VISIBLE);
            }else{
                edMenu.setEnabled(false);
                edQty.setEnabled(false);
                edKet.setEnabled(false);
                imgSave.setVisibility(View.GONE);
            }
        }else if(status.equals("DELETE")){
            setValues();
            setModelToView();
            edMenu.setEnabled(false);
            edQty.setEnabled(false);
            edKet.setEnabled(false);
            imgSave.setImageResource(R.mipmap.ic_delete_forever_white_36dp);
            if(model.getTransactionStatus().isTransDeleteAllowed()){
                imgSave.setVisibility(View.VISIBLE);
            }else{
                imgSave.setVisibility(View.GONE);
            }
        }else{
            Toast.makeText(KeranjangItemDetailActivity.this, "INVALID STATUS", Toast.LENGTH_LONG).show();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED,returnIntent);
            finish();
        }
    }

    private KeranjangModel addItem(KeranjangItemDetailModel detailmodel){
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        BigDecimal total = BigDecimal.ZERO;
        KeranjangItemModel itemModel = new KeranjangItemModel();
        itemModel.setKodeMenu(detailmodel.getKodeMenu());
        itemModel.setNamaMenu(detailmodel.getNamaMenu());
        itemModel.setHarga(detailmodel.getHarga());
        itemModel.setQty(detailmodel.getQty());
        itemModel.setTotal(detailmodel.getHarga().multiply(new BigDecimal(detailmodel.getQty())));
        itemModel.setKeterangan(detailmodel.getKeterangan());
        itemModel.setStatus(KeranjangItemModel.Status.NEW);
        model.addItem(itemModel);
        for(KeranjangItemModel item:model.getItemList()){
            BigDecimal hasil = item.getHarga().multiply(new BigDecimal(item.getQty()));
            total = total.add(hasil);
        }
        model.getHeaderModel().setKodeUser(userId);
        model.getHeaderModel().setTanggal(tanggalNow);
        model.getHeaderModel().setNilai(total);
        return model;
    }

    private KeranjangModel modifyItem(KeranjangItemDetailModel detailmodel){
        BigDecimal total = BigDecimal.ZERO;
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        for(KeranjangItemModel itemModel:model.getItemList()){
            if(itemModel.getKodeMenu().trim().equals(detailmodel.getKodeMenu().trim())){
                itemModel.setQty(detailmodel.getQty());
                itemModel.setKeterangan(detailmodel.getKeterangan());
                itemModel.setTotal(detailmodel.getHarga().multiply(new BigDecimal(detailmodel.getQty())));
                switch (itemModel.getStatus()){
                    case NEW:
                    case NEW_MODIFIED:
                        itemModel.setStatus(KeranjangItemModel.Status.NEW_MODIFIED);
                        break;
                    case OLD:
                    case OLD_MODIFIED:
                        itemModel.setStatus(KeranjangItemModel.Status.OLD_MODIFIED);
                        break;
                }
            }
        }
        for(KeranjangItemModel item:model.getItemList()){
            BigDecimal hasil = item.getHarga().multiply(new BigDecimal(item.getQty()));
            total = total.add(hasil);
        }
        model.getHeaderModel().setKodeUser(userId);
        model.getHeaderModel().setTanggal(tanggalNow);
        model.getHeaderModel().setNilai(total);
        return model;
    }

    private KeranjangModel removeItem(KeranjangItemModel itemModel){
        BigDecimal total = BigDecimal.ZERO;
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        model.removeItem(itemModel);
        for(KeranjangItemModel item:model.getItemList()){
            BigDecimal hasil = item.getHarga().multiply(new BigDecimal(item.getQty()));
            total = total.add(hasil);
        }
        model.getHeaderModel().setKodeUser(userId);
        model.getHeaderModel().setTanggal(tanggalNow);
        model.getHeaderModel().setNilai(total);
        return model;
    }

    private void setValues(){
        detailmodel.setKodeMenu(itemmodel.getKodeMenu());
        detailmodel.setNamaMenu(itemmodel.getNamaMenu());
        detailmodel.setHarga(itemmodel.getHarga());
        detailmodel.setQty(itemmodel.getQty());
        detailmodel.setTotal(itemmodel.getTotal());
        detailmodel.setKeterangan(itemmodel.getKeterangan());
    }

    private void initModel(){
        detailmodel.setQty(Integer.valueOf(edQty.getText().toString().replace(",","")));
        detailmodel.setKeterangan(edKet.getText().toString());
    }

    private void setModelToView(){
        edMenu.setText(detailmodel.getNamaMenu());
        edharga.setText(String.valueOf(formatter.format(detailmodel.getHarga())));
        edQty.setText(String.valueOf(detailmodel.getQty()));
        BigDecimal total = detailmodel.getHarga().multiply(new BigDecimal(detailmodel.getQty()));
        edTotal.setText(String.valueOf(formatter.format(total)));
        edKet.setText(detailmodel.getKeterangan());
    }

    private void pilihMenu(){
        Intent i = new Intent(KeranjangItemDetailActivity.this, ListMenuView.class);
        i.putExtra("kode", model.getHeaderModel().getKodeTenant());
        startActivityForResult(i, 6);
    }

    private boolean validateForInsert()throws Exception{
        if(detailmodel.getKodeMenu()==null || detailmodel.getKodeMenu().equals(""))
            throw new Exception("Menu belum dipilih!");
        if(detailmodel.getHarga()==null || detailmodel.getHarga().compareTo(BigDecimal.ZERO)==0)
            throw new Exception("Harga belum ada!");
        if(detailmodel.getQty().intValue()<0)
            throw new Exception("Qty belum diisi!");
        for(KeranjangItemModel itemModel:model.getItemList()){
            if(itemModel.getKodeMenu().trim().equals(detailmodel.getKodeMenu().trim()))
                throw new Exception("Menu "+ detailmodel.getNamaMenu().trim()+", sudah ada!");
        }
        return true;
    }

    private boolean validateForUpdate()throws Exception{
        if(detailmodel.getKodeMenu()==null || detailmodel.getKodeMenu().equals(""))
            throw new Exception("Menu belum dipilih!");
        if(!itemmodel.getKodeMenu().trim().equals(detailmodel.getKodeMenu().trim()))
            throw new Exception("Menu tidak boleh diubah!");
        if(detailmodel.getHarga()==null || detailmodel.getHarga().compareTo(BigDecimal.ZERO)==0)
            throw new Exception("Harga belum ada!");
        if(itemmodel.getHarga().compareTo(detailmodel.getHarga())!=0)
            throw new Exception("Harga tidak boleh diubah!");
        if(detailmodel.getQty().intValue()<0)
            throw new Exception("Qty belum diisi!");
        TransactionStatus latestStatus = getStatus();
        if (!latestStatus.isTransModifyAllowed())
            throw new Exception(latestStatus.getStatusString() + "\n Data tidak dapat diubah.");
        return true;
    }

    private TransactionStatus getStatus(){
        TransactionStatus returnValue;
        if(!model.getHeaderModel().getCdstatus().substring(0,1).equals("*"))
            returnValue = new TransactionStatus("Pesanan sudah dibayar!", false, false);
        else if(!model.getHeaderModel().getCdstatus().substring(1,2).equals("*"))
            returnValue = new TransactionStatus("Pesanan sedang dipersiapkan!", false, false);
        else if(!model.getHeaderModel().getCdstatus().substring(2,3).equals("*"))
            returnValue = new TransactionStatus("Pesanan sudah sampai ke customer!", false, false);
        else
            returnValue = TransactionStatus.NONE;
        return returnValue;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 6) {
            if(resultCode == RESULT_OK) {
                detailmodel.setKodeMenu(data.getStringExtra("kodeMenu"));
                detailmodel.setNamaMenu(data.getStringExtra("namaMenu"));
                detailmodel.setHarga(new BigDecimal(data.getStringExtra("harga").replace(",","")));
                edMenu.setText(data.getStringExtra("namaMenu"));
                edharga.setText(String.valueOf(formatter.format(detailmodel.getHarga())));
            }
        }
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}