package com.example.pariwisata.transaksi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pariwisata.R;

import androidx.fragment.app.Fragment;

public class FrgDefault extends Fragment {

    private View vupload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vupload     = inflater.inflate(R.layout.frg_default, container,false);
        return vupload;
    }
}
