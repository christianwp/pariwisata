package com.example.pariwisata.transaksi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pariwisata.R;
import com.example.pariwisata.laporan.LaporanTransaksiCustActivity;
import com.example.pariwisata.master.MsBankActivity;
import com.example.pariwisata.master.MsFasilitasActivity;
import com.example.pariwisata.master.MsJenisMenuActivity;
import com.example.pariwisata.master.MsKabView;
import com.example.pariwisata.master.MsKategoriActivity;
import com.example.pariwisata.master.MsTempatWisataActivity;
import com.example.pariwisata.master.MsTempatWisataAdminActivity;
import com.example.pariwisata.master.MsTenantActivity;
import com.example.pariwisata.master.MsUserActivity;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private NavigationView navigationView;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String userId, username, telp, email, status, paswd, infoStatus;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
    private DrawerLayout drawer;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView txtId, txtNama;
    private boolean GpsStatus ;
    private LocationManager locationManager ;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_main);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        Intent i = getIntent();
        infoStatus = i.getStringExtra("status");
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            username = shared.getString(PrefUtil.NAME, null);
            telp = shared.getString(PrefUtil.TELP, null);
            email = shared.getString(PrefUtil.EMAIL, null);
            status = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){
            e.getMessage();
            //Crashlytics.logException(new Exception(e.getMessage()));
        }
        if(getIntent().getExtras()!=null){
            for(String key : getIntent().getExtras().keySet()){
                String value = getIntent().getExtras().getString(key);
                Log.d("TAG", "KEY : " + key + "Value : " + value);
            }
        }
        FirebaseApp.initializeApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseMessaging.getInstance().subscribeToTopic(userId);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        //String token = FirebaseInstanceId.getInstance().getToken();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pariwisata");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        txtId = (TextView)header.findViewById(R.id.txtNavHeaderIDUser);
        txtNama = (TextView)header.findViewById(R.id.txtNavHeaderNamaUser);
        txtId.setText("ID User: "+userId);
        txtNama.setText("Nama: "+username);

        if(status.equals("A")){
            menuaDMIN();
        }else if(status.equals("C")){
            menuCust();
        }else if(status.equals("E")){
            menuEditor();
        }else{//UMKM
            menuUmkm();
        }
        changeFragment(new FrgDefault());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(userId);
            pref.clear();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void menuCust(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.mskategori).setVisible(false);
        nav_Menu.findItem(R.id.mskab).setVisible(false);
        nav_Menu.findItem(R.id.mstenant).setVisible(false);
        nav_Menu.findItem(R.id.mswisata).setVisible(true);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.keranjang).setVisible(true);
        nav_Menu.findItem(R.id.order).setVisible(false);
        nav_Menu.findItem(R.id.bank).setVisible(false);
        nav_Menu.findItem(R.id.laporan).setVisible(false);
//        nav_Menu.findItem(R.id.msmenu).setVisible(false);
        nav_Menu.findItem(R.id.msjenismenu).setVisible(false);
        nav_Menu.findItem(R.id.msfasilitas).setVisible(false);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.profil).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    private void menuEditor(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.mskategori).setVisible(false);
        nav_Menu.findItem(R.id.mskab).setVisible(false);
        nav_Menu.findItem(R.id.mstenant).setVisible(false);
        nav_Menu.findItem(R.id.mswisata).setVisible(true);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.keranjang).setVisible(false);
        nav_Menu.findItem(R.id.order).setVisible(false);
        nav_Menu.findItem(R.id.bank).setVisible(false);
        nav_Menu.findItem(R.id.laporan).setVisible(false);
//        nav_Menu.findItem(R.id.msmenu).setVisible(false);
        nav_Menu.findItem(R.id.msjenismenu).setVisible(false);
        nav_Menu.findItem(R.id.msfasilitas).setVisible(false);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.profil).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    private void menuUmkm(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.mskategori).setVisible(false);
        nav_Menu.findItem(R.id.mskab).setVisible(false);
        nav_Menu.findItem(R.id.mstenant).setVisible(true);
        nav_Menu.findItem(R.id.mswisata).setVisible(false);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.keranjang).setVisible(false);
        nav_Menu.findItem(R.id.order).setVisible(true);
        nav_Menu.findItem(R.id.bank).setVisible(false);
        nav_Menu.findItem(R.id.laporan).setVisible(true);
//        nav_Menu.findItem(R.id.msmenu).setVisible(true);
        nav_Menu.findItem(R.id.msjenismenu).setVisible(false);
        nav_Menu.findItem(R.id.msfasilitas).setVisible(false);
        nav_Menu.findItem(R.id.msuser).setVisible(false);
        nav_Menu.findItem(R.id.profil).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    private void menuaDMIN(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.mskategori).setVisible(true);
        nav_Menu.findItem(R.id.mskab).setVisible(true);
        nav_Menu.findItem(R.id.mstenant).setVisible(false);
        nav_Menu.findItem(R.id.msuser).setVisible(true);
        nav_Menu.findItem(R.id.keranjang).setVisible(false);
        nav_Menu.findItem(R.id.bank).setVisible(true);
        nav_Menu.findItem(R.id.order).setVisible(false);
        nav_Menu.findItem(R.id.laporan).setVisible(true);
//        nav_Menu.findItem(R.id.msmenu).setVisible(true);
        nav_Menu.findItem(R.id.msjenismenu).setVisible(true);
        nav_Menu.findItem(R.id.mswisata).setVisible(true);
        nav_Menu.findItem(R.id.msfasilitas).setVisible(true);
        nav_Menu.findItem(R.id.msuser).setVisible(true);
        nav_Menu.findItem(R.id.profil).setVisible(false);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        int id = item.getItemId();
        Integer data = checkAndRequestPermissionsStatus();
        if(data.intValue()!=0){
            Toast.makeText(MainActivity.this, "Akses permission harap diterima semua!", Toast.LENGTH_SHORT).show();
        }else{
            if (id == R.id.mskategori) {
                startActivity(new Intent(MainActivity.this, MsKategoriActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mskab) {
                startActivity(new Intent(MainActivity.this, MsKabView.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.msuser) {
                startActivity(new Intent(MainActivity.this, MsUserActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.bank) {
                startActivity(new Intent(MainActivity.this, MsBankActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mswisata) {
                if(status.equals("E")){
                    startActivity(new Intent(MainActivity.this, MsTempatWisataAdminActivity.class));
                }else{
                    startActivity(new Intent(MainActivity.this, MsTempatWisataActivity.class));
                }
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }  else if (id == R.id.msfasilitas) {
                startActivity(new Intent(MainActivity.this, MsFasilitasActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mstenant) {
                startActivity(new Intent(MainActivity.this, MsTenantActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.msjenismenu) {
                startActivity(new Intent(MainActivity.this, MsJenisMenuActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.keranjang) {
                startActivity(new Intent(MainActivity.this, KeranjangListActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.order) {
                startActivity(new Intent(MainActivity.this, OrderCustomerActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.laporan) {
                startActivity(new Intent(MainActivity.this, LaporanTransaksiCustActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.logout) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(userId);
                pref.clear();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            } else if (id == R.id.profil) {
                startActivityForResult(new Intent(MainActivity.this, ProfileActivity.class), 9);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Integer checkAndRequestPermissionsStatus() {
        Integer a = 0;
        int writeExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int lokasi = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int lokasi2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (writeExtStorage != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (readExtStorage != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (lokasi != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (lokasi2 != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        return a;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void CheckGpsStatus(){
        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void changeFragment(Fragment targetFragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.FrmMainMenu, targetFragment)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
        drawer.closeDrawer(GravityCompat.START);
    }
    //A:ADMIN, C:CUSTOMER, U: UMKM, E:ADMIN WISATA
}
