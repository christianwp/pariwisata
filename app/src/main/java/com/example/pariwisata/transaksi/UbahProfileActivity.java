package com.example.pariwisata.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.pariwisata.R;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahProfileActivity extends AppCompatActivity {

    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private static final int REQUEST_CODE_GALLERY = 0013;
    private String hasilFoto = "N", encodedString;
    private Uri selectedImage;
    private TextInputEditText eUserId, eNama, ePassword, eTelp, eEmail;
    private TextInputLayout inputLayoutUserId, inputLayoutNama, inputLayoutPasw, inputLayoutTelp,
            inputLayoutEmail;
    private ImageView imgFoto, imgBack;
    private Button btnRegister;
    private Calendar dateAndTime = Calendar.getInstance();
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private String level, userId, nama, email, telp, profile, pasw;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_profile_user);
        mApiService         = Link.getAPIService();
        //mUploadService      = Link.getImgProfileService();
        prefUtil = new PrefUtil(this);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
            nama = shared.getString(PrefUtil.NAME, null);
            email = shared.getString(PrefUtil.EMAIL, null);
            telp = shared.getString(PrefUtil.TELP, null);
            profile = shared.getString(PrefUtil.PATH, null);
            pasw = shared.getString(PrefUtil.PASW, null);
        }catch (Exception e){e.getMessage();}
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        imgBack = (ImageView) findViewById(R.id.img_updateprofile_back);
        btnRegister = (Button) findViewById(R.id.btn_updateprofile_save);
        imgFoto = (ImageView) findViewById(R.id.img_updateprofile_foto);
        inputLayoutUserId = (TextInputLayout)findViewById(R.id.il_updateprofile_id);
        inputLayoutNama = (TextInputLayout)findViewById(R.id.il_updateprofile_nama);
        inputLayoutPasw = (TextInputLayout)findViewById(R.id.il_updateprofile_pasw);
        inputLayoutTelp = (TextInputLayout)findViewById(R.id.il_updateprofile_telp);
        inputLayoutEmail = (TextInputLayout)findViewById(R.id.il_updateprofile_email);

        eUserId = (TextInputEditText)findViewById(R.id.e_updateprofile_id);
        eNama = (TextInputEditText)findViewById(R.id.e_updateprofile_nama);
        ePassword = (TextInputEditText)findViewById(R.id.e_updateprofile_pasw);
        eTelp = (TextInputEditText)findViewById(R.id.e_updateprofile_telp);
        eEmail = (TextInputEditText)findViewById(R.id.e_updateprofile_email);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUserId(eUserId.length()) && validateNama(eNama.length()) &&
                        validatePasw(ePassword.length()) &&
                        validateTelp(eTelp.length()) && validateEmail(eEmail.length())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(UbahProfileActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(hasilFoto.equals("N")){
                                        requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                                eTelp.getText().toString(), eEmail.getText().toString());
                                    }else{
                                        uploadImage();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });

        if(profile!=null && !profile.equals("")){
            pDialog.setMessage("Loading Image....");
            showDialog();
            imgFoto.setBackgroundResource(0);
            StorageReference ref = storageReference.child("profile/"+(userId)+".jpg");
            Glide.with(this /* context */)
                    .load(ref)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            hideDialog();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            hideDialog();
                            return false;
                        }
                    })
                    .into(imgFoto);
        }

        eUserId.setText(userId);
        eNama.setText(nama);
        eEmail.setText(email);
        eTelp.setText(telp);
        ePassword.setText(pasw);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void openImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY);
    }

    private void uploadImage() {
        if (selectedImage != null) {
            pDialog.setMessage("Uploading Image...");
            showDialog();
            StorageReference ref = storageReference.child("profile/"+ (eUserId.getText().toString()+".jpg"));

            ref.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(
                                UploadTask.TaskSnapshot taskSnapshot){
                            requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                    eTelp.getText().toString(), eEmail.getText().toString());
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(UbahProfileActivity.this,"Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    private void requestRegister(String nama, String pasw, String id, String telp, String email){
        pDialog.setMessage("Update Profile ...\nHarap Tunggu");
        showDialog();
        mApiService.updateProfile(id, pasw, nama, telp, email)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    Toast.makeText(UbahProfileActivity.this, "PROFILE BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(UbahProfileActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(UbahProfileActivity.this, "UPDATE PROFILE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(UbahProfileActivity.this, "UPDATE PROFILE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(UbahProfileActivity.this, "UPDATE PROFILE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(UbahProfileActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateUserId(int length) {
        boolean value;
        if (eUserId.getText().toString().isEmpty()){
            value=false;
            requestFocus(eUserId);
            inputLayoutUserId.setError(getString(R.string.err_msg_user));
        } else if (length > inputLayoutUserId.getCounterMaxLength()) {
            value=false;
            inputLayoutUserId.setError("Max character User ID length is " + inputLayoutUserId.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutUserId.setError(null);
        }
        return value;
    }

    private boolean validatePasw(int length) {
        boolean value=true;
        int minValue = 6;
        if (ePassword.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword);
            inputLayoutPasw.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw.setError("Max character password length is " + inputLayoutPasw.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw.setError("Min character password length is 6" );
        } else{
            value=true;
            inputLayoutPasw.setError(null);}
        return value;
    }

    private boolean validateTelp(int length) {
        boolean value=true;
        if (eTelp.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eTelp);
            inputLayoutTelp.setError(getString(R.string.err_msg_telp));
        } else if (length > inputLayoutTelp.getCounterMaxLength()) {
            value=false;
            inputLayoutTelp.setError("Max character name length is " + inputLayoutTelp.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutTelp.setError(null);
        }
        return value;
    }

    private boolean validateEmail(int length) {
        boolean value=true;
        if (eEmail.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eEmail);
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
        } else if (length > inputLayoutEmail.getCounterMaxLength()) {
            value=false;
            inputLayoutEmail.setError("Max character name length is " + inputLayoutEmail.getCounterMaxLength());
        }else {
            value= Utils.isEmailValid(eEmail.getText().toString());
            if(value){
                inputLayoutEmail.setError(null);
            }else{
                inputLayoutEmail.setError("Email Tidak Valid");
            }
        }
        return value;
    }

    private boolean validateNama(int length) {
        boolean value=true;
        if (eNama.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eNama);
            inputLayoutNama.setError(getString(R.string.err_msg_nama));
        } else if (length > inputLayoutNama.getCounterMaxLength()) {
            value=false;
            inputLayoutNama.setError("Max character name length is " + inputLayoutNama.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutNama.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
