package com.example.pariwisata.transaksi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.transaksi.AdpOrder;
import com.example.pariwisata.laporan.LaporanTransaksiCustActivity;
import com.example.pariwisata.list.ListTenantView;
import com.example.pariwisata.model.transaksi.OrderListModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OrderCustomerActivity extends AppCompatActivity {
    private ImageView imgBack;
    private AdpOrder adapter;
    private ListView lsvupload;
    private ArrayList<OrderListModel> columnlist= new ArrayList<OrderListModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeTenant;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private TextInputEditText edTenant;
    private Button btnProses;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_view);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack		= (ImageView)findViewById(R.id.imgOrderTenantBack);
        lsvupload	= (ListView)findViewById(R.id.listDaftarOrder);
        tvstatus	= (TextView)findViewById(R.id.txtListDaftarOrderStatus);
        edTenant	= (TextInputEditText) findViewById(R.id.eOrderListNamaTenant);
        btnProses = (Button)findViewById(R.id.btnOrderListRetrieve);

        adapter		= new AdpOrder(OrderCustomerActivity.this, R.layout.adp_order, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edTenant.setEnabled(false);
                hideKeyboard(v);
                pilihTenant();
                edTenant.setEnabled(true);
            }
        });

        edTenant.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edTenant.setEnabled(false);
                    hideKeyboard(v);
                    pilihTenant();
                    edTenant.setEnabled(true);
                }
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateTenant())
                    getData(Link.BASE_URL_API + Link.KERANJANG);
            }
        });

        lsvupload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                OrderListModel model = new OrderListModel();
                model = columnlist.get(position);
                Intent i = new Intent(OrderCustomerActivity.this, KeranjangItemActivity.class);
                i.putExtra("noTransaksi", model.getNoTransaksi());
                if(model.getCdstatus().substring(1,2).equals("*"))
                    i.putExtra("Status", "ORDER_TERIMA");
                else
                    i.putExtra("Status", "ORDER_KIRIM");
                i.putExtra("pemilikTenant", userId);
                startActivityForResult(i, 9);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private boolean validateTenant() {
        boolean value;
        if (edTenant.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(OrderCustomerActivity.this,"Tenant harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihTenant(){
        Intent i = new Intent(OrderCustomerActivity.this, ListTenantView.class);
        i.putExtra("kode", userId);
        i.putExtra("view", "PEMILIK");
        startActivityForResult(i, 2);
    }

    private void getData(String Url){
        pDialog.setMessage("Loading....");
        showDialog();
        Date tglFrom = Utils.getFirstTimeOfDay(Calendar.getInstance().getTime());
        Date tglTo = Utils.getLastTimeOfDay(Calendar.getInstance().getTime());
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            OrderListModel colums 	= new OrderListModel();
                            colums.setNoTransaksi((String)((JSONObject) object).get("c_kodetransaksi"));
                            colums.setTanggal((String)((JSONObject) object).get("dt_tanggal"));
                            colums.setNilai(new BigDecimal((String) ((JSONObject) object).get("n_nilai")));
                            colums.setCdstatus((String)((JSONObject) object).get("c_dstatus"));
                            colums.setIdUser((String)((JSONObject) object).get("c_userid"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(OrderCustomerActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(OrderCustomerActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(OrderCustomerActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(OrderCustomerActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(OrderCustomerActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(OrderCustomerActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(OrderCustomerActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("view", "VIEW_ORDER");
                params.put("kodeTenant", kodeTenant);
                params.put("tglFrom", df.format(tglFrom));
                params.put("tglTo", df.format(tglTo));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                kodeTenant = data.getStringExtra("kodeTenant");
                edTenant.setText(data.getStringExtra("namaTenant"));
            }
        }
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                adapter		= new AdpOrder(OrderCustomerActivity.this, R.layout.adp_order, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_PROV);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
