package com.example.pariwisata.transaksi;

import static com.example.pariwisata.utilities.Link.KERANJANG;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.transaksi.AdpKeranjangItem;
import com.example.pariwisata.master.MsTempatWisataItemActivity;
import com.example.pariwisata.model.TransactionStatus;
import com.example.pariwisata.model.TransactionToken;
import com.example.pariwisata.model.master.MsWisataFasilitasModel;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.model.master.MsWisataJadwalModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.model.transaksi.KeranjangHeaderModel;
import com.example.pariwisata.model.transaksi.KeranjangItemModel;
import com.example.pariwisata.model.transaksi.KeranjangListKriteriaModel;
import com.example.pariwisata.model.transaksi.KeranjangModel;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.JSONfunctions;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class KeranjangItemActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private AdpKeranjangItem adapter;
    private BaseApiService mApiService;
    private PrefUtil prefUtil;
    private ImageView imgBack, imgAdd, imgSave;
    private ListView lsvData;
    private SharedPreferences shared;
    private String level, userId, infoStatus, noTransaksi, pemilikTenant;
    private TextView edNoTrans, edTanggal, edTotal, edTenant;
    private KeranjangModel model, modelOptimisticLockCheck;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private ArrayList<KeranjangItemModel> columnList = new ArrayList<>();
    private static final String PROP_ORI_NO_TRANS = "oriNoTransaksi";
    private static final String PROP_ORI_MENU = "oriMenu";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_keranjang);
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                infoStatus = i.getString("Status");// ADD/EDIT
                pemilikTenant = i.getString("pemilikTenant");// ADD/EDIT
            } catch (Exception e) {}
        }

        imgBack = (ImageView) findViewById(R.id.imgTransPesanItemBack);
        imgSave = (ImageView) findViewById(R.id.imgTransPesanItemSave);
        edTanggal = (TextView) findViewById(R.id.txtTransPesanItemTanggal);
        edNoTrans = (TextView) findViewById(R.id.txtTransPesanItemNoTrans);
        edTotal = (TextView) findViewById(R.id.txtTransPesanItemNilai);
        edTenant = (TextView) findViewById(R.id.txtTransPesanItemTenant);
        lsvData = (ListView) findViewById(R.id.listTransPesanItem);
        imgAdd = (ImageView) findViewById(R.id.imgListTransPesanItemAdd);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(infoStatus.equals("ADD")){
                        if(validateForInsert()){
                            AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemActivity.this);
                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Simpan data ini?")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            pDialog.setMessage("Proses simpan data");
                                            showDialog();
                                            proses_simpan("Keranjang_insert.php");
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }else if(infoStatus.equals("EDIT") || infoStatus.equals("DELETE")){
                        if(validateForUpdate()){
                            optimisticLockCheck();
                        }
                    }else if(infoStatus.equals("ORDER_TERIMA") || infoStatus.equals("ORDER_KIRIM") ){
                        optimisticLockCheck();
                    }else{
                        Toast.makeText(KeranjangItemActivity.this, "INVALID STATUS", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception ex) {
                    Toast.makeText(KeranjangItemActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!infoStatus.equals("LAPORAN")) {
                    Intent i = new Intent(KeranjangItemActivity.this, KeranjangItemDetailActivity.class);
                    i.putExtra("statusItem", "ADD");
                    i.putExtra("model", model);
                    startActivityForResult(i, 8);
                }
            }
        });

        try {
            if (infoStatus.equals("ADD")) {
                model = (KeranjangModel) i.getSerializable("model");
                setModel(model);
            } else {
                model = new KeranjangModel();
                noTransaksi = i.getString("noTransaksi");
                requestForUpdate(true, noTransaksi, model);
            }
        }catch (Exception ex){
            Toast.makeText(KeranjangItemActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void requestForUpdate(boolean init, String noTransaksi, KeranjangModel data)throws Exception{
        pDialog.setMessage("Harap Tunggu");
        showDialog();
        requestForUpdateKeranjangVolley(Link.BASE_URL_API+KERANJANG, init, noTransaksi, data);
    }

    private void requestForUpdateKeranjangVolley(String url, boolean init, String noTrans, KeranjangModel data)throws Exception{
        pDialog.setMessage("Loading");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    boolean sucses= jsonrespon.getString("value").equals("false");
                    if (sucses){
                        initModel(jsonrespon, data, init);
                        if(!init){
                            String hash = data.getTransactionToken().getOriginHash();
                            if (!model.getTransactionToken().getOriginHash().equals(hash))
                                throw new Exception("Row changed between retrieve & update (header)");
                            if(infoStatus.equals("EDIT")) {
                                showDialogUpdate();
                            }else if(infoStatus.equals("ORDER_TERIMA")){
                                showDialogOrder("Terima pesanan ini?");
                            }else if(infoStatus.equals("ORDER_KIRIM")){
                                showDialogOrder("Selesaikan pesanan ini?");
                            }
                        }
                        hideDialog();
                    }else{
                        Toast.makeText(KeranjangItemActivity.this, "Tidak Ada Data", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    Toast.makeText(KeranjangItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(KeranjangItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                    e.printStackTrace();
                }
                //lsvupload.invalidate();
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(KeranjangItemActivity.this, "Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(KeranjangItemActivity.this, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(KeranjangItemActivity.this, "Check ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(KeranjangItemActivity.this, "Check NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(KeranjangItemActivity.this, "Check ParseError", Toast.LENGTH_LONG).show();
                }
                hideDialog();
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("noTransaksi", noTrans);
                params.put("view", "REQUEST");
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void sendNotification(final String pemilik, final String namaTenant, final String message){
        StringRequest register = new StringRequest(Request.Method.POST, Link.BASE_URL_NOTIF, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonrespon = new JSONObject(response);
                    JSONArray JsonArray = jsonrespon.getJSONArray("data");
                    if (JsonArray!=null){

                    }else{
                        //tvstatus.setText("Tidak Ada Data");
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    //tvstatus.setText("Check Koneksi Internet Anda");
                } else if (error instanceof AuthFailureError) {
                    //tvstatus.setText("AuthFailureError");
                } else if (error instanceof ServerError) {
                    //tvstatus.setText("Check ServerError");
                } else if (error instanceof NetworkError) {
                    //tvstatus.setText("Check NetworkError");
                } else if (error instanceof ParseError) {
                    //tvstatus.setText("Check ParseError");
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("topics", pemilik);
                params.put("message", message);
                if(infoStatus.equals("ORDER_TERIMA") || infoStatus.equals("ORDER_KIRIM")){
                    params.put("judul", ("Pembaruan pesanan " + namaTenant));
                }else {
                    params.put("judul", ("Order baru " + namaTenant));
                }
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void proses_simpan(String link){
        RequestTask task = new RequestTask();
        task.applicationContext = KeranjangItemActivity.this;
        task.execute(new String[] { Link.BASE_URL_API+link });
    }

    class RequestTask extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;
        protected Context applicationContext;

        @Override
        protected void onPreExecute() {
            //this.dialog = ProgressDialog.show(applicationContext,"Proses Menyimpan Data", "Loading...", true);
        }

        @Override
        protected String doInBackground(String... uri) {
            String responseString = null;
            responseString = saveData("Keranjang_insert.php");
            Log.d("TAGS","response:"+responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try{
                super.onPostExecute(result);
                JSONObject jsonRESULTS = new JSONObject(result);
                if (jsonRESULTS.getString("value").equals("false")){
                    String nomor = jsonRESULTS.getString("nomor");
                    sendNotification(pemilikTenant, ("Ada transaksi pemesanan dengan nomor "+nomor),
                            model.getHeaderModel().getNamaTenant());
                    hideDialog();
                    Toast.makeText(KeranjangItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }else{
                    hideDialog();
                    String message = jsonRESULTS.getString("message");
                    Toast.makeText(KeranjangItemActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }catch (JSONException e) {
                hideDialog();
                Toast.makeText(KeranjangItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private void proses_update(String link){
        RequestTaskUpdate task = new RequestTaskUpdate();
        task.applicationContext = KeranjangItemActivity.this;
        task.execute(new String[] { Link.BASE_URL_API+link });
    }

    class RequestTaskUpdate extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;
        protected Context applicationContext;

        @Override
        protected void onPreExecute() {
            //this.dialog = ProgressDialog.show(applicationContext,"Proses Menyimpan Data", "Loading...", true);
        }

        @Override
        protected String doInBackground(String... uri) {
            String responseString = null;
            responseString = saveData("Keranjang_update.php");
            Log.d("TAGS","response:"+responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try{
                super.onPostExecute(result);
                JSONObject jsonRESULTS = new JSONObject(result);
                if (jsonRESULTS.getString("value").equals("false")){
                    hideDialog();
                    Toast.makeText(KeranjangItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }else{
                    hideDialog();
                    String message = jsonRESULTS.getString("message");
                    Toast.makeText(KeranjangItemActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }catch (JSONException e) {
                hideDialog();
                Toast.makeText(KeranjangItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    public String saveData(String link){
        String sret="";
        Gson gson = new Gson();
        String json = gson.toJson(model);
        //System.out.println(json);
        try {
            JSONObject data = new JSONObject(json);
            sret = JSONfunctions.doPost(Link.BASE_URL_API + link, data);
            Log.d("TAG","response:"+sret);
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return sret;
    }

    private void showDialogUpdate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemActivity.this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Ubah data ini?")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pDialog.setMessage("Proses simpan data");
                        showDialog();
                        proses_update("Keranjang_update.php");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showDialogOrder(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(KeranjangItemActivity.this);
        builder.setTitle("Konfirmasi");
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pDialog.setMessage("Proses simpan data");
                        showDialog();
                        if(infoStatus.equals("ORDER_TERIMA")) {
                            order(Link.BASE_URL_API+ KERANJANG, model.getHeaderModel().getNoTransaksi(),userId);
                        }else if(infoStatus.equals("ORDER_KIRIM")) {
                            orderClosed(Link.BASE_URL_API+ KERANJANG, model.getHeaderModel().getNoTransaksi(),userId);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void order(String save, final String noTrans, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            boolean sucses= jsonrespon.getString("value").equals("false");
                            String message = jsonrespon.getString("message");
                            if (sucses ){
                                sendNotification(model.getHeaderModel().getKodeUser(), model.getHeaderModel().getNamaTenant(),
                                        ("Pesanan anda sedang diproses"));
                                Toast.makeText(KeranjangItemActivity.this, "DATA BERHASIL DIUPDATE", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK,returnIntent);
                                finish();
                            }else{
                                hideDialog();
                                Toast.makeText(KeranjangItemActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideDialog();
                            Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
                hideDialog();
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("noTransaksi", noTrans);
                params.put("kodeUser", userId);
                params.put("tglNow", tanggalNow);
                params.put("view", "UPDATE_ORDER_TERIMA");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void orderClosed(String save, final String noTrans, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            boolean sucses= jsonrespon.getString("value").equals("false");
                            String message = jsonrespon.getString("message");
                            if (sucses ){
                                sendNotification(model.getHeaderModel().getKodeUser(), model.getHeaderModel().getNamaTenant(),
                                        ("Selamat menikmati pesanan anda"));
                                Toast.makeText(KeranjangItemActivity.this, "DATA BERHASIL DIUPDATE", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK,returnIntent);
                                finish();
                            }else{
                                hideDialog();
                                Toast.makeText(KeranjangItemActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideDialog();
                            Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(KeranjangItemActivity.this, "UPDATE DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
                hideDialog();
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("noTransaksi", noTrans);
                params.put("kodeUser", userId);
                params.put("tglNow", tanggalNow);
                params.put("view", "UPDATE_CLOSE_ORDER");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void initModel(JSONObject jsonRESULTS, KeranjangModel data, boolean init)throws JSONException{
        String hashCode = "";
        JSONArray JsonArray = jsonRESULTS.getJSONArray("item");
        for (int i = 0; i < JsonArray.length(); i++) {
            JSONObject object = JsonArray.getJSONObject(i);
            KeranjangItemModel item = new KeranjangItemModel();
            item.setNoTransaksi(object.getString("c_kodetransaksi"));
            item.setKodeMenu(object.getString("c_kodemenu"));
            item.setNamaMenu(object.getString("vc_namamenu"));
            item.setHarga(new BigDecimal(object.getDouble("n_harga")));
            item.setQty(object.getInt("n_qty"));
            item.setTotal(new BigDecimal(object.getDouble("n_total")));
            item.setKeterangan(object.getString("vc_keterangan"));
            item.setStatus(KeranjangItemModel.Status.OLD);
            hashCode = hashCode + item.computeHash();
            data.addItem(item);
        }
        KeranjangHeaderModel headerModel = new KeranjangHeaderModel();
        headerModel.setNoTransaksi(jsonRESULTS.getString("c_kodetransaksi"));
        headerModel.setTanggal(jsonRESULTS.getString("dt_tanggal"));
        headerModel.setKodeTenant(jsonRESULTS.getString("c_kodetenant"));
        headerModel.setNamaTenant(jsonRESULTS.getString("vc_namatenant"));
        //headerModel.setLokasi(jsonRESULTS.getString("vc_lokasi"));
        headerModel.setKodeUser(jsonRESULTS.getString("c_userid"));
        headerModel.setNamaUser(jsonRESULTS.getString("vc_username"));
        headerModel.setNilai(new BigDecimal(jsonRESULTS.getDouble("n_nilai")));
        headerModel.setCdstatus(jsonRESULTS.getString("c_dstatus"));
        Map<String, String> stringOriProps = new HashMap<String, String>();
        stringOriProps.put(PROP_ORI_NO_TRANS, headerModel.getNoTransaksi().trim());
        TransactionToken token = new TransactionToken();
        token.setOriginStringProperties(stringOriProps);
        token.setOriginHash(hashCode + headerModel.computeHash());
        data.setHeaderModel(headerModel);
        data.setTransactionToken(token);
        data.setTransactionStatus(getStatus(data));
        if(init)
            setModel(data);
    }

    private boolean validateForInsert()throws Exception{
        KeranjangHeaderModel headerModel = model.getHeaderModel();
        if(headerModel.getKodeTenant()==null)
            throw new Exception("Tenant blm diisi");
        if(model.getItemList().size()==0)
            throw new Exception("Menu belum ada");
        return true;
    }

    private boolean validateForUpdate()throws Exception{
        KeranjangHeaderModel headerModel = model.getHeaderModel();
        if(headerModel.getKodeTenant()==null)
            throw new Exception("Tenant blm diisi");
        if(model.getItemList().size()==0)
            throw new Exception("Menu belum ada");
        TransactionStatus latestStatus = getStatus(model);
        if (!latestStatus.isTransModifyAllowed())
            throw new Exception(latestStatus.getStatusString() + "\n Data tidak dapat diubah.");
        return true;
    }

    private TransactionStatus getStatus(KeranjangModel data){
        TransactionStatus returnValue;
        if(!data.getHeaderModel().getCdstatus().substring(0,1).equals("*"))
            returnValue = new TransactionStatus("Pesanan sudah dibayar!", false, false);
        else if(!data.getHeaderModel().getCdstatus().substring(1,2).equals("*"))
            returnValue = new TransactionStatus("Pesanan sedang dipersiapkan!", false, false);
        else if(!data.getHeaderModel().getCdstatus().substring(2,3).equals("*"))
            returnValue = new TransactionStatus("Pesanan sudah sampai ke customer!", false, false);
        else
            returnValue = TransactionStatus.NONE;
        return returnValue;
    }

    private void setModel(KeranjangModel model){
        BigDecimal total = BigDecimal.ZERO;
        KeranjangHeaderModel header = model.getHeaderModel();
        edNoTrans.setText(header.getNoTransaksi());
        Date tglTrans = Calendar.getInstance().getTime();
        try{
            tglTrans = dfSave.parse(header.getTanggal());
            edTanggal.setText(sdf1.format(tglTrans));
        }catch (Exception ex){}
        edTenant.setText(header.getNamaTenant());
        columnList = new ArrayList<>();
        for(KeranjangItemModel itemModel:model.getItemList()){
            BigDecimal nilai = itemModel.getHarga().multiply(new BigDecimal(itemModel.getQty()));
            total = total.add(nilai);
            columnList.add(itemModel);
        }
        edTotal.setText(String.valueOf(formatter.format(total.setScale(0))));
        if (infoStatus.equals("LAPORAN")) {
            imgAdd.setVisibility(View.GONE);
            imgSave.setVisibility(View.GONE);
        }else if (infoStatus.equals("ORDER_TERIMA") || infoStatus.equals("ORDER_KIRIM")) {
            imgAdd.setVisibility(View.GONE);
            imgSave.setVisibility(View.VISIBLE);
        }else{
            if (model.getTransactionStatus().isTransModifyAllowed()) {
                imgAdd.setVisibility(View.VISIBLE);
                imgSave.setVisibility(View.VISIBLE);
            } else {
                imgAdd.setVisibility(View.GONE);
                imgSave.setVisibility(View.GONE);
            }
        }
        adapter	= new AdpKeranjangItem(KeranjangItemActivity.this, R.layout.adp_keranjang_item, columnList, level,
                model);
        lsvData.setAdapter(adapter);
    }

    private KeranjangModel optimisticLockCheck() throws Exception {
        String oriNoTrans = model.getTransactionToken().getOriginStringProperty(PROP_ORI_NO_TRANS);
        modelOptimisticLockCheck = new KeranjangModel();
        requestForUpdate(false, oriNoTrans, modelOptimisticLockCheck);
        return model;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 8) {
            if(resultCode == RESULT_OK) {
                model = (KeranjangModel) data.getExtras().getSerializable("model");
                setModel(model);
            }
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}