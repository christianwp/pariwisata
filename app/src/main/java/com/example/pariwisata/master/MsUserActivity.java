package com.example.pariwisata.master;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsUser;
import com.example.pariwisata.model.master.MsUserModel;
import com.example.pariwisata.transaksi.RegisterActivity;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MsUserActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsUser adapter;
    private ListView lsvupload;
    private ArrayList<MsUserModel> columnlist= new ArrayList<MsUserModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private Button btnProses;
    private Spinner sptipe;
    private ProgressDialog pDialog;
    private int RESULT_DATA = 9;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_user);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterUserAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterUserBack);
        sptipe = (Spinner) findViewById(R.id.spUserTipe);
        lsvupload	= (ListView)findViewById(R.id.listMasterUser);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterUserStatus);
        btnProses = (Button)findViewById(R.id.btnMasterUserRetrieve);

        adapter		= new AdpMsUser(MsUserActivity.this, R.layout.adp_ms_user, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(level.equals("A")){
                    Intent i  = new Intent(MsUserActivity.this, RegisterActivity.class);
                    i.putExtra("view", "ADD");
                    i.putExtra("status", "ADD");
                    startActivityForResult(i, RESULT_DATA);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(MsUserActivity.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tipe;
                if(String.valueOf(sptipe.getSelectedItem()).equals("Konsumen")){
                    tipe = "C";
                }else if(String.valueOf(sptipe.getSelectedItem()).equals("UMKM")){
                    tipe = "U";
                }else if(String.valueOf(sptipe.getSelectedItem()).equals("Admin Wisata")){
                    tipe = "E";
                }else{
                    tipe = "";
                }
                getData(Link.BASE_URL_API + Link.MS_USER, tipe);
            }
        });

        /*if(!level.equals("A"))
            ImgAdd.setVisibility(View.GONE);
        else
            ImgAdd.setVisibility(View.VISIBLE);*/
    }

    private void getData(String Url, final String tipeUser){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else if(sucses<0){
                        String msg= jsonrespon.getString("message");
                        tvstatus.setVisibility(View.VISIBLE);
                        hideDialog();
                        Toast.makeText(MsUserActivity.this,msg, Toast.LENGTH_SHORT).show();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsUserModel colums 	= new MsUserModel();
                            colums.setKodeUser((String)((JSONObject) object).get("c_userid"));
                            colums.setNamaUser((String)((JSONObject) object).get("vc_username"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsUserActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsUserActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsUserActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsUserActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsUserActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsUserActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsUserActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tipe", tipeUser);
                params.put("view", "LIST");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<>();
        adapter		= new AdpMsUser(MsUserActivity.this, R.layout.adp_ms_user, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<>();
                adapter		= new AdpMsUser(MsUserActivity.this, R.layout.adp_ms_user, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                String tipe;
                if(String.valueOf(sptipe.getSelectedItem()).equals("Konsumen")){
                    tipe = "C";
                }else if(String.valueOf(sptipe.getSelectedItem()).equals("UMKM")){
                    tipe = "U";
                }else if(String.valueOf(sptipe.getSelectedItem()).equals("Admin Wisata")){
                    tipe = "E";
                }else{
                    tipe = "";
                }
                getData(Link.BASE_URL_API + Link.MS_USER, tipe);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
