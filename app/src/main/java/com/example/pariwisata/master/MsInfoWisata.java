package com.example.pariwisata.master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;

import com.example.pariwisata.R;
import com.example.pariwisata.model.master.MsWisataFasilitasModel;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.model.master.MsWisataJadwalModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.model.master.SliderData;
import com.example.pariwisata.service.Anim;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsInfoWisata extends AppCompatActivity implements OnMapReadyCallback, RatingDialogListener {

    private String kodeWisata, namaWisata;
    private MsWisataModel model;
    private PrefUtil prefUtil;
    private ProgressDialog pDialog;
    private BaseApiService mApiService;
    private ImageView imgBack, imgRating, imgViewRating, imgExpand;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private TextView txtNama, txtKode, txtDeskripsi, txtProv, txtKab, txtTelp,
        txtFas, txtAlamat, txtTelfWisata, txtSms, txtWa, txtKategori, txtCountUser, txtNilaiRating, txtEvent;
    private SliderView sliderView;
    private double latt, longt;
    private ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();
    private SliderAdapter adapter;
    private GoogleMap mGoogleMap;
    private float zoomLevel = (float) 16.0;
    private LocationRequest mLocationRequest;
    private static final long INTERVAL = 1000 * 2 * 1; //2 detik
    private static final long FASTEST_INTERVAL = 1000 * 1 * 1; // 1 detik
    private SharedPreferences shared;
    private String level, userId;
    private boolean partisipasi;
    private BigDecimal nilai = BigDecimal.ZERO;
    private Integer countUser = 0;
    private AppCompatRatingBar simpleRatingBar;
    private LinearLayout linJadwal;
    private EditText edBukaMon, edBukaTue, edBukaWed, edBukaThu, edBukaFri, edBukaSat, edBukaSun,
            edTutupMon, edTutupTue, edTutupWed, edTutupThu, edTutupFri, edTutupSat, edTutupSun;
    private CheckBox ck24, ckLiburMon, ckLiburTue, ckLiburWed, ckLiburThu, ckLiburFri, ckLiburSat, ckLiburSun;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_wisata);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                kodeWisata = i.getString("kode");
                namaWisata = i.getString("nama");
                latt = i.getDouble("latt");
                longt = i.getDouble("longt");
            } catch (Exception e) {}
        }
        model = new MsWisataModel();
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView) findViewById(R.id.ImbBackInfoWisata);
        imgRating = (ImageView)findViewById(R.id.imgGiveRating);
        imgViewRating = (ImageView)findViewById(R.id.imgRatingAll);
        imgExpand = (ImageView)findViewById(R.id.imgInfoWisataExpandJadwal);
        linJadwal = (LinearLayout) findViewById(R.id.linInfoWisataJadwal);
        txtNama = (TextView) findViewById(R.id.txtNamaInfoKos);
        txtKode = (TextView) findViewById(R.id.txtIdInfoWisata);
        txtDeskripsi = (TextView) findViewById(R.id.txtDeskripsiInfoWisata);
        txtProv = (TextView) findViewById(R.id.txtProvInfoWisata);
        txtKab = (TextView) findViewById(R.id.txtKabInfoWisata);
        txtTelp = (TextView) findViewById(R.id.txtTelfInfoWisata);
        txtFas = (TextView) findViewById(R.id.txtFasilitasInfoKos);
        txtAlamat = (TextView) findViewById(R.id.txtAlamatInfoWisata);
        txtTelfWisata = (TextView) findViewById(R.id.txtTelpWisata);
        txtSms = (TextView) findViewById(R.id.txtSmsWisata);
        txtWa = (TextView)findViewById(R.id.txtWaWisata);
        txtEvent = (TextView)findViewById(R.id.txtEventWisata);
        txtKategori = (TextView)findViewById(R.id.txtKategoriInfoWisata);
        txtNilaiRating = (TextView)findViewById(R.id.txtNilaiRating);
        txtCountUser = (TextView)findViewById(R.id.txtUserRating);
        sliderView = findViewById(R.id.slider);
        simpleRatingBar = (AppCompatRatingBar) findViewById(R.id.simpleRatingBar);
        simpleRatingBar.setEnabled(false);
        edBukaMon = (EditText)findViewById(R.id.eInfoWisataJamBukaMon);
        edBukaTue = (EditText)findViewById(R.id.eInfoWisataJamBukaTue);
        edBukaWed = (EditText)findViewById(R.id.eInfoWisataJamBukaWed);
        edBukaThu = (EditText)findViewById(R.id.eInfoWisataJamBukaThu);
        edBukaFri = (EditText)findViewById(R.id.eInfoWisataJamBukaFri);
        edBukaSat = (EditText)findViewById(R.id.eInfoWisataJamBukaSat);
        edBukaSun = (EditText)findViewById(R.id.eInfoWisataJamBukaSun);
        edTutupMon = (EditText)findViewById(R.id.eInfoWisataJamTutupMon);
        edTutupTue = (EditText)findViewById(R.id.eInfoWisataJamTutupTue);
        edTutupWed = (EditText)findViewById(R.id.eInfoWisataJamTutupWed);
        edTutupThu = (EditText)findViewById(R.id.eInfoWisataJamTutupThu);
        edTutupFri = (EditText)findViewById(R.id.eInfoWisataJamTutupFri);
        edTutupSat = (EditText)findViewById(R.id.eInfoWisataJamTutupSat);
        edTutupSun = (EditText)findViewById(R.id.eInfoWisataJamTutupSun);
        ckLiburMon = (CheckBox) findViewById(R.id.ckInfoWisataLiburMon);
        ckLiburTue = (CheckBox) findViewById(R.id.ckInfoWisataLiburTue);
        ckLiburWed = (CheckBox) findViewById(R.id.ckInfoWisataLiburWed);
        ckLiburThu = (CheckBox) findViewById(R.id.ckInfoWisataLiburThu);
        ckLiburFri = (CheckBox) findViewById(R.id.ckInfoWisataLiburFri);
        ckLiburSat = (CheckBox) findViewById(R.id.ckInfoWisataLiburSat);
        ckLiburSun = (CheckBox) findViewById(R.id.ckInfoWisataLiburSun);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        requestForUpdate(kodeWisata, model);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        imgExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expcol_jadwal(view);
            }
        });
        imgRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(partisipasi){
                    Toast.makeText(MsInfoWisata.this, "Anda sudah berpartisipasi memberikan rating dan komentar!", Toast.LENGTH_LONG).show();
                }else if(level.equals("A")||level.equals("X")){
                    Toast.makeText(MsInfoWisata.this, "ADMIN tidak diperbolehkan memberikan rating!!", Toast.LENGTH_LONG).show();
                }else{
                    showDialogRating();
                }
            }
        });

        imgViewRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(countUser.intValue() == 0){
                    Toast.makeText(MsInfoWisata.this, "Belum ada rating dan komentar untuk wisata ini!", Toast.LENGTH_LONG).show();
                }else{
                    Intent i = new Intent(MsInfoWisata.this, MsViewRatingActivity.class);
                    i.putExtra("kode", kodeWisata);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

        txtTelfWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:"+model.getHeader().getTelp()));
                startActivity(callIntent);
            }
        });

        txtSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address",model.getHeader().getTelp());
                startActivity(smsIntent);
            }
        });

        txtWa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "Hallo Admin. saya bisa mendapatkan informasi lebih lanjut seputar wisata "
                        +model.getHeader().getNamaWisata().trim();
                openWhatsApp(model.getHeader().getTelp(), message);
            }
        });

        txtEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(MsInfoWisata.this, MsEventActivity.class);
                i.putExtra("model", model.getHeader());
                startActivity(i);
            }
        });

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMapsInfoWisata);
        fm.getMapAsync(this);
    }

    private void expcol_jadwal(View v) {
        if (linJadwal.isShown()) {
            Anim.slide_up(this, linJadwal);
            linJadwal.setVisibility(View.GONE);
            imgExpand.setBackgroundResource(R.drawable.expand);
        } else {
            Anim.slide_down(this, linJadwal);
            linJadwal.setVisibility(View.VISIBLE);
            imgExpand.setBackgroundResource(R.drawable.collapse);
        }
    }

    private void showDialogRating() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Sangat Buruk", "Buruk", "Ok", "Bagus", "Sangat Bagus"))
                .setDefaultRating(2)
                .setTitle("Rating dan Komentar")
                .setDescription("Harap mengisi rating dan komentar anda")
                .setCommentInputEnabled(true)
                .setStarColor(R.color.md_cyan_a400)
                .setNoteDescriptionTextColor(R.color.md_black)
                .setTitleTextColor(R.color.md_black)
                .setDescriptionTextColor(R.color.md_black)
                .setHintTextColor(R.color.md_grey_500)
                .setCommentTextColor(R.color.md_white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .create(MsInfoWisata.this)
                //.setTargetFragment(this, "") // only if listener is implemented by fragment
                .show();
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onNeutralButtonClicked() {

    }

    @Override
    public void onPositiveButtonClicked(int rating, String komentar) {
        Date today = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tanggalNow =df.format(today);
        mApiService.komentar(kodeWisata, rating, komentar, tanggalNow, userId, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    //hideDialog();
                                    Toast.makeText(MsInfoWisata.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    requestForUpdate(kodeWisata, model);
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsInfoWisata.this, error_message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(MsInfoWisata.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                                Toast.makeText(MsInfoWisata.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MsInfoWisata.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(MsInfoWisata.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void openWhatsApp(String nomor, String message){
        try{
            PackageManager packageManager = this.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone="+ nomor +"&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            }else {
                Toast.makeText(this, "Whatsapp belum terinstall", Toast.LENGTH_LONG).show();
            }
        } catch(Exception e) {
            Toast.makeText(this, "GAGAL MEMBUKA WHATSAPP\n"+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;
        LatLng latLng = new LatLng(latt, longt);
        mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .title(namaWisata)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,200));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel), 200, null);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Showing / hiding your current location
        //mGoogleMap.setMyLocationEnabled(true);
        // Enable / Disable zooming controls
        // Enable / Disable my location button
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Enable / Disable Rotate gesture
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        //Enable / Disable Button Zooming
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 200, null);
        //createLocationRequest();
    }

    @Override
    protected void onStop() {
        sliderView.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void requestForUpdate(String kode, MsWisataModel model){
        MsWisataHeaderModel header = new MsWisataHeaderModel();
        pDialog.setMessage("Harap Tunggu");
        showDialog();
        mApiService.requestForUpdateWisata(kode, userId,"INFO")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    header.setKodeWisata(jsonRESULTS.getString("c_kodewisata"));
                                    header.setNamaWisata(jsonRESULTS.getString("vc_namawisata"));
                                    header.setDeskripsi(jsonRESULTS.getString("t_deskripsi"));
                                    header.setTelp(jsonRESULTS.getString("vc_telp"));
                                    header.setAlamat(jsonRESULTS.getString("vc_alamat"));
                                    header.setLatt(jsonRESULTS.getDouble("d_latt"));
                                    header.setLongt(jsonRESULTS.getDouble("d_longt"));
                                    header.setKodeProv(jsonRESULTS.getString("c_idprovinsi"));
                                    header.setNamaProv(jsonRESULTS.getString("vc_nameprovinsi"));
                                    header.setKodeKab(jsonRESULTS.getString("c_idkabkota"));
                                    header.setNamaKab(jsonRESULTS.getString("vc_namekabkota"));
                                    header.setKodeKategori(jsonRESULTS.getString("c_idkategori"));
                                    header.setNamaKategori(jsonRESULTS.getString("vc_namakategori"));
                                    header.setPath1(jsonRESULTS.getString("vc_path1"));
                                    header.setPath2(jsonRESULTS.getString("vc_path2"));
                                    header.setPath3(jsonRESULTS.getString("vc_path3"));
                                    header.setPath4(jsonRESULTS.getString("vc_path4"));
                                    model.setHeader(header);
                                    partisipasi = jsonRESULTS.getInt("partisipasi_user") >0? true:false;
                                    nilai = new BigDecimal(jsonRESULTS.getInt("sum_nilai"));
                                    countUser = jsonRESULTS.getInt("count_user");
                                    if(countUser>0){
                                        BigDecimal hasilRating = nilai.divide(new BigDecimal(countUser),1, RoundingMode.HALF_UP);
                                        txtNilaiRating.setText(String.valueOf(hasilRating));
                                        simpleRatingBar.setRating(hasilRating.floatValue());
                                    }else {
                                        simpleRatingBar.setRating(0);
                                        txtNilaiRating.setText("0.0");
                                    }
                                    txtCountUser.setText(String.valueOf(countUser));
                                    JSONArray JsonArray = jsonRESULTS.getJSONArray("uploade");
                                    for (int i = 0; i < JsonArray.length(); i++) {
                                        JSONObject object = JsonArray.getJSONObject(i);
                                        MsWisataFasilitasModel item = new MsWisataFasilitasModel();
                                        item.setKodeWisata(header.getKodeWisata());
                                        item.setKodeFasilitas(String.valueOf(object.getInt("i_kodefasilitas")));
                                        item.setNamaFasilitas(object.getString("vc_nama"));
                                        model.addItem(item);
                                    }
                                    JSONArray JsonJadwalArray = jsonRESULTS.getJSONArray("jadwal");
                                    for (int i = 0; i < JsonJadwalArray.length(); i++) {
                                        JSONObject object = JsonJadwalArray.getJSONObject(i);
                                        MsWisataJadwalModel jadwal = new MsWisataJadwalModel();
                                        jadwal.setKodeWisata(header.getKodeWisata());
                                        jadwal.setHari(object.getString("c_hari"));
                                        jadwal.setBuka(object.getString("tm_buka"));
                                        jadwal.setTutup(object.getString("tm_tutup"));
                                        jadwal.setStatus(object.getString("c_status"));
                                        model.addItemJadwal(jadwal);
                                    }
                                    setModel(model);
                                    if(header.getPath1()!=null && !header.getPath1().equals("")) {
                                        SliderData model = new SliderData();
                                        StorageReference ref = storageReference.child("gallery/"+kode+"/"+header.getPath1().trim());
                                        model.setImgUrl(ref);
                                        sliderDataArrayList.add(model);
                                    }
                                    if(header.getPath2()!=null && !header.getPath2().equals("")) {
                                        SliderData model = new SliderData();
                                        StorageReference ref = storageReference.child("gallery/"+kode+"/"+header.getPath2().trim());
                                        model.setImgUrl(ref);
                                        sliderDataArrayList.add(model);
                                    }
                                    if(header.getPath3()!=null && !header.getPath3().equals("")) {
                                        SliderData model = new SliderData();
                                        StorageReference ref = storageReference.child("gallery/"+kode+"/"+header.getPath3().trim());
                                        model.setImgUrl(ref);
                                        sliderDataArrayList.add(model);
                                    }
                                    if(header.getPath4()!=null && !header.getPath4().equals("")) {
                                        SliderData model = new SliderData();
                                        StorageReference ref = storageReference.child("gallery/"+kode+"/"+header.getPath4().trim());
                                        model.setImgUrl(ref);
                                        sliderDataArrayList.add(model);
                                    }
                                    adapter = new SliderAdapter(MsInfoWisata.this, sliderDataArrayList);
                                    sliderView.setSliderAdapter(adapter);
                                    sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                                    sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                                    sliderView.setScrollTimeInSec(4);
                                    sliderView.setAutoCycle(true);
                                    sliderView.startAutoCycle();
                                    hideDialog();
                                } else {
                                    hideDialog();
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsInfoWisata.this, error_message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                Toast.makeText(MsInfoWisata.this, "GAGAL AMBIL DATA.("+e.getMessage()+")", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            } catch (IOException e) {
                                hideDialog();
                                Toast.makeText(MsInfoWisata.this, "GAGAL AMBIL DATA.("+e.getMessage()+")", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsInfoWisata.this, "GAGAL AMBIL DATA", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsInfoWisata.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void setModel(MsWisataModel modelEntity){
        MsWisataHeaderModel header = modelEntity.getHeader();
        latt = header.getLatt();
        longt = header.getLongt();
        txtKode.setText("ID Wisata: "+header.getKodeWisata());
        txtNama.setText(header.getNamaWisata());
        txtDeskripsi.setText("Deskripsi: "+header.getDeskripsi());
        txtAlamat.setText("Alamat: "+header.getAlamat());
        txtTelp.setText("Telp: "+header.getTelp());
        txtProv.setText("Provinsi: "+header.getNamaProv());
        txtKab.setText("Kabupaten/Kota: "+header.getNamaKab());
        txtKategori.setText("Kategori: "+header.getNamaKategori());
        StringBuilder sb = new StringBuilder();
        for(MsWisataFasilitasModel item:modelEntity.getItem()){
            sb.append(item.getNamaFasilitas()).append(", ");
        }
        txtFas.setText("Fasilitas: "+sb.substring(0, sb.length()-2));
        for(MsWisataJadwalModel jadwal : modelEntity.getItemjadwal()){
            boolean tutup = jadwal.getStatus().equals("D")?true:false;
            if(jadwal.getHari().equals("SUN")){
                setJamBukaTutup(tutup, ckLiburSun, edBukaSun, edTutupSun, jadwal);
            }
            if(jadwal.getHari().equals("MON")){
                setJamBukaTutup(tutup, ckLiburMon, edBukaMon, edTutupMon, jadwal);
            }
            if(jadwal.getHari().equals("TUE")){
                setJamBukaTutup(tutup, ckLiburTue, edBukaTue, edTutupTue, jadwal);
            }
            if(jadwal.getHari().equals("WED")){
                setJamBukaTutup(tutup, ckLiburWed, edBukaWed, edTutupWed, jadwal);
            }
            if(jadwal.getHari().equals("THU")){
                setJamBukaTutup(tutup, ckLiburThu, edBukaThu, edTutupThu, jadwal);
            }
            if(jadwal.getHari().equals("FRI")){
                setJamBukaTutup(tutup, ckLiburFri, edBukaFri, edTutupFri, jadwal);
            }
            if(jadwal.getHari().equals("SAT")){
                setJamBukaTutup(tutup, ckLiburSat, edBukaSat, edTutupSat, jadwal);
            }
        }
    }

    private void setJamBukaTutup(boolean tutup, CheckBox cekbox, EditText edBuka,
                                 EditText edTutup, MsWisataJadwalModel jadwal){
        cekbox.setChecked(tutup);
        edBuka.setText(tutup?"-":jadwal.getBuka());
        edTutup.setText(tutup?"-":jadwal.getTutup());
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
//https://android-arsenal.com/details/1/6143#!description