package com.example.pariwisata.master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsProvinsi;
import com.example.pariwisata.model.master.MsProvModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class MsProvinsiActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsProvinsi adapter;
    private ListView lsvupload;
    private ArrayList<MsProvModel> columnlist= new ArrayList<MsProvModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_prov);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterProvAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterProvBack);
        lsvupload	= (ListView)findViewById(R.id.listMasterProv);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterProvStatus);

        adapter		= new AdpMsProvinsi(MsProvinsiActivity.this, R.layout.adp_ms_prov, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(!level.equals("A"))
            ImgAdd.setVisibility(View.GONE);
        else
            ImgAdd.setVisibility(View.VISIBLE);

        getData(Link.BASE_URL_API + Link.MS_PROV);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(level.equals("A")){
                    Intent i  = new Intent(MsProvinsiActivity.this, MsProvinsiItemActivity.class);
                    i.putExtra("Status", "ADD");
                    startActivityForResult(i, RESULT_DATA);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(MsProvinsiActivity.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getData(String Url){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsProvModel colums 	= new MsProvModel();
                            colums.setKodeProv((String)((JSONObject) object).get("c_idprovinsi"));
                            colums.setNamaProv((String)((JSONObject) object).get("vc_nameprovinsi"));
                            colums.setCstat((String)((JSONObject) object).get("c_stat"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsProvinsiActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsProvinsiActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsProvinsiActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsProvinsiActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsProvinsiActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsProvinsiActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsProvinsiActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                adapter		= new AdpMsProvinsi(MsProvinsiActivity.this, R.layout.adp_ms_prov, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_PROV);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
