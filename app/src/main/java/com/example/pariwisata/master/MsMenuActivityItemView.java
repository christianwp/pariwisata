package com.example.pariwisata.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.list.ListJenisView;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsMenuActivityItemView extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, kodeTenant, namaTenant, kodeMenu, namaMenu, namaJenis;
    private Integer kodeJenis;
    private BigDecimal harga;
    private ImageView imgBack;
    private TextInputEditText edTenant, edNama, edJenis, edHarga;
    private TextInputLayout ilTenant, ilNama, ilJenis, ilHarga;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_menu_item);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                kodeMenu = i.getString("kodeMenu");
                namaMenu = i.getString("namaMenu");
                kodeTenant = i.getString("kodeTenant");
                namaTenant = i.getString("namaTenant");
                kodeJenis = i.getInt("kodeJenis");
                namaJenis = i.getString("namaJenis");
                harga = new BigDecimal(i.getDouble("harga",0));
            } catch (Exception e) {}
        }
        imgBack = (ImageView)findViewById(R.id.imgViewMenuDetailBack);
        edTenant = (TextInputEditText) findViewById(R.id.eAddViewMenuDetailTenant);
        ilTenant = (TextInputLayout) findViewById(R.id.input_layout_Menu_tenant);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewMenuDetailNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_Menu_nama);
        edJenis = (TextInputEditText) findViewById(R.id.eAddViewMenuDetailJenis);
        ilJenis = (TextInputLayout) findViewById(R.id.input_layout_Menu_jenis);
        edHarga = (TextInputEditText) findViewById(R.id.eAddViewMenuDetailHarga);
        ilHarga = (TextInputLayout) findViewById(R.id.input_layout_Menu_harga);
        btnSave = (Button) findViewById(R.id.btnViewMenuDetailSave);

        edTenant.setText(namaTenant);
        if(status.equals("EDIT")){
            edNama.setText(namaMenu);
            edJenis.setText(namaJenis);
            edHarga.setText(harga.toString());
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateTenant() && validateNama() && validateJenis() && validateHarga()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsMenuActivityItemView.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(status.equals("ADD")){
                                        insertData(edNama.getText().toString(), kodeTenant,
                                                kodeJenis, Double.valueOf(edHarga.getText().toString()), userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(kodeMenu, kodeTenant, edNama.getText().toString(),
                                                kodeJenis, Double.valueOf(edHarga.getText().toString()), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edJenis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edJenis.setEnabled(false);
                hideKeyboard(v);
                pilihJenis();
                edJenis.setEnabled(true);
            }
        });

        edJenis.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edJenis.setEnabled(false);
                    hideKeyboard(v);
                    pilihJenis();
                    edJenis.setEnabled(true);
                }
            }
        });
    }

    private void insertData(String nama, String tenant, Integer jenis, double harga, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.menu("", tenant, nama, jenis, harga, userId, tanggalNow, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsMenuActivityItemView.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsMenuActivityItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsMenuActivityItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsMenuActivityItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsMenuActivityItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsMenuActivityItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeMenu, String kodeTenant, String nama, Integer jenis, double harga, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.menu(kodeMenu, kodeTenant, nama, jenis, harga, userId, tanggalNow, "UPDATE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsMenuActivityItemView.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsMenuActivityItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsMenuActivityItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsMenuActivityItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsMenuActivityItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsMenuActivityItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edJenis.setText(data.getStringExtra("nama"));
                kodeJenis = Integer.valueOf(data.getStringExtra("kode"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihJenis(){
        Intent i = new Intent(MsMenuActivityItemView.this, ListJenisView.class);
        startActivityForResult(i, 2);
    }
    private boolean validateTenant() {
        boolean value;
        if (edTenant.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTenant);
            ilTenant.setError(getString(R.string.err_msg_namatenant));
        } else if (edTenant.length() > ilTenant.getCounterMaxLength()) {
            value=false;
            ilTenant.setError("Max character length is " + ilTenant.getCounterMaxLength());
        } else {
            value=true;
            ilTenant.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_menu));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateJenis() {
        boolean value;
        if (edJenis.getText().toString().isEmpty()){
            value=false;
            requestFocus(edJenis);
            ilJenis.setError(getString(R.string.err_msg_jenismenu));
        } else if (edJenis.length() > ilJenis.getCounterMaxLength()) {
            value=false;
            ilJenis.setError("Max character length is " + ilJenis.getCounterMaxLength());
        } else {
            value=true;
            ilJenis.setError(null);
        }
        return value;
    }

    private boolean validateHarga() {
        boolean value;
        if (edHarga.getText().toString().isEmpty()){
            value=false;
            requestFocus(edHarga);
            ilHarga.setError(getString(R.string.err_msg_hargamenu));
        } else if (edHarga.length() > ilHarga.getCounterMaxLength()) {
            value=false;
            ilHarga.setError("Max character length is " + ilHarga.getCounterMaxLength());
        } else {
            value=true;
            ilHarga.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
