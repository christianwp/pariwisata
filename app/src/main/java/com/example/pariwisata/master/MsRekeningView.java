package com.example.pariwisata.master;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsRekening;
import com.example.pariwisata.model.master.MsRekeningModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MsRekeningView extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsRekening adapter;
    private ListView lsvupload;
    private ArrayList<MsRekeningModel> columnlist= new ArrayList<MsRekeningModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeTenant, namaTenant;
    private TextInputEditText edTenant;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_rekening);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                kodeTenant = i.getString("kodeTenant");
                namaTenant = i.getString("namaTenant");
            } catch (Exception e) {}
        }
        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterRekeningAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterRekeningBack);
        edTenant = (TextInputEditText)findViewById(R.id.eMasterRekeningNamaTenant);
        lsvupload	= (ListView)findViewById(R.id.listMasterRekening);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterRekeningStatus);

        adapter		= new AdpMsRekening(MsRekeningView.this, R.layout.adp_ms_rek, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(level.equals("U")) {
            ImgAdd.setVisibility(View.VISIBLE);
        }else {
            edTenant.setText(namaTenant);
            ImgAdd.setVisibility(View.GONE);
        }

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(level.equals("U")){
                    Intent i  = new Intent(MsRekeningView.this, MsRekeningItemView.class);
                    i.putExtra("Status", "ADD");
                    i.putExtra("kodeTenant", kodeTenant);
                    i.putExtra("namaTenant", edTenant.getText().toString());
                    startActivityForResult(i, RESULT_DATA);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(MsRekeningView.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                }
            }
        });
        if(level.equals("U")){
            edTenant.setEnabled(false);
            edTenant.setText(namaTenant);
        }
        getData(Link.BASE_URL_API + Link.MS_REKENING, kodeTenant);
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<>();
        adapter		= new AdpMsRekening(MsRekeningView.this, R.layout.adp_ms_rek, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

    }

    private void getData(String Url, final String kodeTenant){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Tidak Ada Data");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsRekeningModel colums 	= new MsRekeningModel();
                            colums.setKodeBank((String)((JSONObject) object).get("c_kodebank"));
                            colums.setNamaBank((String)((JSONObject) object).get("vc_namabank"));
                            colums.setKodeTenant((String)((JSONObject) object).get("c_kodetenant"));
                            colums.setNamaTenant((String)((JSONObject) object).get("vc_namatenant"));
                            colums.setNoRek((String)((JSONObject) object).get("c_norekening"));
                            colums.setNamaRek((String)((JSONObject) object).get("vc_namarekening"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsRekeningView.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsRekeningView.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsRekeningView.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsRekeningView.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsRekeningView.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsRekeningView.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsRekeningView.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("kodeTenant", kodeTenant);
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<>();
                adapter		= new AdpMsRekening(MsRekeningView.this, R.layout.adp_ms_rek, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_REKENING, kodeTenant);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}