package com.example.pariwisata.master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.pariwisata.R;
import com.example.pariwisata.list.ListKabView;
import com.example.pariwisata.list.ListKategoriView;
import com.example.pariwisata.list.ListProvinsiView;
import com.example.pariwisata.model.master.MsWisataFasilitasModel;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.model.master.MsWisataJadwalModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.service.Anim;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.transaksi.MapsActivity;
import com.example.pariwisata.utilities.GlideApp;
import com.example.pariwisata.utilities.JSONfunctions;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsTempatWisataItemActivity extends AppCompatActivity {

    private ImageView imgBack, img1, img2, img3, img4, imgColExpJadwal,
    imgBuka, imgBukaMon, imgBukaTue, imgBukaWed, imgBukaThu, imgBukaFri, imgBukaSat, imgBukaSun,
    imgTutup, imgTutupMon, imgTutupTue, imgTutupWed, imgTutupThu, imgTutupFri, imgTutupSat, imgTutupSun;
    private Button btnSave, btnMap, btnJam;
    private TextInputLayout ilNama, ilDes, ilAlamat, ilTelp, ilKategori, ilProv, ilKab;
    private TextInputEditText edNama, edDes, edAlamat, edTelp, edKategori, eProv, eKab;
    private CheckBox ckAlamat;
    private SharedPreferences shared;
    private String level, userId;
    private boolean GpsStatus, alamatManual=false, alamatMaps=false;
    private LocationManager locationManager ;
    private double latitude, longtitude;
    private PrefUtil prefUtil;
    private ProgressDialog pDialog;
    private BaseApiService mApiService;
    private int RESULT_MAP = 5;
    private Calendar dateAndTime = Calendar.getInstance();
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private String alamat, infoStatus, kode, kodeProv, kodeKab, kodeKategori;
    private Date tglFrom = null, tglTo = null;
    private MsWisataModel model;
    private TextView txtStatus, txtFasilitas;
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
    private static final int REQUEST_CODE_GALLERY = 0013;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private String hasilFoto1 = "N", hasilFoto2 = "N", hasilFoto3 = "N", hasilFoto4 = "N";
    private Uri selectedImage1, selectedImage2, selectedImage3, selectedImage4;
    private LinearLayout linJadwal;
    private EditText edBuka, edBukaMon, edBukaTue, edBukaWed, edBukaThu, edBukaFri, edBukaSat, edBukaSun,
    edTutup, edTutupMon, edTutupTue, edTutupWed, edTutupThu, edTutupFri, edTutupSat, edTutupSun;
    private CheckBox ck24, ckLiburMon, ckLiburTue, ckLiburWed, ckLiburThu, ckLiburFri, ckLiburSat, ckLiburSun;
    private int hourZero = 0;
    private int minuteZero = 0;
    private int hourLast = 23;
    private int minuteLast = 59;

    private void expcol_jadwal(View v) {
        if (linJadwal.isShown()) {
            Anim.slide_up(this, linJadwal);
            linJadwal.setVisibility(View.GONE);
            imgColExpJadwal.setBackgroundResource(R.drawable.expand);
        } else {
            Anim.slide_down(this, linJadwal);
            linJadwal.setVisibility(View.VISIBLE);
            imgColExpJadwal.setBackgroundResource(R.drawable.collapse);
        }
    }

    private void pilihJam(final EditText edittext) {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MsTempatWisataItemActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String sHrs, sMins;
                if (selectedMinute < 10) {
                    sMins = "0" + selectedMinute;
                } else {
                    sMins = String.valueOf(selectedMinute);
                }
                if (selectedHour < 10) {
                    sHrs = "0" + selectedHour;
                } else {
                    sHrs = String.valueOf(selectedHour);
                }
                setSelectedTime(sHrs, sMins, edittext);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Pilih Jam");
        mTimePicker.show();
    }

    public void setSelectedTime(String hourOfDay, String minute, EditText edText) {
        edText.setText(hourOfDay + ":" + minute);
    }

    private void offChecked(boolean check, EditText eBuka, EditText eTutup, ImageView imgBuka,
                     ImageView imgTutup) {
        if (check) {
            String sMins, sHrs;
            if (minuteZero < 10) {
                sMins = "0" + minuteZero;
            } else {
                sMins = String.valueOf(minuteZero);
            }
            if (hourZero < 10) {
                sHrs = "0" + hourZero;
            } else {
                sHrs = String.valueOf(hourZero);
            }
            setSelectedTime(sHrs, sMins, eBuka);
            eTutup.setText(hourLast + ":" + minuteLast);
            imgBuka.setVisibility(View.GONE);
            imgTutup.setVisibility(View.GONE);
        } else {
            eBuka.setText(null);
            eTutup.setText(null);
            imgBuka.setVisibility(View.VISIBLE);
            imgTutup.setVisibility(View.VISIBLE);
        }
    }

    private void prosesTerapkan() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MsTempatWisataItemActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MsTempatWisataItemActivity.this);
        }
        if (edBuka.getText().toString().isEmpty() || edTutup.getText().toString().isEmpty()) {
            builder.setTitle("Perhatian")
                    .setMessage("Jam buka/tutup toko tidak boleh kosong.")
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            edBukaSun.setText(edBuka.getText().toString());
            edBukaMon.setText(edBuka.getText().toString());
            edBukaTue.setText(edBuka.getText().toString());
            edBukaWed.setText(edBuka.getText().toString());
            edBukaThu.setText(edBuka.getText().toString());
            edBukaFri.setText(edBuka.getText().toString());
            edBukaSat.setText(edBuka.getText().toString());
            edTutupSun.setText(edTutup.getText().toString());
            edTutupMon.setText(edTutup.getText().toString());
            edTutupTue.setText(edTutup.getText().toString());
            edTutupWed.setText(edTutup.getText().toString());
            edTutupThu.setText(edTutup.getText().toString());
            edTutupFri.setText(edTutup.getText().toString());
            edTutupSat.setText(edTutup.getText().toString());
        }
    }

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_wisata_item);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                infoStatus = i.getString("Status");// ADD/EDIT
                kode = i.getString("kode");
            } catch (Exception e) {}
        }
        model = new MsWisataModel();
        mApiService         = Link.getAPIService();
        prefUtil = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack		= (ImageView)findViewById(R.id.imgViewMsWisataItemBackk);
        ilNama = (TextInputLayout)findViewById(R.id.input_layout_mswisata_nama);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewmswisataNama);
        ilDes = (TextInputLayout)findViewById(R.id.input_layout_mswisata_deskripsi);
        edDes = (TextInputEditText) findViewById(R.id.eAddViewmswisataDeskripsi);
        ilAlamat = (TextInputLayout)findViewById(R.id.input_layout_trevent_alamat);
        edAlamat = (TextInputEditText) findViewById(R.id.eAddViewtreventAlamat);
        ilTelp = (TextInputLayout)findViewById(R.id.input_layout_mswisata_telp);
        edTelp = (TextInputEditText) findViewById(R.id.eAddViewmswisataTelp);
        ilKategori = (TextInputLayout)findViewById(R.id.input_layout_mswisata_kategori);
        edKategori = (TextInputEditText) findViewById(R.id.eAddViewmswisataKategori);
        ilProv = (TextInputLayout)findViewById(R.id.input_layout_mswisata_prov);
        eProv = (TextInputEditText) findViewById(R.id.eAddViewmswisataProv);
        ilKab = (TextInputLayout)findViewById(R.id.input_layout_mswisata_kab);
        eKab = (TextInputEditText) findViewById(R.id.eAddViewmswisataKab);
        btnMap = (Button)findViewById(R.id.bInfotreventAlamatMap);
        ckAlamat = (CheckBox)findViewById(R.id.ckAlamattreventManual);
        btnSave = (Button)findViewById(R.id.btnViewTrEventSave);
        txtStatus = (TextView)findViewById(R.id.txtStatusEvent);
        txtFasilitas = (TextView)findViewById(R.id.txtAddFasilitasWisata);
        img1 = (ImageView)findViewById(R.id.imgInfoGbr1);
        img2 = (ImageView)findViewById(R.id.imgInfoGbr2);
        img3 = (ImageView)findViewById(R.id.imgInfoGbr3);
        img4 = (ImageView)findViewById(R.id.imgInfoGbr4);
        linJadwal = (LinearLayout)findViewById(R.id.linTokoLayoutJadwal);
        imgColExpJadwal = (ImageView)findViewById(R.id.imgExpandJadwal);
        imgBuka = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBuka);
        imgBukaMon = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaMon);
        imgBukaTue = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaTue);
        imgBukaWed = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaWed);
        imgBukaThu = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaThu);
        imgBukaFri = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaFri);
        imgBukaSat = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaSat);
        imgBukaSun = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamBukaSun);
        imgTutup = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutup);
        imgTutupMon = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupMon);
        imgTutupTue = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupTue);
        imgTutupWed = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupWed);
        imgTutupThu = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupThu);
        imgTutupFri = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupFri);
        imgTutupSat = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupSat);
        imgTutupSun = (ImageView)findViewById(R.id.imgTokoLayoutPilihJamTutupSun);
        edBuka = (EditText)findViewById(R.id.eTokoLayoutJamBuka);
        edBukaMon = (EditText)findViewById(R.id.eTokoLayoutJamBukaMon);
        edBukaTue = (EditText)findViewById(R.id.eTokoLayoutJamBukaTue);
        edBukaWed = (EditText)findViewById(R.id.eTokoLayoutJamBukaWed);
        edBukaThu = (EditText)findViewById(R.id.eTokoLayoutJamBukaThu);
        edBukaFri = (EditText)findViewById(R.id.eTokoLayoutJamBukaFri);
        edBukaSat = (EditText)findViewById(R.id.eTokoLayoutJamBukaSat);
        edBukaSun = (EditText)findViewById(R.id.eTokoLayoutJamBukaSun);
        edTutup = (EditText)findViewById(R.id.eTokoLayoutJamTutup);
        edTutupMon = (EditText)findViewById(R.id.eTokoLayoutJamTutupMon);
        edTutupTue = (EditText)findViewById(R.id.eTokoLayoutJamTutupTue);
        edTutupWed = (EditText)findViewById(R.id.eTokoLayoutJamTutupWed);
        edTutupThu = (EditText)findViewById(R.id.eTokoLayoutJamTutupThu);
        edTutupFri = (EditText)findViewById(R.id.eTokoLayoutJamTutupFri);
        edTutupSat = (EditText)findViewById(R.id.eTokoLayoutJamTutupSat);
        edTutupSun = (EditText)findViewById(R.id.eTokoLayoutJamTutupSun);
        ck24 = (CheckBox)findViewById(R.id.ckTokoLayout24jam);
        ckLiburMon = (CheckBox) findViewById(R.id.ckLiburMon);
        ckLiburTue = (CheckBox) findViewById(R.id.ckLiburTue);
        ckLiburWed = (CheckBox) findViewById(R.id.ckLiburWed);
        ckLiburThu = (CheckBox) findViewById(R.id.ckLiburThu);
        ckLiburFri = (CheckBox) findViewById(R.id.ckLiburFri);
        ckLiburSat = (CheckBox) findViewById(R.id.ckLiburSat);
        ckLiburSun = (CheckBox) findViewById(R.id.ckLiburSun);
        btnJam = (Button)findViewById(R.id.btnProsesTerapkan);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        txtStatus.setText("");
        txtStatus.setVisibility(View.GONE);

        if(level.equals("X")){
            eKab.setEnabled(false);
            kodeKab = shared.getString(PrefUtil.KDKAB, null);
            eKab.setText(shared.getString(PrefUtil.NMKAB, null));
        }else{
            eKab.setEnabled(true);
        }

        btnJam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prosesTerapkan();
            }
        });

        ck24.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBuka, edTutup, imgBuka, imgTutup);
            }
        });

        ckLiburSun.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaSun, edTutupSun, imgBukaSun, imgTutupSun);
            }
        });

        ckLiburMon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaMon, edTutupMon, imgBukaMon, imgTutupMon);
            }
        });

        ckLiburTue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaTue, edTutupTue, imgBukaTue, imgTutupTue);
            }
        });

        ckLiburWed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaWed, edTutupWed, imgBukaWed, imgTutupWed);
            }
        });

        ckLiburThu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaThu, edTutupThu, imgBukaThu, imgTutupThu);
            }
        });

        ckLiburFri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaFri, edTutupFri, imgBukaFri, imgTutupFri);
            }
        });

        ckLiburSat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                offChecked(b, edBukaSat, edTutupSat, imgBukaSat, imgTutupSat);
            }
        });

        imgBuka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBuka);
            }
        });

        imgBukaMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaMon);
            }
        });

        imgBukaTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaTue);
            }
        });

        imgBukaWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaWed);
            }
        });

        imgBukaThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaThu);
            }
        });

        imgBukaFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaFri);
            }
        });

        imgBukaSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaSat);
            }
        });

        imgBukaSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edBukaSun);
            }
        });

        imgTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutup);
            }
        });

        imgTutupMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupMon);
            }
        });

        imgTutupTue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupTue);
            }
        });

        imgTutupWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupWed);
            }
        });

        imgTutupThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupThu);
            }
        });

        imgTutupFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupFri);
            }
        });

        imgTutupSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupSat);
            }
        });

        imgTutupSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihJam(edTutupSun);
            }
        });

        imgColExpJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expcol_jadwal(view);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateNama() && validateDeskripsi() && validateAlamat() && validateMaps() &&
                        validateKategori() && validateKab()){
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MsTempatWisataItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    initEntity(model);
                                    if(infoStatus.equals("ADD")){
                                        proses_simpan();
                                    }else{
                                        proses_update();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edKategori.setEnabled(false);
                hideKeyboard(v);
                pilihKategori();
                edKategori.setEnabled(true);
            }
        });

        edKategori.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edKategori.setEnabled(false);
                    hideKeyboard(v);
                    pilihKategori();
                    edKategori.setEnabled(true);
                }
            }
        });

        eProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eProv.setEnabled(false);
                hideKeyboard(v);
                pilihProv();
                eProv.setEnabled(true);
            }
        });

        eProv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    eProv.setEnabled(false);
                    hideKeyboard(v);
                    pilihProv();
                    eProv.setEnabled(true);
                }
            }
        });

        eKab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kodeProv!=null && !kodeProv.equals("")){
                    eKab.setEnabled(false);
                    hideKeyboard(v);
                    pilihKab();
                    eKab.setEnabled(true);
                }else{
                    Toast.makeText(MsTempatWisataItemActivity.this, "Provinsi harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                }
            }
        });

        eKab.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(kodeProv!=null && !kodeProv.equals("")){
                        eKab.setEnabled(false);
                        hideKeyboard(v);
                        pilihKab();
                        eKab.setEnabled(true);
                    }else{
                        Toast.makeText(MsTempatWisataItemActivity.this, "Provinsi harus diisi terlebih dahulu", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckGpsStatus() ;
                if(GpsStatus == true){
                    Intent i = new Intent(getApplication(), MapsActivity.class);
                    startActivityForResult(i, RESULT_MAP);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsTempatWisataItemActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("GPS belum diaktifkan\nAktifkan dahulu!")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        ckAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alamatManual=!alamatManual;
                edAlamat.setEnabled(alamatManual);
            }
        });

        txtFasilitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihFasilitas();
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 21);

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 22);

            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 23);

            }
        });

        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 24);

            }
        });

        if(infoStatus.equals("EDIT")) {
            if(kode == null || kode.equals(""))
                Toast.makeText(MsTempatWisataItemActivity.this, "Kode wisata tidak ada", Toast.LENGTH_LONG).show();
            else
                requestForUpdate(kode, model);
        }else{
            model = new MsWisataModel();
            kodeProv = "72";
            mode(true);
            edAlamat.setEnabled(false);
        }
        edNama.requestFocus();
    }

    private void requestForUpdate(String kode, MsWisataModel model){
        MsWisataHeaderModel header = new MsWisataHeaderModel();
        pDialog.setMessage("Harap Tunggu");
        showDialog();
        mApiService.requestForUpdateWisata(kode, userId,"DETAIL")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    header.setKodeWisata(jsonRESULTS.getString("c_kodewisata"));
                                    header.setNamaWisata(jsonRESULTS.getString("vc_namawisata"));
                                    header.setDeskripsi(jsonRESULTS.getString("t_deskripsi"));
                                    header.setTelp(jsonRESULTS.getString("vc_telp"));
                                    header.setAlamat(jsonRESULTS.getString("vc_alamat"));
                                    header.setLatt(jsonRESULTS.getDouble("d_latt"));
                                    header.setLongt(jsonRESULTS.getDouble("d_longt"));
                                    kodeProv = jsonRESULTS.getString("c_idprovinsi");
                                    header.setKodeProv(jsonRESULTS.getString("c_idprovinsi"));
                                    header.setNamaProv(jsonRESULTS.getString("vc_nameprovinsi"));
                                    kodeKab = jsonRESULTS.getString("c_idkabkota");
                                    header.setKodeKab(jsonRESULTS.getString("c_idkabkota"));
                                    header.setNamaKab(jsonRESULTS.getString("vc_namekabkota"));
                                    kodeKategori = jsonRESULTS.getString("c_idkategori");
                                    header.setKodeKategori(jsonRESULTS.getString("c_idkategori"));
                                    header.setNamaKategori(jsonRESULTS.getString("vc_namakategori"));
                                    header.setPath1(jsonRESULTS.getString("vc_path1"));
                                    header.setPath2(jsonRESULTS.getString("vc_path2"));
                                    header.setPath3(jsonRESULTS.getString("vc_path3"));
                                    header.setPath4(jsonRESULTS.getString("vc_path4"));
                                    model.setHeader(header);
                                    JSONArray JsonArray = jsonRESULTS.getJSONArray("uploade");
                                    for (int i = 0; i < JsonArray.length(); i++) {
                                        JSONObject object = JsonArray.getJSONObject(i);
                                        MsWisataFasilitasModel item = new MsWisataFasilitasModel();
                                        item.setKodeWisata(header.getKodeWisata());
                                        item.setKodeFasilitas(String.valueOf(object.getInt("i_kodefasilitas")));
                                        item.setNamaFasilitas(object.getString("vc_nama"));
                                        model.addItem(item);
                                    }
                                    JSONArray JsonJadwalArray = jsonRESULTS.getJSONArray("jadwal");
                                    for (int i = 0; i < JsonJadwalArray.length(); i++) {
                                        JSONObject object = JsonJadwalArray.getJSONObject(i);
                                        MsWisataJadwalModel jadwal = new MsWisataJadwalModel();
                                        jadwal.setKodeWisata(header.getKodeWisata());
                                        jadwal.setHari(object.getString("c_hari"));
                                        jadwal.setBuka(object.getString("tm_buka"));
                                        jadwal.setTutup(object.getString("tm_tutup"));
                                        jadwal.setStatus(object.getString("c_status"));
                                        model.addItemJadwal(jadwal);
                                    }
                                    setModel(model);
                                    if(header.getPath1()!=null && !header.getPath1().equals(""))
                                        loadImage(img1, header.getPath1().trim(), header.getKodeWisata());
                                    if(header.getPath2()!=null && !header.getPath2().equals(""))
                                        loadImage(img2, header.getPath2().trim(), header.getKodeWisata());
                                    if(header.getPath3()!=null && !header.getPath3().equals(""))
                                        loadImage(img3, header.getPath3().trim(), header.getKodeWisata());
                                    if(header.getPath4()!=null && !header.getPath4().equals(""))
                                        loadImage(img4, header.getPath4().trim(), header.getKodeWisata());
                                    hideDialog();
                                } else {
                                    hideDialog();
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsTempatWisataItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsTempatWisataItemActivity.this, "GAGAL LOGIN", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsTempatWisataItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void loadImage(ImageView img, String path, String kode){
        pDialog.setMessage("Loading Image....");
        showDialog();
        img.setBackgroundResource(0);
        StorageReference ref = storageReference.child("gallery/"+kode+"/"+path);
        Glide.with(this /* context */)
                .load(ref)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        hideDialog();
                        return false;
                    }
                })
                .into(img);

    }

    private void mode(boolean stat){
        edNama.setEnabled(stat);
        edDes.setEnabled(stat);
        edAlamat.setEnabled(stat);
        ckAlamat.setEnabled(stat);
        edTelp.setEnabled(stat);
        edDes.setEnabled(stat);
    }

    private void pilihKategori(){
        Intent i = new Intent(MsTempatWisataItemActivity.this, ListKategoriView.class);
        startActivityForResult(i, 2);
    }

    private void pilihProv(){
        Intent i = new Intent(MsTempatWisataItemActivity.this, ListProvinsiView.class);
        startActivityForResult(i, 3);
    }

    private void pilihKab(){
        Intent i = new Intent(MsTempatWisataItemActivity.this, ListKabView.class);
        i.putExtra("kode", kodeProv);
        startActivityForResult(i, 4);
    }

    private void pilihFasilitas(){
        Intent i = new Intent(MsTempatWisataItemActivity.this, MsTempatWisataFasilitasItemActivity.class);
        i.putExtra("model", model);
        startActivityForResult(i, 6);
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setModel(MsWisataModel modelEntity){
        MsWisataHeaderModel header = modelEntity.getHeader();
        latitude = header.getLatt();
        longtitude = header.getLongt();
        edNama.setText(header.getNamaWisata());
        edDes.setText(header.getDeskripsi());
        edAlamat.setText(header.getAlamat());
        edTelp.setText(header.getTelp());
        eProv.setText(header.getNamaProv());
        kodeProv = header.getKodeProv();
        eKab.setText(header.getNamaKab());
        kodeKab = header.getKodeKab();
        edKategori.setText(header.getNamaKategori());
        for(MsWisataJadwalModel jadwal : modelEntity.getItemjadwal()){
            boolean tutup = jadwal.getStatus().equals("D")?true:false;
            if(jadwal.getHari().equals("SUN")){
                setJamBukaTutup(tutup, ckLiburSun, edBukaSun, edTutupSun, jadwal,
                        imgBukaSun, imgTutupSun);
            }
            if(jadwal.getHari().equals("MON")){
                setJamBukaTutup(tutup, ckLiburMon, edBukaMon, edTutupMon, jadwal,
                        imgBukaMon, imgTutupMon);
            }
            if(jadwal.getHari().equals("TUE")){
                setJamBukaTutup(tutup, ckLiburTue, edBukaTue, edTutupTue, jadwal,
                        imgBukaTue, imgTutupTue);
            }
            if(jadwal.getHari().equals("WED")){
                setJamBukaTutup(tutup, ckLiburWed, edBukaWed, edTutupWed, jadwal,
                        imgBukaWed, imgTutupWed);
            }
            if(jadwal.getHari().equals("THU")){
                setJamBukaTutup(tutup, ckLiburThu, edBukaThu, edTutupThu, jadwal,
                        imgBukaThu, imgTutupThu);
            }
            if(jadwal.getHari().equals("FRI")){
                setJamBukaTutup(tutup, ckLiburFri, edBukaFri, edTutupFri, jadwal,
                        imgBukaFri, imgTutupFri);
            }
            if(jadwal.getHari().equals("SAT")){
                setJamBukaTutup(tutup, ckLiburSat, edBukaSat, edTutupSat, jadwal,
                        imgBukaSat, imgTutupSat);
            }
        }
    }

    private void setJamBukaTutup(boolean tutup, CheckBox cekbox, EditText edBuka,
                                 EditText edTutup, MsWisataJadwalModel jadwal,
                                 ImageView imgBuka, ImageView imgTutup){
        cekbox.setChecked(tutup);
        if(tutup) {
            imgBuka.setVisibility(View.GONE);
            imgTutup.setVisibility(View.GONE);
        }else{
            imgBuka.setVisibility(View.VISIBLE);
            imgTutup.setVisibility(View.VISIBLE);
        }
        edBuka.setText(jadwal.getBuka());
        edTutup.setText(jadwal.getTutup());
    }

    private void initEntity(MsWisataModel model){
        MsWisataHeaderModel header = new MsWisataHeaderModel();
        if(!infoStatus.equals("ADD"))
            header.setKodeWisata(kode);
        header.setNamaWisata(edNama.getText().toString());
        header.setDeskripsi(edDes.getText().toString());
        header.setAlamat(edAlamat.getText().toString());
        header.setTelp(edTelp.getText().toString());
        header.setLatt(latitude);
        header.setLongt(longtitude);
        header.setKodeKategori(kodeKategori);
        header.setNamaKategori(edKategori.getText().toString());
        header.setKodeProv(kodeProv);
        header.setNamaProv(eProv.getText().toString());
        header.setKodeKab(kodeKab);
        header.setNamaKab(eKab.getText().toString());
        header.setKodeKategori(kodeKategori);
        header.setKodeUser(userId);
        header.setTglNow(df.format(Calendar.getInstance().getTime()));
        if(hasilFoto1.equals("Y"))
            header.setPath1("Y");
        else
            header.setPath1("N");

        if(hasilFoto2.equals("Y"))
            header.setPath2("Y");
        else
            header.setPath2("N");

        if(hasilFoto3.equals("Y"))
            header.setPath3("Y");
        else
            header.setPath3("N");

        if(hasilFoto4.equals("Y"))
            header.setPath4("Y");
        else
            header.setPath4("N");
        model.setItemjadwal(new ArrayList<MsWisataJadwalModel>());
        MsWisataJadwalModel jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("SUN");
        jadwal.setBuka(edBukaSun.getText().toString());
        jadwal.setTutup(edTutupSun.getText().toString());
        jadwal.setStatus(ckLiburSun.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("MON");
        jadwal.setBuka(edBukaMon.getText().toString());
        jadwal.setTutup(edTutupMon.getText().toString());
        jadwal.setStatus(ckLiburMon.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("TUE");
        jadwal.setBuka(edBukaTue.getText().toString());
        jadwal.setTutup(edTutupTue.getText().toString());
        jadwal.setStatus(ckLiburTue.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("WED");
        jadwal.setBuka(edBukaWed.getText().toString());
        jadwal.setTutup(edTutupWed.getText().toString());
        jadwal.setStatus(ckLiburWed.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("THU");
        jadwal.setBuka(edBukaThu.getText().toString());
        jadwal.setTutup(edTutupThu.getText().toString());
        jadwal.setStatus(ckLiburThu.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("FRI");
        jadwal.setBuka(edBukaFri.getText().toString());
        jadwal.setTutup(edTutupFri.getText().toString());
        jadwal.setStatus(ckLiburFri.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        jadwal = new MsWisataJadwalModel();
        if(!infoStatus.equals("ADD"))
            jadwal.setKodeWisata(header.getKodeWisata());
        jadwal.setHari("SAT");
        jadwal.setBuka(edBukaSat.getText().toString());
        jadwal.setTutup(edTutupSat.getText().toString());
        jadwal.setStatus(ckLiburSat.isChecked()?"D":"A");
        model.addItemJadwal(jadwal);

        model.setHeader(header);
    }

    private void proses_update(){
        RequestTaskUpdate task = new RequestTaskUpdate();
        task.applicationContext = MsTempatWisataItemActivity.this;
        task.execute(new String[] { Link.BASE_URL_API+"wisata_update_obj.php" });
    }

    private void proses_simpan(){
        RequestTask task = new RequestTask();
        task.applicationContext = MsTempatWisataItemActivity.this;
        task.execute(new String[] { Link.BASE_URL_API+"wisata_insert_obj.php" });
    }

    class RequestTask extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;
        protected Context applicationContext;

        @Override
        protected void onPreExecute() {
            //this.dialog = ProgressDialog.show(applicationContext,"Proses Menyimpan Data", "Loading...", true);
        }

        @Override
        protected String doInBackground(String... uri) {
            String responseString = null;
            responseString = saveData();
            Log.d("TAGS","response:"+responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try{
                super.onPostExecute(result);
                JSONObject jsonRESULTS = new JSONObject(result);
                if (jsonRESULTS.getString("value").equals("false")){
                    hideDialog();
                    String nomor = jsonRESULTS.getString("nomor");
                    if(hasilFoto1.equals("Y")){
                        uploadImage(nomor, 1, selectedImage1);
                    }else if(hasilFoto2.equals("Y")){
                        uploadImage(nomor, 2, selectedImage2);
                    }else if(hasilFoto3.equals("Y")){
                        uploadImage(nomor, 3, selectedImage3);
                    }else if(hasilFoto4.equals("Y")){
                        uploadImage(nomor, 4, selectedImage4);
                    }else{
                        Toast.makeText(MsTempatWisataItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    }
                }
            }catch (JSONException e) {
                hideDialog();
                Toast.makeText(MsTempatWisataItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    class RequestTaskUpdate extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;
        protected Context applicationContext;

        @Override
        protected void onPreExecute() {
            //this.dialog = ProgressDialog.show(applicationContext,"Proses Menyimpan Data", "Loading...", true);
        }

        @Override
        protected String doInBackground(String... uri) {
            String responseString = null;
            responseString = updateData();
            Log.d("TAGS","response:"+responseString);
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try{
                super.onPostExecute(result);
                JSONObject jsonRESULTS = new JSONObject(result);
                if (jsonRESULTS.getString("value").equals("false")){
                    hideDialog();
                    if(hasilFoto1.equals("Y")){
                        uploadImage(model.getHeader().getKodeWisata(), 1, selectedImage1);
                    }else if(hasilFoto2.equals("Y")){
                        uploadImage(model.getHeader().getKodeWisata(), 2, selectedImage2);
                    }else if(hasilFoto3.equals("Y")){
                        uploadImage(model.getHeader().getKodeWisata(), 3, selectedImage3);
                    }else if(hasilFoto4.equals("Y")){
                        uploadImage(model.getHeader().getKodeWisata(), 4, selectedImage4);
                    }else{
                        Toast.makeText(MsTempatWisataItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    }
                }else{
                    String message = jsonRESULTS.getString("message");
                    Toast.makeText(MsTempatWisataItemActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }catch (JSONException e) {
                hideDialog();
                Toast.makeText(MsTempatWisataItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    public String saveData(){
        String sret="";
        Gson gson = new Gson();
        String json = gson.toJson(model);
        //System.out.println(json);
        try {
            JSONObject data = new JSONObject(json);
            sret= JSONfunctions.doPost(Link.BASE_URL_API+"wisata_insert_obj.php", data);
            Log.d("TAG","response:"+sret);
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return sret;
    }

    public String updateData(){
        String sret="";
        Gson gson = new Gson();
        String json = gson.toJson(model);
        //System.out.println(json);
        try {
            JSONObject data = new JSONObject(json);
            sret= JSONfunctions.doPost(Link.BASE_URL_API+"wisata_update_obj.php", data);
            Log.d("TAG","response:"+sret);
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return sret;
    }

    private void uploadImage(String nomor, Integer line, Uri selected) {
        if (selected != null) {
            pDialog.setMessage("Uploading Image "+line+".....");
            showDialog();
            StorageReference ref = storageReference.child("gallery/"+nomor+"/"+ nomor.trim()+"_"+String.valueOf(line)+".jpg");
            ref.putFile(selected)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                            hideDialog();
                            if(line.intValue()==1){
                                if(hasilFoto2.equals("Y")){
                                    uploadImage(nomor, 2, selectedImage2);
                                }else if(hasilFoto3.equals("Y")){
                                    uploadImage(nomor, 3, selectedImage3);
                                }else if(hasilFoto4.equals("Y")){
                                    uploadImage(nomor, 4, selectedImage3);
                                }else{
                                    Toast.makeText(MsTempatWisataItemActivity.this, "GAMBAR 1 BERHASIL DIUPLOAD!", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }
                            }else if(line.intValue()==2){
                                if(hasilFoto3.equals("Y")){
                                    uploadImage(nomor, 3, selectedImage3);
                                }else if(hasilFoto4.equals("Y")){
                                    uploadImage(nomor, 4, selectedImage3);
                                }else{
                                    Toast.makeText(MsTempatWisataItemActivity.this, "GAMBAR 1 BERHASIL DIUPLOAD!", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }
                            }else if(line.intValue()==3){
                                if(hasilFoto4.equals("Y")){
                                    uploadImage(nomor, 4, selectedImage3);
                                }else{
                                    Toast.makeText(MsTempatWisataItemActivity.this, "GAMBAR 1 BERHASIL DIUPLOAD!", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }
                            }else{
                                Toast.makeText(MsTempatWisataItemActivity.this, "GAMBAR 1 BERHASIL DIUPLOAD!", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK,returnIntent);
                                finish();
                            }
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(MsTempatWisataItemActivity.this,"Failed " + e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)MsTempatWisataItemActivity.this.getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateAlamat() {
        boolean value;
        if (edAlamat.getText().toString().isEmpty()){
            value=false;
            requestFocus(edAlamat);
            ilAlamat.setError(getString(R.string.err_msg_alamat));
        } else {
            value=true;
            ilAlamat.setError(null);
        }
        return value;
    }

    private boolean validateKategori() {
        boolean value;
        if (edKategori.getText().toString().isEmpty()){
            value=false;
            requestFocus(edAlamat);
            ilKategori.setError(getString(R.string.err_msg_kategori));
        } else {
            value=true;
            ilKategori.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_nama));
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateDeskripsi() {
        boolean value;
        if (edDes.getText().toString().isEmpty()){
            value=false;
            requestFocus(edDes);
            ilDes.setError(getString(R.string.err_msg_deskripsi));
        } else {
            value=true;
            ilDes.setError(null);
        }
        return value;
    }

    private boolean validateKab() {
        boolean value;
        if (eKab.getText().toString().isEmpty()){
            value=false;
            //requestFocus(edDes);
            ilKab.setError(getString(R.string.err_msg_namakab));
        } else {
            value=true;
            ilKab.setError(null);
        }
        return value;
    }

    private boolean validateMaps() {
        boolean value;
        if (new BigDecimal(latitude).compareTo(BigDecimal.ZERO)==0 && new BigDecimal(longtitude).compareTo(BigDecimal.ZERO)==0){
            value=false;
            Toast.makeText(MsTempatWisataItemActivity.this,
                            "Koordinat lokasi map, belum ditentukan", Toast.LENGTH_LONG)
                    .show();
        } else {
            value=true;
        }
        return value;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_MAP) {//map
            if(resultCode == RESULT_OK) {//map
                alamat = data.getStringExtra("alamat");
                latitude = data.getDoubleExtra("latitude",0);
                longtitude = data.getDoubleExtra("longtitude",0);
                edAlamat.setText(alamat);
                alamatMaps=true;
            }else{
                alamatMaps=false;
            }
        }
        if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edKategori.setText(data.getStringExtra("nama"));
                kodeKategori = data.getStringExtra("kode");
            }
        }
        if (requestCode == 3) {
            if(resultCode == RESULT_OK) {
                eProv.setText(data.getStringExtra("namaProv"));
                kodeProv = data.getStringExtra("kodeProv");
            }
        }
        if (requestCode == 4) {
            if(resultCode == RESULT_OK) {
                eKab.setText(data.getStringExtra("namaKab"));
                kodeKab = data.getStringExtra("kodeKab");
            }
        }
        if (requestCode == 6) {
            if(resultCode == RESULT_OK) {
                model = (MsWisataModel) data.getExtras().getSerializable("data");
            }
        }
        if (requestCode == 21 && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto1 = "Y";
                selectedImage1 = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage1, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                img1.setBackgroundResource(0);
                GlideApp.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .override(Target.SIZE_ORIGINAL)
                        .into(img1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 22 && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto2 = "Y";
                selectedImage2 = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage2, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                img2.setBackgroundResource(0);
                GlideApp.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .override(Target.SIZE_ORIGINAL)
                        .into(img2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 23 && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto3 = "Y";
                selectedImage3 = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage3, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                img3.setBackgroundResource(0);
                GlideApp.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .override(Target.SIZE_ORIGINAL)
                        .into(img3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 24 && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto4 = "Y";
                selectedImage4 = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage4, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                img4.setBackgroundResource(0);
                GlideApp.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .override(Target.SIZE_ORIGINAL)
                        .into(img4);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}