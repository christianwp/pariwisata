package com.example.pariwisata.master;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsEvent;
import com.example.pariwisata.model.master.MsEventModel;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MsEventActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsEvent adapter;
    private ListView lsvupload;
    private ArrayList<MsEventModel> columnlist= new ArrayList<MsEventModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeWisata;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;
    private MsWisataHeaderModel headerModel;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_event);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                headerModel = (MsWisataHeaderModel)i.getSerializable("model");
                kodeWisata = headerModel.getKodeWisata();
            } catch (Exception e) {}
        }
        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterEventAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterEventBack);
        lsvupload	= (ListView)findViewById(R.id.listMasterEvent);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterEventStatus);

        adapter		= new AdpMsEvent(MsEventActivity.this, R.layout.adp_ms_event, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(level.equals("E"))
            ImgAdd.setVisibility(View.VISIBLE);
        else
            ImgAdd.setVisibility(View.GONE);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(level.equals("E")){
                    Intent i  = new Intent(MsEventActivity.this, MsEventItemActivity.class);
                    MsEventModel model = new MsEventModel();
                    model.setKodeWisata(kodeWisata);
                    model.setNamaWisata(headerModel.getNamaWisata());
                    i.putExtra("Status", "ADD");
                    i.putExtra("model", model);
                    startActivityForResult(i, RESULT_DATA);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(MsEventActivity.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                }
            }
        });
        getData(Link.BASE_URL_API + Link.MS_EVENT, kodeWisata);
    }

    private void getData(String Url, final String kodeWisata){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsEventModel colums 	= new MsEventModel();
                            colums.setKodeWisata((String)((JSONObject) object).get("c_kodewisata"));
                            colums.setNamaWisata((String)((JSONObject) object).get("vc_namawisata"));
                            colums.setKodeEvent((String)((JSONObject) object).get("c_idevent"));
                            colums.setNamaEvent((String)((JSONObject) object).get("vc_namaevent"));
                            colums.setDeskripsi((String)((JSONObject) object).get("t_deskripsi"));
                            colums.setTglAwal((String)((JSONObject) object).get("dt_start"));
                            colums.setTglAkhir((String)((JSONObject) object).get("dt_end"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsEventActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsEventActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsEventActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsEventActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsEventActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsEventActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsEventActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("kodeWisata", kodeWisata);
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<>();
        adapter		= new AdpMsEvent(MsEventActivity.this, R.layout.adp_ms_event, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<>();
                adapter		= new AdpMsEvent(MsEventActivity.this, R.layout.adp_ms_event, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_EVENT, kodeWisata);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}