package com.example.pariwisata.master;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpViewRating;
import com.example.pariwisata.model.master.MsViewRatingModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MsViewRatingActivity extends AppCompatActivity {

    private ImageView imgBack;
    private ListView lsvupload;
    private AdpViewRating adapter;
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private ProgressDialog pDialog;
    private String kodeWisata;
    private ArrayList<MsViewRatingModel> columnlist= new ArrayList<MsViewRatingModel>();

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_rating_wisata);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                kodeWisata = i.getString("kode");
            } catch (Exception e) {}
        }
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack		= (ImageView)findViewById(R.id.imgViewRatingBack);
        lsvupload	= (ListView)findViewById(R.id.listViewRating);
        tvstatus	= (TextView)findViewById(R.id.txtListViewRatingStatus);

        adapter		= new AdpViewRating(MsViewRatingActivity.this, R.layout.adp_view_rating, columnlist);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getData(Link.BASE_URL_API + Link.KOMENTAR, kodeWisata);
    }

    private void getData(String Url, String kode){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else if(sucses==1){
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsViewRatingModel colums 	= new MsViewRatingModel();
                            colums.setKodeWisata(kode);
                            colums.setNilai(Integer.valueOf((String)((JSONObject) object).get("n_nilai")));
                            colums.setKomentar((String)((JSONObject) object).get("t_komentar"));
                            colums.setKodeUser((String)((JSONObject) object).get("c_userid"));
                            colums.setNamaUser((String)((JSONObject) object).get("vc_username"));
                            colums.setTanggal((String)((JSONObject) object).get("dt_tanggal"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }else{
                        Toast.makeText(MsViewRatingActivity.this,jsonrespon.getString("message"), Toast.LENGTH_SHORT).show();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsViewRatingActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsViewRatingActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsViewRatingActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsViewRatingActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsViewRatingActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsViewRatingActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsViewRatingActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("view", "VIEW");
                params.put("kodeWisata", kode);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}