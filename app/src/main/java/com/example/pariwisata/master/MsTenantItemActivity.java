package com.example.pariwisata.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsTenantItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, kodeWisata, namaWisata, kodeTenant, namaTenant, desk, kodeWst, namaWst;
    private ImageView imgBack;
    private TextInputEditText edWisata, edNama, edDeskripsi;
    private TextInputLayout ilWisata, ilNama, ilDeskripsi;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_tenant_item);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
            kodeWst = shared.getString(PrefUtil.KDWST, null);
            namaWst = shared.getString(PrefUtil.NMWST, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                kodeWisata = i.getString("kodeWisata");
                namaWisata = i.getString("namaWisata");
                kodeTenant = i.getString("kodeTenant");
                namaTenant = i.getString("namaTenant");
                desk = i.getString("deskripsi");
            } catch (Exception e) {}
        }
        imgBack = (ImageView)findViewById(R.id.imgViewTenantDetailBack);
        edWisata = (TextInputEditText) findViewById(R.id.eAddViewTenantDetailWisata);
        ilWisata = (TextInputLayout) findViewById(R.id.input_layout_Tenant_wisata);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewTenantDetailNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_Tenant_nama);
        edDeskripsi = (TextInputEditText) findViewById(R.id.eAddViewTenantDetailDeskripsi);
        ilDeskripsi = (TextInputLayout) findViewById(R.id.input_layout_Tenant_deskripsi);
        btnSave = (Button) findViewById(R.id.btnViewTenantDetailSave);

        if(level.equals("U")){
            edWisata.setEnabled(false);
            kodeWisata = kodeWst;
            namaWisata = namaWst;
        }
        edWisata.setText(namaWisata);
        if(status.equals("EDIT")){
            edWisata.setEnabled(false);
            edWisata.setText(namaWisata);
            edNama.setText(namaTenant);
            edDeskripsi.setText(desk);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateWisata() && validateNama() && validateDeskripsi()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsTenantItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(status.equals("ADD")){
                                        insertData(edNama.getText().toString(), kodeWisata,
                                                edDeskripsi.getText().toString(), userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(kodeWisata, kodeTenant, edNama.getText().toString(),
                                                edDeskripsi.getText().toString(), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private void insertData(String nama, String wisata, String deskripsi, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.tenant("", wisata, nama, deskripsi, userId, tanggalNow, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsTenantItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsTenantItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsTenantItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsTenantItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsTenantItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsTenantItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeWisata, String kodeTenant, String nama, String deskripsi, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.tenant(kodeTenant, kodeWisata, nama, deskripsi, userId, tanggalNow, "UPDATE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsTenantItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsTenantItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsTenantItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsTenantItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsTenantItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsTenantItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateWisata() {
        boolean value;
        if (edWisata.getText().toString().isEmpty()){
            value=false;
            requestFocus(edWisata);
            ilWisata.setError(getString(R.string.err_msg_wisata));
        } else if (edWisata.length() > ilWisata.getCounterMaxLength()) {
            value=false;
            ilWisata.setError("Max character length is " + ilWisata.getCounterMaxLength());
        } else {
            value=true;
            ilWisata.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_namatenant));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateDeskripsi() {
        boolean value;
        if (edDeskripsi.getText().toString().isEmpty()){
            value=false;
            requestFocus(edDeskripsi);
            ilDeskripsi.setError(getString(R.string.err_msg_deskripsi));
        } else if (edDeskripsi.length() > ilDeskripsi.getCounterMaxLength()) {
            value=false;
            ilWisata.setError("Max character length is " + ilDeskripsi.getCounterMaxLength());
        } else {
            value=true;
            ilDeskripsi.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
