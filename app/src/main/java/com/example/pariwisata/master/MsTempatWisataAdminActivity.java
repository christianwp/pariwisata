package com.example.pariwisata.master;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMasterWisata;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MsTempatWisataAdminActivity extends AppCompatActivity {

    private ImageView imgBack;
    private AdpMasterWisata adapter;
    private ListView lsvupload;
    private ArrayList<MsWisataHeaderModel> columnlist= new ArrayList<MsWisataHeaderModel>();
    private TextView tvstatus;
    private ProgressBar prbstatus;
    private String getUpload	="wisata_list_admin.php";
    private String kodeWisata;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_tempat_wisata_admin);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
            kodeWisata = shared.getString(PrefUtil.KDWST, null);
        }catch (Exception e){e.getMessage();}
        imgBack		= (ImageView)findViewById(R.id.imgMasterWisataAdminBack);
        lsvupload	= (ListView)findViewById(R.id.listMasterWisataAdmin);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterWisataAdminStatus);
        prbstatus	= (ProgressBar)findViewById(R.id.prbListMasterWisataAdminStatus);

        adapter		= new AdpMasterWisata(MsTempatWisataAdminActivity.this, R.layout.adp_ms_wisata, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.GONE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lsvupload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MsWisataHeaderModel mdl = new MsWisataHeaderModel();
                mdl = columnlist.get(position);
                Intent i = new Intent(MsTempatWisataAdminActivity.this, MsInfoWisata.class);
                i.putExtra("kode", mdl.getKodeWisata());
                i.putExtra("nama", mdl.getNamaWisata());
                i.putExtra("latt", mdl.getLatt());
                i.putExtra("longt", mdl.getLongt());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //https://www.geeksforgeeks.org/auto-image-slider-in-android-with-example/
                //https://abhiandroid.com/ui/ratingbar#:~:text=A%20user%20can%20simply%20touch,a%20subclass%20of%20AbsSeekBar%20class.
            }
        });

        getDataUpload(Link.BASE_URL_API+getUpload, kodeWisata);
    }

    private void getDataUpload(String Url, final String kode){
        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.VISIBLE);
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    if (sucses==1){
                        tvstatus.setVisibility(View.GONE);
                        prbstatus.setVisibility(View.GONE);
                        adapter.clear();
                        JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                        for (int i = 0; i < JsonArray.length(); i++) {
                            JSONObject object = JsonArray.getJSONObject(i);
                            MsWisataHeaderModel colums 	= new MsWisataHeaderModel();
                            colums.setKodeWisata(object.getString("c_kodewisata"));
                            colums.setNamaWisata(object.getString("vc_namawisata"));
                            colums.setAlamat(object.getString("vc_alamat"));
                            colums.setTelp(object.getString("vc_telp"));
                            colums.setLatt(object.getDouble("d_latt"));
                            colums.setLongt(object.getDouble("d_longt"));
                            colums.setDeskripsi(object.getString("t_deskripsi"));
                            colums.setTelp(object.getString("vc_telp"));
                            colums.setKodeKategori(object.getString("c_idkategori"));
                            colums.setNamaKategori(object.getString("vc_namakategori"));
                            colums.setKodeProv(object.getString("c_idprovinsi"));
                            colums.setNamaProv(object.getString("vc_nameprovinsi"));
                            colums.setKodeKab(object.getString("c_idkabkota"));
                            colums.setNamaKab(object.getString("vc_namekabkota"));
                            columnlist.add(colums);
                        }
                    }else{
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Tidak Ada Data");
                        prbstatus.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
                //lsvupload.invalidate();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check Koneksi Internet Anda");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof AuthFailureError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("AuthFailureError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ServerError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ServerError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof NetworkError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check NetworkError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ParseError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ParseError");
                    prbstatus.setVisibility(View.GONE);
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeWisata", kode);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }
}
