package com.example.pariwisata.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.model.master.MsEventModel;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsEventItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status;
    private ImageView imgBack, imgAwal, imgAkhir;
    private TextInputEditText edWisata, edNama, edDeskripsi, edTglAwal, edTglAkhir;
    private TextInputLayout ilWisata, ilNama, ilDeskripsi, ilTglAwal, ilTglAkhir;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private MsEventModel model;
    private DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar dateAndTime = Calendar.getInstance();
    private Calendar dateAndTimeTo = Calendar.getInstance();
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_event_item);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                model = (MsEventModel) i.getSerializable("model");
            } catch (Exception e) {}
        }
        imgBack = (ImageView)findViewById(R.id.imgViewEventDetailBack);
        imgAwal = (ImageView)findViewById(R.id.imgViewEventTglAwal);
        imgAkhir = (ImageView)findViewById(R.id.imgViewEventTglAkhir);
        edWisata = (TextInputEditText) findViewById(R.id.eAddViewEventDetailWisata);
        ilWisata = (TextInputLayout) findViewById(R.id.input_layout_Event_wisata);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewEventDetailNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_Event_nama);
        edDeskripsi = (TextInputEditText) findViewById(R.id.eAddViewEventDetailDeskripsi);
        ilDeskripsi = (TextInputLayout) findViewById(R.id.input_layout_Event_deskripsi);
        edTglAwal = (TextInputEditText) findViewById(R.id.eAddViewEventDetailTglAwal);
        ilTglAwal = (TextInputLayout) findViewById(R.id.input_layout_Event_tgl_awal);
        edTglAkhir = (TextInputEditText) findViewById(R.id.eAddViewEventDetailTglAkhir);
        ilTglAkhir = (TextInputLayout) findViewById(R.id.input_layout_Event_tgl_akhir);
        btnSave = (Button) findViewById(R.id.btnViewEventDetailSave);

        edWisata.setEnabled(false);
        edWisata.setText(model.getNamaWisata());
        if(status.equals("EDIT")){
            edNama.setText(model.getNamaEvent());
            edDeskripsi.setText(model.getDeskripsi());
            edTglAwal.setText(model.getTglAwal());
            edTglAkhir.setText(model.getTglAkhir());
            Date tglAwal = Calendar.getInstance().getTime();
            Date tglAkhir = Calendar.getInstance().getTime();
            try{
                tglAwal = df.parse(model.getTglAwal());
                tglAkhir = df.parse(model.getTglAkhir());
                edTglAwal.setText(sdf1.format(tglAwal));
                edTglAkhir.setText(sdf1.format(tglAkhir));
            }catch (Exception ex){}
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        imgAwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MsEventItemActivity.this, dFrom, dateAndTime.get(Calendar.YEAR), dateAndTime.get(Calendar.MONTH),
                        dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        imgAkhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MsEventItemActivity.this, dTo, dateAndTimeTo.get(Calendar.YEAR), dateAndTimeTo.get(Calendar.MONTH),
                        dateAndTimeTo.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateWisata() && validateNama() && validateDeskripsi() && validateTglAwal() && validateTglAkhir()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsEventItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Date tglAwal = Calendar.getInstance().getTime();
                                    Date tglAkhir = Calendar.getInstance().getTime();
                                    try{
                                        tglAwal = Utils.getFirstTimeOfDay(sdf1.parse(edTglAwal.getText().toString()));
                                        tglAkhir = Utils.getFirstTimeOfDay(sdf1.parse(edTglAkhir.getText().toString()));
                                    }catch (Exception ex){}
                                    if(status.equals("ADD")){
                                        insertData(edNama.getText().toString(), model.getKodeWisata(),
                                                edDeskripsi.getText().toString(), df.format(tglAwal), df.format(tglAkhir), userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(model.getKodeWisata(), model.getKodeEvent(), edNama.getText().toString(),
                                                edDeskripsi.getText().toString(), df.format(tglAwal), df.format(tglAkhir), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener dFrom =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, month);
            dateAndTime.set(Calendar.DAY_OF_MONTH, day);
            updatelabelFrom();
        }
    };

    private void updatelabelFrom(){
        edTglAwal.setText(sdf1.format(dateAndTime.getTime()));
    }

    private DatePickerDialog.OnDateSetListener dTo =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTimeTo.set(Calendar.YEAR, year);
            dateAndTimeTo.set(Calendar.MONTH, month);
            dateAndTimeTo.set(Calendar.DAY_OF_MONTH, day);
            updatelabelTo();
        }
    };

    private void updatelabelTo(){
        edTglAkhir.setText(sdf1.format(dateAndTimeTo.getTime()));
    }

    private void insertData(String nama, String wisata, String deskripsi, String tglAwal, String tglAkhir, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.event("", wisata, nama, deskripsi, tglAwal, tglAkhir, userId, tanggalNow, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsEventItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsEventItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsEventItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsEventItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsEventItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsEventItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeWisata, String kodeEvent, String nama, String deskripsi, String tglAwal,
                            String tglAkhir, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.event(kodeEvent, kodeWisata, nama, deskripsi, tglAwal, tglAkhir, userId, tanggalNow, "UPDATE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsEventItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsEventItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsEventItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsEventItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsEventItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsEventItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateWisata() {
        boolean value;
        if (edWisata.getText().toString().isEmpty()){
            value=false;
            requestFocus(edWisata);
            ilWisata.setError(getString(R.string.err_msg_wisata));
        } else if (edWisata.length() > ilWisata.getCounterMaxLength()) {
            value=false;
            ilWisata.setError("Max character length is " + ilWisata.getCounterMaxLength());
        } else {
            value=true;
            ilWisata.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_namaevent));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateDeskripsi() {
        boolean value;
        if (edDeskripsi.getText().toString().isEmpty()){
            value=false;
            requestFocus(edDeskripsi);
            ilDeskripsi.setError(getString(R.string.err_msg_deskripsi));
        } else if (edDeskripsi.length() > ilDeskripsi.getCounterMaxLength()) {
            value=false;
            ilWisata.setError("Max character length is " + ilDeskripsi.getCounterMaxLength());
        } else {
            value=true;
            ilDeskripsi.setError(null);
        }
        return value;
    }

    private boolean validateTglAwal() {
        boolean value;
        if (edTglAwal.getText().toString().isEmpty()){
            value=false;
            ilTglAwal.setError(getString(R.string.err_msg_tgl_awal));
        } else {
            value=true;
            ilTglAwal.setError(null);
        }
        return value;
    }

    private boolean validateTglAkhir() {
        boolean value;
        if (edTglAkhir.getText().toString().isEmpty()){
            value=false;
            ilTglAkhir.setError(getString(R.string.err_msg_tgl_akhir));
        } else {
            value=true;
            ilTglAkhir.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
