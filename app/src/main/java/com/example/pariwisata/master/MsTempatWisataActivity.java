package com.example.pariwisata.master;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMasterWisata;
import com.example.pariwisata.list.ListKabView;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MsTempatWisataActivity extends AppCompatActivity {

    private ImageView imgBack, ImgAdd, imgMap;
    private AdpMasterWisata adapter;
    private ListView lsvupload;
    private ArrayList<MsWisataHeaderModel> columnlist= new ArrayList<MsWisataHeaderModel>();
    private TextView tvstatus;
    private ProgressBar prbstatus;
    private String getUpload	="wisata_list.php";
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeKab;
    private MsWisataModel model;
    private boolean GpsStatus ;
    private LocationManager locationManager ;
    private Button btnproses;
    private TextInputEditText edKab;
    private TextInputLayout ilKab;
    private CheckBox ckAll;
    private boolean all;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_tempat_wisata);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        imgBack		= (ImageView)findViewById(R.id.imgMasterWisataBack);
        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterWisataAdd);
        imgMap      = (ImageView)findViewById(R.id.imgMasterWisataMap);
        lsvupload	= (ListView)findViewById(R.id.listMasterWisata);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterWisataStatus);
        prbstatus	= (ProgressBar)findViewById(R.id.prbListMasterWisataStatus);
        btnproses = (Button)findViewById(R.id.btnMasterWstRetrieve);
        edKab = (TextInputEditText)findViewById(R.id.eMasterWstKab);
        ilKab = (TextInputLayout)findViewById(R.id.input_layout_wisata_kab);
        ckAll = (CheckBox)findViewById(R.id.ckAllWisata);

        imgMap.setVisibility(View.INVISIBLE);

        adapter		= new AdpMasterWisata(MsTempatWisataActivity.this, R.layout.adp_ms_wisata, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.GONE);

        /*if (level.equals("X")){
            ckAll.setEnabled(false);
            edKab.setEnabled(false);
            kodeKab = shared.getString(PrefUtil.KDKAB, "");
            edKab.setText(shared.getString(PrefUtil.NMKAB, ""));
            ImgAdd.setVisibility(View.VISIBLE);
        }else */if(level.equals("A")){
            ckAll.setEnabled(true);
            edKab.setEnabled(true);
            ImgAdd.setVisibility(View.VISIBLE);
        }else if(level.equals("C")){
            ckAll.setEnabled(true);
            edKab.setEnabled(true);
            ImgAdd.setVisibility(View.GONE);
        }else{
            ckAll.setEnabled(true);
            edKab.setEnabled(true);
        }

        ckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(all){
                    all=false;
                    edKab.setEnabled(true);
                    kodeKab = null;
                }else{
                    all=true;
                    kodeKab = "%";
                    edKab.setText(null);
                    edKab.setEnabled(false);
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edKab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edKab.setEnabled(false);
                hideKeyboard(v);
                pilihKab();
                edKab.setEnabled(true);
            }
        });

        edKab.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edKab.setEnabled(false);
                    hideKeyboard(v);
                    pilihKab();
                    edKab.setEnabled(true);
                }
            }
        });

        lsvupload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MsWisataHeaderModel mdl = new MsWisataHeaderModel();
                mdl = columnlist.get(position);
                Intent i = new Intent(MsTempatWisataActivity.this, MsInfoWisata.class);
                i.putExtra("kode", mdl.getKodeWisata());
                i.putExtra("nama", mdl.getNamaWisata());
                i.putExtra("latt", mdl.getLatt());
                i.putExtra("longt", mdl.getLongt());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //https://www.geeksforgeeks.org/auto-image-slider-in-android-with-example/
                //https://abhiandroid.com/ui/ratingbar#:~:text=A%20user%20can%20simply%20touch,a%20subclass%20of%20AbsSeekBar%20class.
            }
        });

        btnproses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kodeKab == null || kodeKab.equals("")) {
                    ilKab.setError(getString(R.string.err_msg_namakab));
                } else {
                    ilKab.setError(null);
                    getDataUpload(Link.BASE_URL_API+getUpload, kodeKab);
                }
            }
        });

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MsWisataModel model = new MsWisataModel();
                Intent i = new Intent(MsTempatWisataActivity.this, MsTempatWisataItemActivity.class);
                i.putExtra("Status", "ADD");
                i.putExtra("model", model);
                startActivityForResult(i, 9);
            }
        });
    }

    private void pilihKab(){
        Intent i = new Intent(MsTempatWisataActivity.this, ListKabView.class);
        i.putExtra("kode", "72");
        startActivityForResult(i, 4);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 9) {
            if(resultCode == RESULT_OK) {
                columnlist= new ArrayList<MsWisataHeaderModel>();
                adapter		= new AdpMasterWisata(MsTempatWisataActivity.this, R.layout.adp_ms_wisata, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getDataUpload(Link.BASE_URL_API+getUpload, kodeKab);
            }
        }
        if (requestCode == 4) {
            if(resultCode == RESULT_OK) {
                edKab.setText(data.getStringExtra("namaKab"));
                kodeKab = data.getStringExtra("kodeKab");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        columnlist= new ArrayList<MsWisataHeaderModel>();
//        adapter		= new AdpMasterWisata(MsTempatWisataActivity.this, R.layout.adp_ms_wisata, columnlist, level, userId);
//        lsvupload.setAdapter(adapter);
//        getDataUpload(Link.BASE_URL_API+getUpload, kodeKab);
//    }

    private void getDataUpload(String Url, final String kode){
        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.VISIBLE);
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    if (sucses==1){
                        tvstatus.setVisibility(View.GONE);
                        prbstatus.setVisibility(View.GONE);
                        adapter.clear();
                        JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                        for (int i = 0; i < JsonArray.length(); i++) {
                            JSONObject object = JsonArray.getJSONObject(i);
                            MsWisataHeaderModel colums 	= new MsWisataHeaderModel();
                            colums.setKodeWisata(object.getString("c_kodewisata"));
                            colums.setNamaWisata(object.getString("vc_namawisata"));
                            colums.setAlamat(object.getString("vc_alamat"));
                            colums.setTelp(object.getString("vc_telp"));
                            colums.setLatt(object.getDouble("d_latt"));
                            colums.setLongt(object.getDouble("d_longt"));
                            colums.setDeskripsi(object.getString("t_deskripsi"));
                            colums.setTelp(object.getString("vc_telp"));
                            colums.setKodeKategori(object.getString("c_idkategori"));
                            colums.setNamaKategori(object.getString("vc_namakategori"));
                            colums.setKodeProv(object.getString("c_idprovinsi"));
                            colums.setNamaProv(object.getString("vc_nameprovinsi"));
                            colums.setKodeKab(object.getString("c_idkabkota"));
                            colums.setNamaKab(object.getString("vc_namekabkota"));
                            columnlist.add(colums);
                        }
                    }else{
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Tidak Ada Data");
                        prbstatus.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
                //lsvupload.invalidate();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check Koneksi Internet Anda");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof AuthFailureError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("AuthFailureError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ServerError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ServerError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof NetworkError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check NetworkError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ParseError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ParseError");
                    prbstatus.setVisibility(View.GONE);
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kab", kode);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist= new ArrayList<MsWisataHeaderModel>();
        adapter		= new AdpMasterWisata(MsTempatWisataActivity.this, R.layout.adp_ms_wisata, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
    }
}
