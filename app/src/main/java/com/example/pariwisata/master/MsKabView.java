package com.example.pariwisata.master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsKabKota;
import com.example.pariwisata.list.ListProvinsiView;
import com.example.pariwisata.model.master.MsKabModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class MsKabView extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsKabKota adapter;
    private ListView lsvupload;
    private ArrayList<MsKabModel> columnlist= new ArrayList<MsKabModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeProv;
    private Button btnProses;
    private TextInputEditText edNama;
    private int RESULT_PROV = 2;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_kab);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterKabAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterKabBack);
        edNama = (TextInputEditText)findViewById(R.id.eMasterKabNamaProv);
        lsvupload	= (ListView)findViewById(R.id.listMasterKab);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterKabStatus);
        btnProses = (Button)findViewById(R.id.btnMasterKabRetrieve);

        adapter		= new AdpMsKabKota(MsKabView.this, R.layout.adp_ms_kabkota, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(!level.equals("A"))
            ImgAdd.setVisibility(View.GONE);
        else
            ImgAdd.setVisibility(View.VISIBLE);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateProv()){
                    if(level.equals("A")){
                        Intent i  = new Intent(MsKabView.this, MsKabItemView.class);
                        i.putExtra("Status", "ADD");
                        i.putExtra("kodeProv", kodeProv);
                        i.putExtra("namaProv", edNama.getText().toString());
                        startActivityForResult(i, RESULT_DATA);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }else{
                        Toast.makeText(MsKabView.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        edNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edNama.setEnabled(false);
                hideKeyboard(v);
                pilihProv();
                edNama.setEnabled(true);
            }
        });

        edNama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edNama.setEnabled(false);
                    hideKeyboard(v);
                    pilihProv();
                    edNama.setEnabled(true);
                }
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateProv()){
                    getData(Link.BASE_URL_API + Link.MS_KAB_KOTA, kodeProv);
                }
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihProv(){
        Intent i = new Intent(MsKabView.this, ListProvinsiView.class);
        startActivityForResult(i, RESULT_PROV);
    }

    private void getData(String Url, final String kodeProv){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsKabModel colums 	= new MsKabModel();
                            colums.setKodeProv((String)((JSONObject) object).get("c_idprovinsi"));
                            colums.setNamaProv((String)((JSONObject) object).get("vc_nameprovinsi"));
                            colums.setKodeKabKota((String)((JSONObject) object).get("c_idkabkota"));
                            colums.setNamaKabKota((String)((JSONObject) object).get("vc_namekabkota"));
                            colums.setCstat((String)((JSONObject) object).get("c_stat"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsKabView.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsKabView.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsKabView.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsKabView.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsKabView.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsKabView.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsKabView.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("kodeProv", kodeProv);
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean validateProv() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(MsKabView.this,"Provinsi harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<>();
        adapter		= new AdpMsKabKota(MsKabView.this, R.layout.adp_ms_kabkota, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_PROV) {
            if(resultCode == RESULT_OK) {
                kodeProv = data.getStringExtra("kodeProv");
                edNama.setText(data.getStringExtra("namaProv"));
            }
        }
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<>();
                adapter		= new AdpMsKabKota(MsKabView.this, R.layout.adp_ms_kabkota, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_KAB_KOTA, kodeProv);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}