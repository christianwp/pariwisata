package com.example.pariwisata.master;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMasterWisata;
import com.example.pariwisata.adapter.master.AdpWisataFasilitas;
import com.example.pariwisata.list.ListFasilitasView;
import com.example.pariwisata.model.master.MsWisataFasilitasModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import java.util.ArrayList;

public class MsTempatWisataFasilitasItemActivity extends AppCompatActivity {

    private ImageView imgBack, ImgAdd, imgSave;
    private ListView lsvupload;
    private AdpWisataFasilitas adapter;
    private ArrayList<MsWisataFasilitasModel> columnlist= new ArrayList<MsWisataFasilitasModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private MsWisataModel model;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_wisata_fasilitas);
        Intent i = getIntent();
        model = (MsWisataModel) i.getSerializableExtra("model");
        imgBack		= (ImageView)findViewById(R.id.imgMasterWisataFsBack);
        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterWisataFsAdd);
        imgSave     = (ImageView)findViewById(R.id.imgMasterWisataFsSave);
        lsvupload	= (ListView)findViewById(R.id.listMasterWisataFs);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterWisataFsStatus);

        adapter		= new AdpWisataFasilitas(MsTempatWisataFasilitasItemActivity.this, R.layout.adp_wisata_fasilitas, columnlist);
        lsvupload.setAdapter(adapter);
        if(!model.getItem().isEmpty()){
            for(MsWisataFasilitasModel item:model.getItem()){
                columnlist.add(item);
            }
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MsTempatWisataFasilitasItemActivity.this, ListFasilitasView.class);
                startActivityForResult(i, 9);
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(columnlist.isEmpty()) {
                    Toast.makeText(MsTempatWisataFasilitasItemActivity.this, "Pilih salah satu fasilitas", Toast.LENGTH_LONG).show();
                }else{
                    Intent returnIntent = new Intent();
                    model.setItem(columnlist);
                    returnIntent.putExtra("data", model);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
                //model.setItem(columnlist);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter		= new AdpWisataFasilitas(MsTempatWisataFasilitasItemActivity.this, R.layout.adp_wisata_fasilitas, columnlist);
        lsvupload.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9) {
            if(resultCode == RESULT_OK) {
                try{
                    MsWisataFasilitasModel fasilitas = new MsWisataFasilitasModel();
                    Integer kode = Integer.valueOf(data.getStringExtra("kodeFs"));
                    String nama = data.getStringExtra("namaFs");
                    if(!columnlist.isEmpty()){
                        for(MsWisataFasilitasModel item:columnlist){
                            if(Integer.valueOf(item.getKodeFasilitas()).intValue() == kode.intValue())
                                throw new Exception("Fasilitas sudah ditambahkan sebelumnya");
                        }
                    }
                    fasilitas.setKodeFasilitas(kode.toString());
                    fasilitas.setNamaFasilitas(nama);
                    fasilitas.setKodeWisata(model.getHeader()==null?null:model.getHeader().getKodeWisata());
                    adapter		= new AdpWisataFasilitas(MsTempatWisataFasilitasItemActivity.this, R.layout.adp_wisata_fasilitas, columnlist);
                    lsvupload.setAdapter(adapter);
                    columnlist.add(fasilitas);
                }catch (Exception ex){
                    Toast.makeText(MsTempatWisataFasilitasItemActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
