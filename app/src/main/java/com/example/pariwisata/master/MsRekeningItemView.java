package com.example.pariwisata.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.list.ListBankView;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsRekeningItemView extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, kodeTenant, namaTenant, kodeBank, namaBank, noRek, namaRek;
    private ImageView imgBack;
    private TextInputEditText edBank, edNoRek, edNama;
    private TextInputLayout ilBank, ilNoRek, ilNama;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_rek_item_view);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                kodeBank = i.getString("kodeBank");
                namaBank = i.getString("namaBank");
                kodeTenant = i.getString("kodeTenant");
                namaTenant = i.getString("namaTenant");
                noRek = i.getString("noRek");
                namaRek = i.getString("namaRek");
            } catch (Exception e) {}
        }
        imgBack = (ImageView)findViewById(R.id.imgViewRekDetailBack);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewMenuRekNamaRek);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_rek_namarek);
        edBank = (TextInputEditText) findViewById(R.id.eAddViewMenuRekBank);
        ilBank = (TextInputLayout) findViewById(R.id.input_layout_Rek_bank);
        edNoRek = (TextInputEditText) findViewById(R.id.eAddViewMenuRekNoRek);
        ilNoRek = (TextInputLayout) findViewById(R.id.input_layout_rek_norek);
        btnSave = (Button) findViewById(R.id.btnViewRekDetailSave);

        if(status.equals("EDIT")){
            edNama.setText(namaRek);
            edBank.setText(namaBank);
            edNoRek.setText(noRek);
            edBank.setEnabled(false);
            edNoRek.setEnabled(false);
        }else{
            edBank.setEnabled(true);
            edNoRek.setEnabled(true);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateNama() && validateBank() && validateNorek()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsRekeningItemView.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(status.equals("ADD")){
                                        insertData(kodeTenant, kodeBank, edNoRek.getText().toString(), edNama.getText().toString(), userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(kodeTenant, kodeBank, edNoRek.getText().toString(), edNama.getText().toString(), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edBank.setEnabled(false);
                hideKeyboard(v);
                pilihBank();
                edBank.setEnabled(true);
            }
        });

        edBank.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edBank.setEnabled(false);
                    hideKeyboard(v);
                    pilihBank();
                    edBank.setEnabled(true);
                }
            }
        });
    }

    private void insertData(String tenant, String kodeBank, String noRek, String namaRek, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.rekening(tenant, kodeBank, noRek, namaRek, userId, tanggalNow, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsRekeningItemView.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsRekeningItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsRekeningItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsRekeningItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsRekeningItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsRekeningItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeTenant, String kodeBank, String noRek, String namaRek, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.rekening(kodeTenant, kodeBank, noRek, namaRek, userId, tanggalNow, "UPDATE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsRekeningItemView.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsRekeningItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsRekeningItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsRekeningItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsRekeningItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsRekeningItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edBank.setText(data.getStringExtra("namaBank"));
                kodeBank = data.getStringExtra("kodeBank");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihBank(){
        Intent i = new Intent(MsRekeningItemView.this, ListBankView.class);
        startActivityForResult(i, 2);
    }

    private boolean validateBank() {
        boolean value;
        if (edBank.getText().toString().isEmpty()){
            value=false;
            requestFocus(edBank);
            ilBank.setError(getString(R.string.err_msg_namabank));
        } else if (edBank.length() > ilBank.getCounterMaxLength()) {
            value=false;
            ilBank.setError("Max character length is " + ilBank.getCounterMaxLength());
        } else {
            value=true;
            ilBank.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_namarek));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateNorek() {
        boolean value;
        if (edNoRek.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNoRek);
            ilNoRek.setError(getString(R.string.err_msg_norek));
        } else if (edNoRek.length() > ilNoRek.getCounterMaxLength()) {
            value=false;
            ilNoRek.setError("Max character length is " + ilNoRek.getCounterMaxLength());
        } else {
            value=true;
            ilNoRek.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}