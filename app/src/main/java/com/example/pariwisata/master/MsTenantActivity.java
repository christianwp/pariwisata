package com.example.pariwisata.master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.master.AdpMsTenant;
import com.example.pariwisata.list.ListProvinsiView;
import com.example.pariwisata.list.ListWisataView;
import com.example.pariwisata.model.master.MsTenantModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MsTenantActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsTenant adapter;
    private ListView lsvupload;
    private ArrayList<MsTenantModel> columnlist= new ArrayList<MsTenantModel>();
    private TextView tvstatus;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId, kodeWisata, namaWisata;
    private Button btnProses;
    private TextInputEditText edNama;
    private int RESULT_WISATA = 2;
    private int RESULT_DATA = 9;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_tenant);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
            kodeWisata = shared.getString(PrefUtil.KDWST, null);
            namaWisata = shared.getString(PrefUtil.NMWST, null);
        }catch (Exception e){e.getMessage();}

        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterTenantAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterTenantBack);
        edNama = (TextInputEditText)findViewById(R.id.eMasterTenantNamaWisata);
        lsvupload	= (ListView)findViewById(R.id.listMasterTenant);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterTenantStatus);
        btnProses = (Button)findViewById(R.id.btnMasterTenantRetrieve);

        adapter		= new AdpMsTenant(MsTenantActivity.this, R.layout.adp_ms_tenant, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(level.equals("U"))
            ImgAdd.setVisibility(View.VISIBLE);
        else
            ImgAdd.setVisibility(View.GONE);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateWisata()){
                    if(level.equals("A") || level.equals("U")){
                        Intent i  = new Intent(MsTenantActivity.this, MsTenantItemActivity.class);
                        i.putExtra("Status", "ADD");
                        i.putExtra("kodeWisata", kodeWisata);
                        i.putExtra("namaWisata", edNama.getText().toString());
                        startActivityForResult(i, RESULT_DATA);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }else{
                        Toast.makeText(MsTenantActivity.this, "Anda tidak berhak merubah master ini!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        if(level.equals("A")){
            edNama.setEnabled(true);
        }else if(level.equals("U")){
            edNama.setEnabled(false);
            edNama.setText(namaWisata);
        }else{
            Toast.makeText(MsTenantActivity.this, "Anda tidak berhak mengakses menu ini!", Toast.LENGTH_LONG).show();
            finish();
        }
        edNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(level.equals("A")){
                    edNama.setEnabled(false);
                    hideKeyboard(v);
                    pilihWisata();
                    edNama.setEnabled(true);
                }
            }
        });

        edNama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(level.equals("A")) {
                        edNama.setEnabled(false);
                        hideKeyboard(v);
                        pilihWisata();
                        edNama.setEnabled(true);
                    }
                }
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateWisata()){
                    getData(Link.BASE_URL_API + Link.MS_TENANT, kodeWisata);
                }
            }
        });
    }

    private void getData(String Url, final String kodeWisata){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                    adapter.clear();
                    if(sucses==0){
                        tvstatus.setVisibility(View.VISIBLE);
                        tvstatus.setText("Data Tidak Ada");
                        hideDialog();
                    }else{
                        tvstatus.setVisibility(View.INVISIBLE);
                        for (int i = 0; i <JsonArray.length(); i++) {
                            Object object = JsonArray.get(i);
                            MsTenantModel colums 	= new MsTenantModel();
                            colums.setKodeWisata((String)((JSONObject) object).get("c_kodewisata"));
                            colums.setNamaWisata((String)((JSONObject) object).get("vc_namawisata"));
                            colums.setKodeTenant((String)((JSONObject) object).get("c_kodetenant"));
                            colums.setNamaTenant((String)((JSONObject) object).get("vc_namatenant"));
                            colums.setKodePemilik((String)((JSONObject) object).get("c_pemilik"));
                            colums.setNamaPemilik((String)((JSONObject) object).get("vc_username"));
                            colums.setDeskripsi((String)((JSONObject) object).get("t_deskripsi"));
                            columnlist.add(colums);
                        }
                        adapter.notifyDataSetChanged();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MsTenantActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(MsTenantActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(MsTenantActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(MsTenantActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(MsTenantActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(MsTenantActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(MsTenantActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("kodeWisata", kodeWisata);
                params.put("view", "VIEW");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihWisata(){
        Intent i = new Intent(MsTenantActivity.this, ListWisataView.class);
        i.putExtra("kode", "%");
        startActivityForResult(i, RESULT_WISATA);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private boolean validateWisata() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(MsTenantActivity.this,"Wisata harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<>();
        adapter		= new AdpMsTenant(MsTenantActivity.this, R.layout.adp_ms_tenant, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_WISATA) {
            if(resultCode == RESULT_OK) {
                kodeWisata = data.getStringExtra("kodeWisata");
                edNama.setText(data.getStringExtra("namaWisata"));
            }
        }
        if(requestCode == RESULT_DATA) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<>();
                adapter		= new AdpMsTenant(MsTenantActivity.this, R.layout.adp_ms_tenant, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getData(Link.BASE_URL_API + Link.MS_TENANT, kodeWisata);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
