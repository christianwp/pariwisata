package com.example.pariwisata.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pariwisata.R;
import com.example.pariwisata.service.BaseApiService;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MsKabItemView extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, nama, kodeProv, namaProv, kodeKab;
    private ImageView imgBack;
    private TextInputEditText edNama, edProv;
    private TextInputLayout ilNama, ilProv;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ms_kab_item_view);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                kodeProv = i.getString("kodeProv");
                kodeKab = i.getString("kodeKab");
                nama = i.getString("namaKab");
                namaProv = i.getString("namaProv");
            } catch (Exception e) {}
        }
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.imgViewKabDetailBack);
        edProv = (TextInputEditText) findViewById(R.id.eAddViewKabDetailNamaProv);
        ilProv = (TextInputLayout) findViewById(R.id.input_layout_KabDetail_nama_prov);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewKabDetailNamaKab);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_KabDetail_namaKab);
        btnSave = (Button) findViewById(R.id.btnViewKabDetailSave);

        edProv.setText(namaProv);
        if(status.equals("EDIT")){
            edNama.setText(nama);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateNama() && validatePrv()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MsKabItemView.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(status.equals("ADD")){
                                        insertData(edNama.getText().toString(), kodeProv, userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(kodeProv, kodeKab, edNama.getText().toString(), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private void insertData(String nama, String kodeProv, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.kabKota(kodeProv, "", nama, "INSERT")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsKabItemView.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsKabItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsKabItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsKabItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsKabItemView.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsKabItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeProv, String kodeKab, String namaKab, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.kabKota(kodeProv, kodeKab, namaKab, "UPDATE")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MsKabItemView.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MsKabItemView.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsKabItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MsKabItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MsKabItemView.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MsKabItemView.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validatePrv() {
        boolean value;
        if (edProv.getText().toString().isEmpty()){
            value=false;
            requestFocus(edProv);
            ilProv.setError(getString(R.string.err_msg_namaprov));
        } else if (edProv.length() > ilProv.getCounterMaxLength()) {
            value=false;
            ilProv.setError("Max character length is " + ilProv.getCounterMaxLength());
        } else {
            value=true;
            ilProv.setError(null);
        }
        return value;
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_namakab));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}