package com.example.pariwisata.adapter.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.master.MsTempatWisataItemActivity;
import com.example.pariwisata.master.MsTenanWisataActivity;
import com.example.pariwisata.model.master.MsWisataHeaderModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdpMasterWisata extends ArrayAdapter<MsWisataHeaderModel> {

    private List<MsWisataHeaderModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private ListView lsvchoose;
    private static final int SEND_UPLOAD = 201;
    private AlertDialog alert;
    private String FileDeteleted = "wisata_delete.php";
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String levelku, userIdku;

    public AdpMasterWisata(Context context, int resource, List<MsWisataHeaderModel> objects, String level,
                           String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNama	    =	 (TextView)v.findViewById(R.id.txtColMasterWisataNama);
            holder.TvAlamat	    =	 (TextView)v.findViewById(R.id.txtColMasterWisataAlamat);
            holder.TvTelp	    =	 (TextView)v.findViewById(R.id.txtColMasterWisataTelp);
            holder.Tvkab = (TextView)v.findViewById(R.id.txtColMasterWisatakab);
            holder.ImgDelete	=	 (ImageView)v.findViewById(R.id.imgColMasterWisataDelete);
            holder.ImgEdit		=	 (ImageView)v.findViewById(R.id.imgColMasterWisataEdit);
            holder.ImgUmkm		=	 (ImageView)v.findViewById(R.id.imgColMasterWisataTenant);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        holder.ImgDelete.setVisibility(View.VISIBLE);
        holder.ImgEdit.setVisibility(View.VISIBLE);

        if(levelku.equals("A") || levelku.equals("E")){
            holder.ImgDelete.setVisibility(View.VISIBLE);
            holder.ImgEdit.setVisibility(View.VISIBLE);
        }else{
            holder.ImgDelete.setVisibility(View.GONE);
            holder.ImgEdit.setVisibility(View.GONE);
        }

        holder.TvNama.setText(columnslist.get(position).getNamaWisata());
        holder.TvAlamat.setText(columnslist.get(position).getAlamat().trim());
        holder.TvTelp.setText(columnslist.get(position).getTelp());
        holder.Tvkab.setText(columnslist.get(position).getNamaKab());

        holder.ImgUmkm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MsTenanWisataActivity.class);
                i.putExtra("kode", columnslist.get(position).getKodeWisata());
                ((Activity) context).startActivity(i);
            }
        });

        holder.ImgEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MsTempatWisataItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kode", columnslist.get(position).getKodeWisata());
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.ImgDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kode = columnslist.get(position).getKodeWisata();
                        deletedData(Link.BASE_URL_API + FileDeteleted, position, kode, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });
        return v;
    }

    private void deletedData(String save, final int position, final String kode, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            int Sucsess = jsonrespon.getInt("success");
                            String message = jsonrespon.getString("message");
                            if (Sucsess ==1 ){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kode", kode);
                params.put("kodeUser", userId);
                params.put("tglNow", tanggalNow);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private TextView TvNama;
        private TextView TvAlamat;
        private TextView TvTelp;
        private TextView Tvkab;
        private ImageView ImgDelete;
        private ImageView ImgEdit;
        private ImageView ImgUmkm;
    }
}
