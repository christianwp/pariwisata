package com.example.pariwisata.adapter.master;

import static com.example.pariwisata.utilities.Link.MS_MENU;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.master.MsMenuActivityItemView;
import com.example.pariwisata.model.master.MsMenuModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdpMsMenu extends ArrayAdapter<MsMenuModel> {

    private List<MsMenuModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private String levelku, userIdku;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
    private DecimalFormat formatter = new DecimalFormat("###,##0.00", symbols);

    public AdpMsMenu(Context context, int resource, List<MsMenuModel> objects, String level,
                       String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new AdpMsMenu.ViewHolder();
            v= vi.inflate(Resource, null);
            holder.tvJenis    =	 (TextView)v.findViewById(R.id.txtColMenuNmJenis);
            holder.tvNama    =	 (TextView)v.findViewById(R.id.txtColMenuNmMenu);
            holder.tvHarga    =	 (TextView)v.findViewById(R.id.txtColMenuNmHarga);
            holder.imgDel = (ImageView)v.findViewById(R.id.imgColMasterMenuDelete);
            holder.imgEdit = (ImageView)v.findViewById(R.id.imgColMasterMenuEdit);
            v.setTag(holder);
        }else{
            holder 	= (AdpMsMenu.ViewHolder)v.getTag();
        }
        if(levelku.equals("U")){
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.imgDel.setVisibility(View.VISIBLE);
        }else{
            holder.imgEdit.setVisibility(View.GONE);
            holder.imgDel.setVisibility(View.GONE);
        }
        holder.tvJenis.setText("Jenis: "+columnslist.get(position).getNamaJenis());
        holder.tvNama.setText("Nama Menu: "+columnslist.get(position).getNamaMenu());
        holder.tvHarga.setText("Rp "+formatter.format(columnslist.get(position).getHarga().setScale(2)));

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), MsMenuActivityItemView.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kodeMenu", columnslist.get(position).getKodeMenu());
                i.putExtra("namaMenu", columnslist.get(position).getNamaMenu());
                i.putExtra("kodeTenant", columnslist.get(position).getKodeTenant());
                i.putExtra("namaTenant", columnslist.get(position).getNamaTenant());
                i.putExtra("kodeJenis", columnslist.get(position).getKodeJenis());
                i.putExtra("namaJenis", columnslist.get(position).getNamaJenis());
                i.putExtra("harga", Double.valueOf(String.valueOf(columnslist.get(position).getHarga())));
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kodeMenu = columnslist.get(position).getKodeMenu();
                        deletedData(Link.BASE_URL_API + MS_MENU, position, kodeMenu, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });
        return v;
    }

    private void deletedData(String save, final int position, final String kodeMenu, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            int Sucsess = jsonrespon.getInt("success");
                            String message = jsonrespon.getString("message");
                            if (Sucsess ==1 ){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeMenu", kodeMenu);
                params.put("tglNow", tanggalNow);
                params.put("kodeUser", userId);
                params.put("view", "DELETE");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private TextView tvJenis;
        private TextView tvNama;
        private TextView tvHarga;
        private ImageView imgDel;
        private ImageView imgEdit;
    }
}
