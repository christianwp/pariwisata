package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListKab;

import java.util.ArrayList;

public class AdpKab extends RecyclerView.Adapter<AdpKab.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListKab> mArrayList;
    private ArrayList<ListKab> mFilteredList;

    public AdpKab(Context contextku, ArrayList<ListKab> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpKab.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_kab, viewGroup, false);
        return new AdpKab.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpKab.ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(mFilteredList.get(i).getKodeKab());
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaKab());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListKab> filteredList = new ArrayList<>();
                    for (ListKab entity : mArrayList) {
                        if (entity.getKodeKab().toLowerCase().contains(charString) ||
                                entity.getNamaKab().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListKab>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_kab_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_kab_nama);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeKab", tv_kode.getText().toString());
            intent.putExtra("namaKab", tv_nama.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
