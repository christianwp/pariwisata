package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListWisata;

import java.util.ArrayList;

public class AdpWisata extends RecyclerView.Adapter<AdpWisata.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListWisata> mArrayList;
    private ArrayList<ListWisata> mFilteredList;

    public AdpWisata(Context contextku, ArrayList<ListWisata> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpWisata.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_wisata, viewGroup, false);
        return new AdpWisata.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpWisata.ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(mFilteredList.get(i).getKodeWisata());
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaWisata());
        viewHolder.tvAlamat.setText("Alamat: "+mFilteredList.get(i).getAlamat());
        viewHolder.tvKabKota.setText(mFilteredList.get(i).getKabKota());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListWisata> filteredList = new ArrayList<>();
                    for (ListWisata entity : mArrayList) {
                        if (entity.getKodeWisata().toLowerCase().contains(charString) ||
                                entity.getNamaWisata().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListWisata>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama, tvAlamat, tvKabKota;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_wisata_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_wisata_nama);
            tvAlamat = (TextView)view.findViewById(R.id.txt_view_wisata_alamat);
            tvKabKota = (TextView)view.findViewById(R.id.txt_view_wisata_kabkota);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeWisata", tv_kode.getText().toString());
            intent.putExtra("namaWisata", tv_nama.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
