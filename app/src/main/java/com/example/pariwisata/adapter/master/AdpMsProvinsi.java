package com.example.pariwisata.adapter.master;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.master.MsProvinsiItemActivity;
import com.example.pariwisata.model.master.MsProvModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;

public class AdpMsProvinsi extends ArrayAdapter<MsProvModel> {

    private List<MsProvModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private String levelku, userIdku;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public AdpMsProvinsi(Context context, int resource, List<MsProvModel> objects, String level,
                        String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNamaProv    =	 (TextView)v.findViewById(R.id.txtColMasterProvNama);
            holder.imgDel = (ImageView)v.findViewById(R.id.imgColMasterProvDelete);
            holder.imgEdit = (ImageView)v.findViewById(R.id.imgColMasterProvEdit);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        if(levelku.equals("A")){
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.imgDel.setVisibility(View.VISIBLE);
        }else{
            holder.imgEdit.setVisibility(View.GONE);
            holder.imgDel.setVisibility(View.GONE);
        }
        holder.TvNamaProv.setText(columnslist.get(position).getNamaProv());

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), MsProvinsiItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kodeProv", columnslist.get(position).getKodeProv());
                i.putExtra("namaProv", columnslist.get(position).getNamaProv());
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kodeProv = columnslist.get(position).getKodeProv();
                        deletedData(Link.BASE_URL_API + Link.MS_PROV, position, kodeProv, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });
        return v;
    }

    private void deletedData(String save, final int position, final String kodeProv, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            int Sucsess = jsonrespon.getInt("success");
                            String message = jsonrespon.getString("message");
                            if (Sucsess ==1 ){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeProv", kodeProv);
                params.put("view", "DELETE");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private TextView TvNamaProv;
        private ImageView imgDel;
        private ImageView imgEdit;
    }
}
