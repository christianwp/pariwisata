package com.example.pariwisata.adapter.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pariwisata.R;
import com.example.pariwisata.model.transaksi.OrderListModel;
import com.example.pariwisata.transaksi.KeranjangBayarActivity;
import com.example.pariwisata.transaksi.OrderCustomerActivity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class AdpOrder extends ArrayAdapter<OrderListModel> {

    private List<OrderListModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Context context;
    private AlertDialog alert;
    private String FileDeteleted = "Keranjang.php";
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private String levelku, userIdku;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

    public AdpOrder(Context context, int resource, List<OrderListModel> objects, String level,
                        String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v	=	convertView;
        if (v == null){
            holder	=	new AdpOrder.ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNo	    =	 (TextView)v.findViewById(R.id.txtColOrderNoTrans);
            holder.TvNilai	    =	 (TextView)v.findViewById(R.id.txtColOrderNilai);
            holder.TvStatus = (TextView)v.findViewById(R.id.txtColOrderStatusPesanan);
            holder.imgBayar = (ImageView)v.findViewById(R.id.imgColOrderLihatbayar);
            v.setTag(holder);
        }else {
            holder = (AdpOrder.ViewHolder) v.getTag();
        }
        if(columnslist.get(position).getCdstatus().substring(0,1).equals("*")){
            holder.imgBayar.setVisibility(View.GONE);
        }else{
            holder.imgBayar.setVisibility(View.generateViewId());
        }
        holder.TvNo.setText("No Transaksi: "+columnslist.get(position).getNoTransaksi().trim());
        holder.TvNilai.setText("Nominal Rp "+ String.valueOf(formatter.format(columnslist.get(position).getNilai().setScale(0))));
        holder.TvStatus.setText("Status: "+
                (columnslist.get(position).getCdstatus().substring(0,1).equals("")?"Transaksi belum dibayar":
                (columnslist.get(position).getCdstatus().substring(1,2).equals("")?"Transaksi lunas. Menunggu konfirmasi tenant ":
                        columnslist.get(position).getCdstatus().substring(2,3).equals("*")?"Sedang disiapkan oleh tenant":
                                "Transaksi Pesanan selesai")));

        holder.imgBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(columnslist.get(position).getCdstatus().substring(0,1).equals("*")){
                    Toast.makeText(getContext(),"Transaksi belum dibayar!", Toast.LENGTH_LONG).show();
                }else{
                    Intent i = new Intent(getContext(), KeranjangBayarActivity.class);
                    i.putExtra("noTransaksi", columnslist.get(position).getNoTransaksi().trim());
                    i.putExtra("status", "VIEW");
                    ((Activity) context).startActivity(i);
                }
            }
        });
        return v;
    }

    static class ViewHolder{
        private TextView TvNo;
        //private TextView TvStatusBayar;
        private TextView TvNilai;
        private TextView TvStatus;
        private ImageView imgBayar;
    }
}
