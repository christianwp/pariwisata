package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListRekeningBank;

import java.util.ArrayList;

public class AdpRekBank extends RecyclerView.Adapter<AdpRekBank.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListRekeningBank> mArrayList;
    private ArrayList<ListRekeningBank> mFilteredList;

    public AdpRekBank(Context contextku, ArrayList<ListRekeningBank> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpRekBank.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_rek_bank, viewGroup, false);
        return new AdpRekBank.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpRekBank.ViewHolder viewHolder, int i) {
        viewHolder.tv_kodebank.setText(mFilteredList.get(i).getKodeBank());
        viewHolder.tv_bank.setText(mFilteredList.get(i).getNamaBank());
        viewHolder.tv_norek.setText(mFilteredList.get(i).getNoRek());
        viewHolder.tvNamaRek.setText(mFilteredList.get(i).getNamaRek());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListRekeningBank> filteredList = new ArrayList<>();
                    for (ListRekeningBank entity : mArrayList) {
                        if (entity.getNoRek().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListRekeningBank>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_bank, tv_kodebank, tv_norek, tvNamaRek;

        public ViewHolder(View view) {
            super(view);
            tv_kodebank = (TextView)view.findViewById(R.id.txt_view_rekbank_kodebank);
            tv_bank = (TextView)view.findViewById(R.id.txt_view_rekbank_bank);
            tv_norek = (TextView)view.findViewById(R.id.txt_view_rekbank_norek);
            tvNamaRek = (TextView)view.findViewById(R.id.txt_view_rekbank_namarek);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeBank", tv_kodebank.getText().toString());
            intent.putExtra("namaBank", tv_bank.getText().toString());
            intent.putExtra("noRek", tv_norek.getText().toString());
            intent.putExtra("namaRek", tvNamaRek.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
