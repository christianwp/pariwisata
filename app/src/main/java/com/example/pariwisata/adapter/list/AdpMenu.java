package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListMenu;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class AdpMenu extends RecyclerView.Adapter<AdpMenu.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListMenu> mArrayList;
    private ArrayList<ListMenu> mFilteredList;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);

    public AdpMenu(Context contextku, ArrayList<ListMenu> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpMenu.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_menu, viewGroup, false);
        return new AdpMenu.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpMenu.ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(mFilteredList.get(i).getKodeMenu());
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaMenu());
        viewHolder.tvHarga.setText(String.valueOf(formatter.format(mFilteredList.get(i).getHarga().setScale(0))));
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListMenu> filteredList = new ArrayList<>();
                    for (ListMenu entity : mArrayList) {
                        if (entity.getNamaMenu().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListMenu>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama, tvHarga;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_menu_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_menu_nama);
            tvHarga = (TextView)view.findViewById(R.id.txt_view_menu_harga);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeMenu", tv_kode.getText().toString());
            intent.putExtra("namaMenu", tv_nama.getText().toString());
            intent.putExtra("harga", tvHarga.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
