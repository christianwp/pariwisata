package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListJenis;

import java.util.ArrayList;

public class AdpJenis extends RecyclerView.Adapter<AdpJenis.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<ListJenis> mArrayList;
    private ArrayList<ListJenis> mFilteredList;

    public AdpJenis(Context contextku, ArrayList<ListJenis> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpJenis.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_jenis, viewGroup, false);
        return new AdpJenis.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpJenis.ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(String.valueOf(mFilteredList.get(i).getKodeJenis()));
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaJenis());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListJenis> filteredList = new ArrayList<>();
                    for (ListJenis entity : mArrayList) {
                        if (entity.getNamaJenis().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListJenis>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_jenis_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_jenis_nama);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kode", tv_kode.getText().toString());
            intent.putExtra("nama", tv_nama.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
