package com.example.pariwisata.adapter.master;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.pariwisata.R;
import com.example.pariwisata.model.master.MsViewRatingModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdpViewRating extends ArrayAdapter<MsViewRatingModel> {

    private List<MsViewRatingModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private String levelku, userIdku;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public AdpViewRating(Context context, int resource, List<MsViewRatingModel> objects) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.txtTgl    =	 (TextView)v.findViewById(R.id.txtColViewRatingTanggal);
            holder.txtKomen = (TextView) v.findViewById(R.id.txtColViewRatingKomen);
            holder.txtUser = (TextView) v.findViewById(R.id.txtColViewRatingUser);
            holder.bintang = (RatingBar) v.findViewById(R.id.rtColViewRatingBintang);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }

        holder.txtKomen.setText(columnslist.get(position).getKomentar());
        holder.txtTgl.setText(columnslist.get(position).getTanggal());
        holder.txtUser.setText(columnslist.get(position).getNamaUser());
        holder.bintang.setRating(columnslist.get(position).getNilai().floatValue());
        holder.bintang.setEnabled(false);
        return v;
    }

    static class ViewHolder{
        private TextView txtKomen;
        private TextView txtTgl;
        private TextView txtUser;
        private RatingBar bintang;
    }
}
