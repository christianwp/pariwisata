package com.example.pariwisata.adapter.master;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.pariwisata.R;
import com.example.pariwisata.model.master.MsWisataFasilitasModel;
import com.example.pariwisata.model.master.MsWisataModel;
import com.example.pariwisata.utilities.Link;

import java.util.List;

public class AdpWisataFasilitas extends ArrayAdapter<MsWisataFasilitasModel> {

    private List<MsWisataFasilitasModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private ListView lsvchoose;
    private static final int SEND_UPLOAD = 201;
    private AlertDialog alert;
    private MsWisataModel modelku;

    public AdpWisataFasilitas(Context context, int resource, List<MsWisataFasilitasModel> data) {
        super(context, resource,  data);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNama	    =	 (TextView)v.findViewById(R.id.txtColMasterWisataFNama);
            holder.ImgDelete	=	 (ImageView)v.findViewById(R.id.imgColMasterWisataFDelete);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }

        holder.TvNama.setText(columnslist.get(position).getNamaFasilitas());

        holder.ImgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        columnslist.remove(position);
                        notifyDataSetChanged();
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });
        return v;
    }

    static class ViewHolder{
        private TextView TvNama;
        private ImageView ImgDelete;
    }
}