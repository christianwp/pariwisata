package com.example.pariwisata.adapter.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.model.transaksi.KeranjangListKriteriaModel;
import com.example.pariwisata.transaksi.KeranjangBayarActivity;
import com.example.pariwisata.transaksi.KeranjangItemActivity;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AdpKeranjang extends ArrayAdapter<KeranjangListKriteriaModel> {

    private List<KeranjangListKriteriaModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Context context;
    private AlertDialog alert;
    private String FileDeteleted = "Keranjang.php";
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private String levelku, userIdku;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

    public AdpKeranjang(Context context, int resource, List<KeranjangListKriteriaModel> objects, String level,
                           String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNo	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangNo);
            holder.TvTanggal	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangTanggal);
            holder.TvNilai	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangNilai);
            holder.TvStatus = (TextView)v.findViewById(R.id.txtColTransKeranjangStatus);
            holder.TvStatusPesanan = (TextView)v.findViewById(R.id.txtColTransKeranjangStatusPesanan);
            holder.ImgDelete	=	 (ImageView)v.findViewById(R.id.imgColTransKeranjangDelete);
            holder.ImgEdit		=	 (ImageView)v.findViewById(R.id.imgColTransKeranjangEdit);
            holder.ImgBayar		=	 (ImageView)v.findViewById(R.id.imgColTransKeranjangUpload);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        holder.ImgDelete.setVisibility(View.GONE);
        holder.ImgEdit.setVisibility(View.GONE);
        holder.ImgBayar.setVisibility(View.GONE);

        Date tglTrans = Calendar.getInstance().getTime();
        try{
            tglTrans = dfSave.parse(columnslist.get(position).getTanggal());
        }catch (Exception ex){}

        holder.TvNo.setText("No Transaksi: "+columnslist.get(position).getNoTrans().trim());
        holder.TvTanggal.setText("Tanggal: "+sdf1.format(tglTrans));
        holder.TvNilai.setText("Nominal Rp "+ String.valueOf(formatter.format(columnslist.get(position).getNilai().setScale(0))));
        holder.TvStatus.setText("Status: "+(columnslist.get(position).getCdstatus().substring(0,1).equals("*")?
                "Transaksi belum dibayar":"Transaksi sudah lunas"));
        holder.TvStatusPesanan.setText("Status: "+(columnslist.get(position).getCdstatus().substring(1,2).equals("*")?
                "Menunggu konfirmasi dari tenant":columnslist.get(position).getCdstatus().substring(2,3).equals("*")?
                "Order sedang disiapkan tenant":"Pesanan selesai"));

        if(levelku.equals("C")){
            if(columnslist.get(position).getCdstatus().substring(0,3).equals("***")){
                holder.ImgDelete.setVisibility(View.VISIBLE);
                holder.ImgEdit.setVisibility(View.VISIBLE);
                holder.ImgBayar.setVisibility(View.VISIBLE);
            }else{
                holder.ImgBayar.setVisibility(View.GONE);
                holder.ImgDelete.setVisibility(View.GONE);
                holder.ImgEdit.setVisibility(View.GONE);
            }
        }else{
            holder.ImgDelete.setVisibility(View.GONE);
            holder.ImgEdit.setVisibility(View.GONE);
            holder.ImgBayar.setVisibility(View.GONE);
        }

        holder.ImgBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeranjangListKriteriaModel item = columnslist.get(position);
                Intent i = new Intent(getContext(), KeranjangBayarActivity.class);
                i.putExtra("noTransaksi", item.getNoTrans().trim());
                i.putExtra("kodeTenant", item.getKodeTenant());
                //i.putExtra("namaTenant", item.getNamaTenant().trim());
                i.putExtra("pemilikTenant", item.getPemilikTenant().trim());
                i.putExtra("nilai", item.getNilai().doubleValue());
                i.putExtra("status", "UPLOAD");
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.ImgEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), KeranjangItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("noTransaksi", columnslist.get(position).getNoTrans());
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.ImgDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kode = columnslist.get(position).getNoTrans().trim();
                        deletedData(Link.BASE_URL_API + Link.KERANJANG, position, kode, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });
        return v;
    }

    private void deletedData(String save, final int position, final String noTransaksi, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            boolean sucses= jsonrespon.getString("value").equals("false");
                            String message = jsonrespon.getString("message");
                            if (sucses){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL "+e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("noTransaksi", noTransaksi);
                params.put("kodeUser", userId);
                params.put("tglNow", tanggalNow);
                params.put("view", "DELETE");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private TextView TvNo;
        private TextView TvTanggal;
        private TextView TvNilai;
        private TextView TvStatus;
        private TextView TvStatusPesanan;
        private ImageView ImgDelete;
        private ImageView ImgEdit;
        private ImageView ImgBayar;
    }
}