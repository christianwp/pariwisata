package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListFasilitas;

import java.util.ArrayList;

public class AdpFasilitas extends RecyclerView.Adapter<AdpFasilitas.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListFasilitas> mArrayList;
    private ArrayList<ListFasilitas> mFilteredList;

    public AdpFasilitas(Context contextku, ArrayList<ListFasilitas> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpFasilitas.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_fasilitas, viewGroup, false);
        return new AdpFasilitas.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpFasilitas.ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(String.valueOf(mFilteredList.get(i).getKode()));
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNama());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListFasilitas> filteredList = new ArrayList<>();
                    for (ListFasilitas entity : mArrayList) {
                        if (entity.getNama().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListFasilitas>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_fs_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_fs_nama);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeFs", tv_kode.getText().toString());
            intent.putExtra("namaFs", tv_nama.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
