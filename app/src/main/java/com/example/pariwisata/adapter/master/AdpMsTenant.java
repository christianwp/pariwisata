package com.example.pariwisata.adapter.master;

import static com.example.pariwisata.utilities.Link.MS_TENANT;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.master.MsMenuActivity;
import com.example.pariwisata.master.MsRekeningView;
import com.example.pariwisata.master.MsTenantItemActivity;
import com.example.pariwisata.model.master.MsTenantModel;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdpMsTenant extends ArrayAdapter<MsTenantModel> {

    private List<MsTenantModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private String levelku, userIdku;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public AdpMsTenant(Context context, int resource, List<MsTenantModel> objects, String level,
                        String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.tvWisata    =	 (TextView)v.findViewById(R.id.txtColTenantNmWisata);
            holder.tvNama    =	 (TextView)v.findViewById(R.id.txtColTenantNmTenant);
            holder.imgDel = (ImageView)v.findViewById(R.id.imgColMasterTenantDelete);
            holder.imgEdit = (ImageView)v.findViewById(R.id.imgColMasterTenantEdit);
            holder.imgMenu = (ImageView)v.findViewById(R.id.imgColMasterTenantMenu);
            holder.imgRek = (ImageView)v.findViewById(R.id.imgColMasterTenantRekening);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        if(levelku.equals("A") || levelku.equals("U")){
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.imgDel.setVisibility(View.VISIBLE);
            holder.imgMenu.setVisibility(View.VISIBLE);
            holder.imgRek.setVisibility(View.VISIBLE);
        }else{
            holder.imgEdit.setVisibility(View.GONE);
            holder.imgDel.setVisibility(View.GONE);
            holder.imgMenu.setVisibility(View.GONE);
            holder.imgRek.setVisibility(View.GONE);
        }
        holder.tvWisata.setText("Tempat Wisata: "+columnslist.get(position).getNamaWisata());
        holder.tvNama.setText("Nama Tenant: "+columnslist.get(position).getNamaTenant());

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), MsTenantItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kodeWisata", columnslist.get(position).getKodeWisata());
                i.putExtra("namaWisata", columnslist.get(position).getNamaWisata());
                i.putExtra("kodeTenant", columnslist.get(position).getKodeTenant());
                i.putExtra("namaTenant", columnslist.get(position).getNamaTenant());
                i.putExtra("deskripsi", columnslist.get(position).getDeskripsi());
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kodeTenant = columnslist.get(position).getKodeTenant();
                        deletedData(Link.BASE_URL_API + MS_TENANT, position, kodeTenant, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });

        holder.imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), MsMenuActivity.class);
                i.putExtra("kodeTenant", columnslist.get(position).getKodeTenant());
                i.putExtra("namaTenant", columnslist.get(position).getNamaTenant());
                ((Activity) context).startActivity(i);
            }
        });

        holder.imgRek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MsRekeningView.class);
                i.putExtra("kodeTenant", columnslist.get(position).getKodeTenant());
                i.putExtra("namaTenant", columnslist.get(position).getNamaTenant());
                ((Activity) context).startActivity(i);
            }
        });

        return v;
    }

    private void deletedData(String save, final int position, final String kodeTenant, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow = dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            int Sucsess = jsonrespon.getInt("success");
                            String message = jsonrespon.getString("message");
                            if (Sucsess ==1 ){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeTenant", kodeTenant);
                params.put("tglNow", tanggalNow);
                params.put("kodeUser", userId);
                params.put("view", "DELETE");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private TextView tvWisata;
        private TextView tvNama;
        private ImageView imgDel;
        private ImageView imgEdit;
        private ImageView imgMenu;
        private ImageView imgRek;
    }
}
