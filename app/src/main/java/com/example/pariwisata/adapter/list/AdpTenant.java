package com.example.pariwisata.adapter.list;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.list.ListTenant;

import java.util.ArrayList;

public class AdpTenant extends RecyclerView.Adapter<AdpTenant.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ListTenant> mArrayList;
    private ArrayList<ListTenant> mFilteredList;
    private String view;

    public AdpTenant(Context contextku, ArrayList<ListTenant> arrayList, String viewku) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
        view = viewku;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_tenant, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.tv_kode.setText(mFilteredList.get(i).getKodeTenant());
        viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaTenant());
        viewHolder.tv_pemilik.setText(mFilteredList.get(i).getPemilikTenant());
        if(view.equals("WISATA"))
            viewHolder.tv_pemilik.setVisibility(View.GONE);
        else if(view.equals("PEMILIK"))
            viewHolder.tv_pemilik.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListTenant> filteredList = new ArrayList<>();
                    for (ListTenant entity : mArrayList) {
                        if (entity.getNamaTenant().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListTenant>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama, tv_pemilik;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_tenant_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_tenant_nama);
            tv_pemilik = (TextView)view.findViewById(R.id.txt_view_tenant_pemilik_tenant);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kodeTenant", tv_kode.getText().toString());
            intent.putExtra("namaTenant", tv_nama.getText().toString());
            intent.putExtra("pemilikTenant", tv_pemilik.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
