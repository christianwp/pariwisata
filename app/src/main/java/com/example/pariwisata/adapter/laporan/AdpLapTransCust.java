package com.example.pariwisata.adapter.laporan;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.pariwisata.R;
import com.example.pariwisata.model.laporan.LapTransCustHeaderModel;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdpLapTransCust extends ArrayAdapter<LapTransCustHeaderModel> {

    private List<LapTransCustHeaderModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private int lastPosition = -1;
    private ViewHolder holder;
    private Activity parent;
    private Context context;
    private String levelku, userIdku;
    private DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
    private DecimalFormat formatter = new DecimalFormat("###,##0.00", symbols);
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public AdpLapTransCust(Context context, int resource, List<LapTransCustHeaderModel> objects, String level,
                                  String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNoTrans	    =	 (TextView)v.findViewById(R.id.txtColLapTransCustNoTrans);
            holder.TvNilai	    =	 (TextView)v.findViewById(R.id.txtColLapTransCustNominal);
            holder.TvNamaBank	    =	 (TextView)v.findViewById(R.id.txtColLapTransCustBank);
            holder.TvTglTrans	    =	 (TextView)v.findViewById(R.id.txtColLapTransCustTglTrans);
            holder.TvNoRek	    =	 (TextView)v.findViewById(R.id.txtColLapTransCustNoRek);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        Date tanggal = Calendar.getInstance().getTime();
        try{
            tanggal = dfSave.parse(columnslist.get(position).getTanggal());
        }catch (Exception ex){}
        holder.TvNilai.setText("Rp "+formatter.format(columnslist.get(position).getNominal().setScale(2)));
        holder.TvNoTrans.setText(columnslist.get(position).getNoTrans());
        holder.TvNamaBank.setText(columnslist.get(position).getNamaBank());
        holder.TvTglTrans.setText(sdf1.format(tanggal));
        holder.TvNoRek.setText(columnslist.get(position).getNoRek());
        symbols.setGroupingSeparator(' ');

        if(columnslist.get(position).getCdstatus().substring(0,1).equals("*")){
            holder.TvNoTrans.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
            holder.TvNilai.setTextColor(getContext().getResources().getColor(R.color.md_red_600));
        }else{
            holder.TvNoTrans.setTextColor(getContext().getResources().getColor(R.color.md_green_600));
            holder.TvNilai.setTextColor(getContext().getResources().getColor(R.color.md_green_600));
        }
        return v;
    }

    static class ViewHolder{
        private TextView TvNilai;
        private TextView TvNoTrans;
        private TextView TvNamaBank;
        private TextView TvTglTrans;
        private TextView TvNoRek;
    }
}