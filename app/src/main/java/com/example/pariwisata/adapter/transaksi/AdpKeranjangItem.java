package com.example.pariwisata.adapter.transaksi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.pariwisata.R;
import com.example.pariwisata.model.transaksi.KeranjangItemModel;
import com.example.pariwisata.model.transaksi.KeranjangModel;
import com.example.pariwisata.transaksi.KeranjangItemDetailActivity;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class AdpKeranjangItem extends ArrayAdapter<KeranjangItemModel> {

    private List<KeranjangItemModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private String levelku;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private boolean edited;
    private KeranjangModel modelData;

    public AdpKeranjangItem(Context context, int resource, List<KeranjangItemModel> objects, String level,
                            KeranjangModel model) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        edited = model.getTransactionStatus().isTransModifyAllowed();
        modelData = model;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNMenu	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangItemNmMenu);
            holder.TvHargaQtyTotal	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangItemHargaQtyTotal);
            holder.TvKet	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangItemKet);
            holder.TvNo	    =	 (TextView)v.findViewById(R.id.txtColTransKeranjangItemNomor);
            holder.ImgDelete	=	 (ImageView)v.findViewById(R.id.imgColTransKeranjangItemDelete);
            holder.ImgEdit		=	 (ImageView)v.findViewById(R.id.imgColTransKeranjangItemEdit);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        holder.ImgDelete.setVisibility(View.GONE);
        holder.ImgEdit.setVisibility(View.GONE);

        holder.TvNo.setText((String.valueOf(position+1))+". ");
        holder.TvNMenu.setText(columnslist.get(position).getNamaMenu().trim());
        BigDecimal total = columnslist.get(position).getHarga().multiply(new BigDecimal(columnslist.get(position).getQty()));
        holder.TvHargaQtyTotal.setText("@"+String.valueOf(formatter.format(columnslist.get(position).getHarga().setScale(0)))+" X "
                +String.valueOf(columnslist.get(position).getQty())+" = Rp "+ String.valueOf(formatter.format(total.setScale(0))));
        holder.TvKet.setText("Note: "+columnslist.get(position).getKeterangan());

        if(levelku.equals("C")){
            if(edited){
                holder.ImgDelete.setVisibility(View.VISIBLE);
                holder.ImgEdit.setVisibility(View.VISIBLE);
            }else{
                holder.ImgDelete.setVisibility(View.GONE);
                holder.ImgEdit.setVisibility(View.GONE);
            }
        }else{
            holder.ImgDelete.setVisibility(View.GONE);
            holder.ImgEdit.setVisibility(View.GONE);
        }

        holder.ImgEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), KeranjangItemDetailActivity.class);
                i.putExtra("statusItem", "EDIT");
                i.putExtra("model", modelData);
                i.putExtra("itemModel", columnslist.get(position));
                ((Activity) context).startActivityForResult(i, 8);
            }
        });

        holder.ImgDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), KeranjangItemDetailActivity.class);
                i.putExtra("statusItem", "DELETE");
                i.putExtra("model", modelData);
                i.putExtra("itemModel", columnslist.get(position));
                ((Activity) context).startActivityForResult(i, 8);
            }
        });
        return v;
    }

    static class ViewHolder{
        private TextView TvNMenu;
        private TextView TvHargaQtyTotal;
        private TextView TvKet;
        private TextView TvNo;
        private ImageView ImgDelete;
        private ImageView ImgEdit;
    }
}