package com.example.pariwisata.laporan;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pariwisata.R;
import com.example.pariwisata.list.ListRekBankView;
import com.example.pariwisata.list.ListTenantView;
import com.example.pariwisata.utilities.PrefUtil;
import com.example.pariwisata.utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LaporanTransaksiCustActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private ImageView imgBack, imgAwal, imgAkhir;
    private String userId, level, kodeTenant, pemilikTenant, kodeBank;
    private Button btnProses;
    private CheckBox ckAllRek;
    private Calendar dateAndTimeFrom = Calendar.getInstance();
    private Calendar dateAndTimeTo = Calendar.getInstance();
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    private TextInputEditText edTenant, edRekening, edAwal, edAkhir, edBank, edNamaRek;
    private boolean allRek;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lap_kriteria_transaksi);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        prefUtil = new PrefUtil(this);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Calendar cal = Calendar.getInstance();
        edTenant = (TextInputEditText) findViewById(R.id.eLapKriteriaTransTenant);
        edRekening = (TextInputEditText) findViewById(R.id.eLapKriteriaTransRek);
        edAwal = (TextInputEditText) findViewById(R.id.eLapKriteriaTransPeriodeAwal);
        edAkhir = (TextInputEditText) findViewById(R.id.eLapKriteriaTransPeriodeAkhir);
        edBank = (TextInputEditText) findViewById(R.id.eLapKriteriaTransBank);
        edNamaRek = (TextInputEditText) findViewById(R.id.eLapKriteriaTransNamaRek);
        btnProses = (Button) findViewById(R.id.btnLapKriteriaTransProses);
        imgBack = (ImageView) findViewById(R.id.imgLapKriteriaTransBack);
        imgAwal = (ImageView) findViewById(R.id.imgLapKriteriaTransPeriodeAwal);
        imgAkhir = (ImageView) findViewById(R.id.imgLapKriteriaTransPeriodeAkhir);
        ckAllRek = (CheckBox)findViewById(R.id.ckAllLapKriteriaTransRek);

        dateAndTimeFrom.setTime(Utils.getFirstTimeOfDay(cal.getTime()));
        dateAndTimeTo.setTime(Utils.getFirstTimeOfDay(cal.getTime()));
        edAwal.setText(sdf2.format(Utils.getFirstDayOfMonth(dateAndTimeFrom.getTime())));
        edAkhir.setText(sdf2.format(Utils.getLastDayOfMonth(dateAndTimeTo.getTime())));

        ckAllRek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allRek){
                    allRek=false;
                    edRekening.setEnabled(true);
                }else{
                    allRek=true;
                    edRekening.setEnabled(false);
                }
                kodeBank = null;
                edBank.setText("");
                edRekening.setText("");
                edNamaRek.setText("");
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgAwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(LaporanTransaksiCustActivity.this, dFrom, dateAndTimeFrom.get(Calendar.YEAR), dateAndTimeFrom.get(Calendar.MONTH),
                        dateAndTimeFrom.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        imgAkhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(LaporanTransaksiCustActivity.this, dTo, dateAndTimeTo.get(Calendar.YEAR), dateAndTimeTo.get(Calendar.MONTH),
                        dateAndTimeTo.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edTenant.setEnabled(false);
                hideKeyboard(v);
                pilihTenant();
                edTenant.setEnabled(true);
            }
        });

        edTenant.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edTenant.setEnabled(false);
                    hideKeyboard(v);
                    pilihTenant();
                    edTenant.setEnabled(true);
                }
            }
        });

        edRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edTenant.getText().toString().equals("")){
                    Toast.makeText(LaporanTransaksiCustActivity.this, "Tenant harus diisi", Toast.LENGTH_LONG).show();
                }else {
                    edRekening.setEnabled(false);
                    hideKeyboard(v);
                    pilihRekBank();
                    edRekening.setEnabled(true);
                }
            }
        });

        edRekening.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(edTenant.getText().toString().equals("")){
                        Toast.makeText(LaporanTransaksiCustActivity.this, "Tenant harus diisi", Toast.LENGTH_LONG).show();
                    }else {
                        edRekening.setEnabled(false);
                        hideKeyboard(v);
                        pilihRekBank();
                        edRekening.setEnabled(true);
                    }
                }
            }
        });


        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateAwal() && validateAkhir() && validateTenant() && validateDate() && validateRek()){
                    Intent i = new Intent(LaporanTransaksiCustActivity.this, LaporanTransaksiCustItemActivity.class);
                    i.putExtra("kodeTenant", kodeTenant);
                    i.putExtra("namaTenant", edTenant.getText().toString());
                    i.putExtra("noRek", ckAllRek.isChecked()?"%":edRekening.getText().toString());
                    i.putExtra("tglFrom", df.format(Utils.getFirstTimeOfDay(dateAndTimeFrom.getTime())));
                    i.putExtra("tglTo", df.format(Utils.getLastTimeOfDay(dateAndTimeTo.getTime())));
                    startActivity(i);
                }
            }
        });
    }

    private void pilihTenant(){
        Intent i = new Intent(LaporanTransaksiCustActivity.this, ListTenantView.class);
        if (level.equals("A"))
            i.putExtra("kode", "%");
        else
            i.putExtra("kode", userId);
        i.putExtra("view", "PEMILIK");
        startActivityForResult(i, 2);
    }

    private void pilihRekBank(){
        Intent i = new Intent(LaporanTransaksiCustActivity.this, ListRekBankView.class);
        i.putExtra("kode", kodeTenant);
        startActivityForResult(i, 4);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                kodeTenant = data.getStringExtra("kodeTenant");
                pemilikTenant = data.getStringExtra("pemilikTenant");
                edTenant.setText(data.getStringExtra("namaTenant"));
            }
        }
        if(requestCode == 4) {
            if(resultCode == RESULT_OK) {
                kodeBank = data.getStringExtra("kodeBank");
                edBank.setText(data.getStringExtra("namaBank"));
                edRekening.setText(data.getStringExtra("noRek"));
                edNamaRek.setText(data.getStringExtra("namaRek"));
            }
        }
    }

    private DatePickerDialog.OnDateSetListener dFrom =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTimeFrom.set(Calendar.YEAR, year);
            dateAndTimeFrom.set(Calendar.MONTH, month);
            dateAndTimeFrom.set(Calendar.DAY_OF_MONTH, day);
            updatelabelFrom();
        }
    };

    private void updatelabelFrom(){
        edAwal.setText(sdf2.format(dateAndTimeFrom.getTime()));
    }

    private DatePickerDialog.OnDateSetListener dTo =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTimeTo.set(Calendar.YEAR, year);
            dateAndTimeTo.set(Calendar.MONTH, month);
            dateAndTimeTo.set(Calendar.DAY_OF_MONTH, day);
            updatelabelTo();
        }
    };

    private void updatelabelTo(){
        edAkhir.setText(sdf2.format(dateAndTimeTo.getTime()));
    }

    private boolean validateTenant() {
        boolean value;
        if (edTenant.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(LaporanTransaksiCustActivity.this,"Tenant harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateRek() {
        boolean value;
        if (edRekening.getText().toString().isEmpty() && !ckAllRek.isChecked()){
            value=false;
            Toast.makeText(LaporanTransaksiCustActivity.this,"Sumber rekening harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateAwal() {
        boolean value;
        if (edAwal.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(LaporanTransaksiCustActivity.this,"Periode Awal harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateAkhir() {
        boolean value;
        if (edAkhir.getText().toString().isEmpty()){
            value=false;
            Toast.makeText(LaporanTransaksiCustActivity.this,"Periode Akhir harus diisi !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateDate() {
        boolean value;
        if (dateAndTimeTo.compareTo(dateAndTimeFrom)<0){
            value=false;
            Toast.makeText(LaporanTransaksiCustActivity.this,"Periode Akhir harus lebih besar dari periode awal !", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}