package com.example.pariwisata.laporan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.pariwisata.R;
import com.example.pariwisata.adapter.laporan.AdpLapTransCust;
import com.example.pariwisata.model.laporan.LapTransCustHeaderModel;
import com.example.pariwisata.transaksi.KeranjangItemActivity;
import com.example.pariwisata.transaksi.KeranjangListActivity;
import com.example.pariwisata.utilities.AppController;
import com.example.pariwisata.utilities.Link;
import com.example.pariwisata.utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LaporanTransaksiCustItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private ImageView imgBack;
    private String userId, level;
    private ProgressDialog pDialog;
    private TextView txtTenant, txtPeriode, txtNoRek, txtStatus, txtTotal, txtTotalBlm, txtTotalBayar;
    private ListView lsvData;
    private String kodeTenant, tglAwal, tglAkhir, noRek, namaTenant;
    private AdpLapTransCust adapter;
    private ArrayList<LapTransCustHeaderModel> columnlist= new ArrayList<>();
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lap_transaksi);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        prefUtil = new PrefUtil(this);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                kodeTenant = i.getString("kodeTenant");
                namaTenant = i.getString("namaTenant");
                noRek = i.getString("noRek");
                tglAwal = i.getString("tglFrom");
                tglAkhir = i.getString("tglTo");
            } catch (Exception e) {}
        }

        imgBack = (ImageView) findViewById(R.id.imgLapTransaksiCustItemBack);
        txtTenant = (TextView) findViewById(R.id.txtLapTransTenant);
        txtPeriode = (TextView) findViewById(R.id.txtLapTransPeriode);
        txtNoRek = (TextView) findViewById(R.id.txtLapTransRek);
        lsvData = (ListView) findViewById(R.id.listLapTrans);
        txtStatus = (TextView) findViewById(R.id.TvStatusLapTrans);
        txtTotal = (TextView) findViewById(R.id.txtLapMutasiSaldoCustTotal);
        txtTotalBayar = (TextView) findViewById(R.id.txtLapMutasiSaldoCustTotalLunas);
        txtTotalBlm = (TextView) findViewById(R.id.txtLapMutasiSaldoCustTotalBlm);

        adapter		= new AdpLapTransCust(LaporanTransaksiCustItemActivity.this, R.layout.adp_lap_trans_summary,
                columnlist, level, userId);
        lsvData.setAdapter(adapter);
        clear();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lsvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(LaporanTransaksiCustItemActivity.this, KeranjangItemActivity.class);
                i.putExtra("Status", "LAPORAN");
                i.putExtra("noTransaksi", columnlist.get(position).getNoTrans());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        getData(Link.BASE_URL_API+Link.KERANJANG, noRek, kodeTenant, tglAwal, tglAkhir);
    }

    private void clear(){
        txtTenant.setText("");
        txtNoRek.setText("");
        txtPeriode.setText("");
        txtTotal.setText("Rp 0");
    }

    private void getData(String Url, final String noRek, final String kodeTenant, final String tglAwal, final String tglAkhir){
        pDialog.setMessage("Loading....");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    adapter.clear();
                    if(sucses==0){
                        txtStatus.setVisibility(View.VISIBLE);
                        txtStatus.setText("Data Tidak Ada");
                    }else{
                        txtStatus.setVisibility(View.INVISIBLE);
                        JSONArray JsonItem = jsonrespon.getJSONArray("uploade");
                        BigDecimal total = BigDecimal.ZERO;
                        BigDecimal totalBayar = BigDecimal.ZERO;
                        BigDecimal totalBlmBayar = BigDecimal.ZERO;
                        for (int a = 0; a <JsonItem.length(); a++) {
                            Object objItem = JsonItem.getJSONObject(a);
                            LapTransCustHeaderModel colums 	= new LapTransCustHeaderModel();
                            colums.setNoTrans((String)((JSONObject) objItem).get("c_kodetransaksi"));
                            colums.setKodeUser((String)((JSONObject) objItem).get("c_userid"));
                            colums.setNamaUser((String)((JSONObject) objItem).get("vc_username"));
                            colums.setKodeTenant((String)((JSONObject) objItem).get("c_kodetenant"));
                            colums.setNamaTenant((String)((JSONObject) objItem).get("vc_namatenant"));
                            colums.setTanggal((String)((JSONObject) objItem).get("dt_tanggal"));
                            colums.setNominal(new BigDecimal(((JSONObject) objItem).getDouble("n_nilai")));
                            colums.setKodeBank((String)((JSONObject) objItem).get("c_kodebank"));
                            colums.setNamaBank((String)((JSONObject) objItem).get("vc_namabank"));
                            colums.setNoRek((String)((JSONObject) objItem).get("c_norekening"));
                            colums.setNamaRek((String)((JSONObject) objItem).get("vc_namarekening"));
                            colums.setCdstatus((String)((JSONObject) objItem).get("c_dstatus"));
                            if(colums.getCdstatus().substring(0,1).equals("*")){
                                total = total.add(colums.getNominal());
                                totalBlmBayar = totalBlmBayar.add(colums.getNominal());
                            }else{
                                total = total.add(colums.getNominal());
                                totalBayar = totalBayar.add(colums.getNominal());
                            }
                            columnlist.add(colums);
                        }
                        txtTenant.setText(namaTenant);
                        txtNoRek.setText(noRek.equals("%")?"Semua Rekening":noRek);
                        Date tglFrom = Calendar.getInstance().getTime();
                        Date tglTo = Calendar.getInstance().getTime();
                        try{
                            tglFrom = dfSave.parse(tglAwal);
                            tglTo = dfSave.parse(tglAkhir);
                        }catch (Exception ex){}
                        txtPeriode.setText(sdf1.format(tglFrom)+ " s/d "+ sdf1.format(tglTo));
                        txtTotal.setText("Rp "+String.valueOf(formatter.format(total)));
                        txtTotalBayar.setText("Rp "+String.valueOf(formatter.format(totalBayar)));
                        txtTotalBlm.setText("Rp "+String.valueOf(formatter.format(totalBlmBayar)));
                        adapter.notifyDataSetChanged();
                    }
                    hideDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                    hideDialog();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,"Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,"AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,"Check Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,"Check Network Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(LaporanTransaksiCustItemActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("noRek", noRek);
                params.put("kodeTenant", kodeTenant);
                params.put("tglAwal", tglAwal);
                params.put("tglAkhir", tglAkhir);
                params.put("view", "LAPORAN");
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}