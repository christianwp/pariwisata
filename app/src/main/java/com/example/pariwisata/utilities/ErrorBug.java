package com.example.pariwisata.utilities;

import android.os.Bundle;
import android.widget.TextView;

import com.example.pariwisata.R;

import androidx.appcompat.app.AppCompatActivity;

public class ErrorBug extends AppCompatActivity {
    private TextView tvError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.error_bug);

        tvError     = (TextView)findViewById(R.id.TvErorr);
        tvError.setText(getIntent().getStringExtra("error"));

    }
}
