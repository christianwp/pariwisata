package com.example.pariwisata.utilities;

import com.example.pariwisata.service.BaseApiService;

public class Link {

    public static final String BASE_URL_API = "http://softchrist.com/jeremi/php/";
    public static final String BASE_URL_NOTIF = "http://softchrist.com/jeremi/php/notification.php";
    public static final String BASE_URL_IMAGE_PROFILE = "http://softchrist.com/jeremi/profile/";
    public static final String MS_KAB_KOTA = "MsKabKota.php";
    public static final String MS_TENANT = "MsTenant.php";
    public static final String MS_EVENT = "MsEvent.php";
    public static final String MS_MENU = "MsMenu.php";
    public static final String KOMENTAR = "Komentar.php";
    public static final String MS_USER = "MsUser.php";
    public static final String MS_PROV = "MsProvinsi.php";
    public static final String MS_BANK = "MsBank.php";
    public static final String MS_REKENING = "MsRekening.php";
    public static final String MS_JNSMENU = "MsJenisMenu.php";
    public static final String MS_FASILITAS = "MsFasilitas.php";
    public static final String MS_KATEGORI_WISATA = "MsKategoriWisata.php";
    public static final String KERANJANG = "Keranjang.php";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }

    public static BaseApiService getImgProfileService(){
        return RetrofitClient.getClient(BASE_URL_IMAGE_PROFILE).create(BaseApiService.class);
    }
}
