package com.example.pariwisata.utilities;

public class FormValidationException extends Exception {
    private String message;
    private String[] fieldNames;

    public FormValidationException(String message, String... invalidFieldNames){
        this.message = message;
        this.fieldNames = invalidFieldNames;
    }

    public String getMessage() {return this.message;}
    public String[] getInvalidFieldNames() {return this.fieldNames;}
}
