package com.example.pariwisata.utilities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtil {

    private Activity activity;
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String STATUS = "STATUS";// A:ADMIN, E:EDITOR KAB/KOTA, C:CUSTOMER
    public static final String PATH = "PATH";
    public static final String PASW = "PASW";
    public static final String TELP = "TELP";
    public static final String EMAIL = "EMAIL";
    public static final String COUNT = "COUNT";
    public static final String KDKAB = "KDKAB";
    public static final String NMKAB = "NMKAB";
    public static final String KDPRV = "KDPRV";
    public static final String NMPRV = "NMPRV";
    public static final String KDWST = "KDWST";
    public static final String NMWST = "NMWST";

    public PrefUtil(Activity activity) {
        this.activity = activity;
    }

    public void clear() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply(); // This line is IMPORTANT !!!
    }

    public void saveUserInfo(String id, String name, String status, String path, String pasw, String telp,
                             String email, Integer count, String kdkab, String nmkab, String kdPrv,
                             String nmPrv, String kdwisata, String nmWisata){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ID, id);
        editor.putString(NAME, name);
        editor.putString(STATUS, status);
        editor.putString(PATH, path);
        editor.putString(PASW, pasw);
        editor.putString(TELP, telp);
        editor.putString(EMAIL, email);
        editor.putInt(COUNT, count);
        editor.putString(KDKAB, kdkab);
        editor.putString(NMKAB, nmkab);
        editor.putString(KDPRV, kdPrv);
        editor.putString(NMPRV, nmPrv);
        editor.putString(KDWST, kdwisata);
        editor.putString(NMWST, nmWisata);
        editor.apply();
    }

    public SharedPreferences getUserInfo(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        //Log.d("MyApp", "Name : "+prefs.getString("fb_name",null)+"\nEmail : "+prefs.getString("fb_email",null));
        return prefs;
    }
}
