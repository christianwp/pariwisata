package com.example.pariwisata.utilities;

import com.example.pariwisata.model.list.ListBank;
import com.example.pariwisata.model.list.ListFasilitas;
import com.example.pariwisata.model.list.ListJenis;
import com.example.pariwisata.model.list.ListKab;
import com.example.pariwisata.model.list.ListKategori;
import com.example.pariwisata.model.list.ListMenu;
import com.example.pariwisata.model.list.ListProvinsi;
import com.example.pariwisata.model.list.ListRekeningBank;
import com.example.pariwisata.model.list.ListTenant;
import com.example.pariwisata.model.list.ListWisata;

public class JSONResponse {

    private ListProvinsi[] province;
    private ListKab[] kab;
    private ListKategori[] kategori;
    private ListFasilitas[] fasilitas;
    private ListBank[] bank;
    private ListWisata[] wisata;

    private ListJenis[] jenis;

    private ListTenant[] tenant;

    private ListMenu[] menu;
    private ListRekeningBank[] rekBank;

    public ListProvinsi[] getProvince() {
        return province;
    }

    public ListKategori[] getKategori() {
        return kategori;
    }

    public ListKab[] getKab() {
        return kab;
    }

    public ListFasilitas[] getFasilitas() {
        return fasilitas;
    }

    public ListWisata[] getWisata() {return wisata;}

    public ListJenis[] getJenis() {
        return jenis;
    }

    public ListTenant[] getTenant() {
        return tenant;
    }

    public ListMenu[] getMenu() {
        return menu;
    }

    public ListBank[] getBank() {
        return bank;
    }

    public ListRekeningBank[] getRekBank() {
        return rekBank;
    }
}