package com.example.pariwisata.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pariwisata.R;
import com.example.pariwisata.service.BaseApiService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.ocpsoft.prettytime.PrettyTime;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utils {

    private static final ImageLoader imgloader = ImageLoader.getInstance();

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    public static void freeMemory(){
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static String trimCommaOfString(String string) {
        if(string.contains(",")){
            return string.replace(",","");}
        else {
            return string;
        }
    }

    public static String getDecimalFormattedString(String value){
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str1 = value;
        String str2 = "";
        if (lst.countTokens() > 1){
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        if (str1.charAt( -1 + str1.length()) == '.'){
            j--;
            str3 = ".";
        }
        for (int k = j;; k--){
            if (k < 0){
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3){
                str3 = "," + str3;
                i = 0;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }
    }

    public static void currentTime(String date, TextView txtTime){
        long longTimeAgo	= timeStringtoMilis(date);
        PrettyTime prettyTime = new PrettyTime();
        txtTime.setText(prettyTime.format(new Date(longTimeAgo)));
    }

    private static long timeStringtoMilis(String time) {
        long milis = 0;
        try {
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date 	= sd.parse(time);
            milis 		= date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return milis;
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static java.util.Date getFirstTimeOfDay(java.util.Date date){
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTime(date);
        tempCalendar.set(Calendar.HOUR_OF_DAY,00);
        tempCalendar.set(Calendar.MINUTE,00);
        tempCalendar.set(Calendar.SECOND,00);
        tempCalendar.set(Calendar.MILLISECOND,000);
        return tempCalendar.getTime();
    }

    public static java.util.Date getLastTimeOfDay(java.util.Date date){
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTime(date);
        tempCalendar.set(Calendar.HOUR_OF_DAY,23);
        tempCalendar.set(Calendar.MINUTE,59);
        tempCalendar.set(Calendar.SECOND,59);
        tempCalendar.set(Calendar.MILLISECOND,999);
        return tempCalendar.getTime();
    }

    public static void getCycleImage(String url, ImageView img, Context context){
        DisplayImageOptions options;
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer((int) 50.5f))
                .showImageOnLoading(R.mipmap.ic_launcher)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .build();
        imgloader.init(ImageLoaderConfiguration.createDefault(context));
        imgloader.displayImage(url, img, options);
        return;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /*public static void sendNotification(BaseApiService mApiService, String judul, String nama, String idTujuan){
        mApiService.sendNotification(judul, nama, idTujuan)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                        if(stat){
//                            Intent returnIntent = new Intent();
//                            setResult(RESULT_OK, returnIntent);
//                            if (response.isSuccessful()){
//                                hideDialog();
//                                Toast.makeText(DaftarTahapanSidangItemActivity.this, "DATA BERHASIL "+(status.equals("*")?"DIBATALKAN":"DIVALIDASI"), Toast.LENGTH_LONG).show();
//                                finish();
//                            } else {
//                                hideDialog();
//                                Toast.makeText(DaftarTahapanSidangItemActivity.this, "DATA BERHASIL "+(status.equals("*")?"DIBATALKAN":"DIVALIDASI"), Toast.LENGTH_LONG).show();
//                                finish();
//                            }
//                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }*/

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public static java.util.Date getFirstDayOfMonth(java.util.Date date) {
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTime(date);
        tempCalendar.set(Calendar.DAY_OF_MONTH, 1);
        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
        tempCalendar.set(Calendar.MINUTE, 0);
        tempCalendar.set(Calendar.SECOND, 0);
        tempCalendar.set(Calendar.MILLISECOND, 0);
        return tempCalendar.getTime();
    }

    public static java.util.Date getLastDayOfMonth(java.util.Date date) {
        return getLastDayOfMonth(date, false);
    }

    public static java.util.Date getLastDayOfMonth(java.util.Date date, boolean zeroTime) {
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTime(date);
        tempCalendar.set(Calendar.DAY_OF_MONTH, 1);
        tempCalendar.add(Calendar.MONTH, 1);
        tempCalendar.add(Calendar.DAY_OF_MONTH, -1);
        tempCalendar.set(Calendar.HOUR_OF_DAY, zeroTime ? 0 : 23);
        tempCalendar.set(Calendar.MINUTE, zeroTime ? 0 : 59);
        tempCalendar.set(Calendar.SECOND, zeroTime ? 0 : 59);
        tempCalendar.set(Calendar.MILLISECOND, zeroTime ? 0 : 999);
        return tempCalendar.getTime();
    }

}
